package config

// UseInvalidationPrediction is a flag that indicates whether the software must
// use the invalidation prediction algorithm or not? Invalidation predication is
// the new algorithm that can predicts for how long a formula does not hold. This can
// particularly boost the efficiency of negation and box operators. By default, the
// uses the invalidation prediction algorithm
var UseInvalidationPrediction = true

// CollectStats indicates whether the engine must collect statistics about performance and memory usage
const CollectStats = true

// StratifyProgram indicates whether the engine must stratify programs or not. By default, this setting is
// is enabled because for our algorithms to work correctly we must first stratify programs. However, for some
// test cases, I want to test un-stratified programs. This setting allows me to test in such scenarios
var StratifyProgram = true

package cassert

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of a custom assertion package which
 * enables/disables some assert statements according to the configurations
 * of Laser 2.0
 */

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/config"
	"github.com/stretchr/testify/assert"
)

// CAssert keeps an assert object in order to customize the testing behavior
// according to the configurations of Laser
type CAssert struct {
	assert *assert.Assertions
}

// New creates a new CTesting struct
func New(t *testing.T) *CAssert {
	return &CAssert{
		assert: assert.New(t),
	}
}

// CEqual tests whether two values are equal or not only when the configuration is set to use
// the invalidation prediction algorithm. When the configuration is set otherwise, the function
// returns true
func (c *CAssert) CEqual(expected, actual interface{}, msgAndArgs ...interface{}) bool {
	// Only perform the actual test if Laser is configured to use the invalidation prediction algorithm
	if config.UseInvalidationPrediction {
		return c.assert.Equal(expected, actual, msgAndArgs...)
	}
	return true
}

// Equal tests whether two values are equal or not
func (c *CAssert) Equal(expected, actual interface{}, msgAndArgs ...interface{}) bool {
	return c.assert.Equal(expected, actual, msgAndArgs...)
}

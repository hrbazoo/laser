package program

import (
	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/conjunctformulae"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of Program in Laser
 */

// Program represents the definition of Program in Laser
type Program struct {
	Strata            []*stratum.Stratum
	PredicateToStrata map[types.Predicate][]*stratum.Stratum
}

// New takes a number of rules in their string format, parses them into rule structures,
// organizes them into strata, creates the program structure and returns it.
func New(ruleStrings []string) *Program {
	// First, parse the rules
	rules := make([]*rule.Rule, len(ruleStrings))
	for i, s := range ruleStrings {
		rules[i] = rule.Parse(s)
	}

	// Stratify the program (if the program is configured to do so)
	var strata []*stratum.Stratum
	if config.StratifyProgram {
		strata = stratify(rules)
	} else {
		strata = []*stratum.Stratum{stratum.New(rules)}
	}
	// Create the predicate to strata mappings
	predicateToStrata := map[types.Predicate][]*stratum.Stratum{}
	for _, s := range strata {
		for p := range s.PredicateToFormula {
			predicateToStrata[p] = append(predicateToStrata[p], s)
		}
	}
	// Create the new program and return its pointer
	return &Program{
		Strata:            strata,
		PredicateToStrata: predicateToStrata,
	}
}

// consolidateRulesWithSameHead takes a number of rules and reconsolidates those which have
// the same head into a single rule
func consolidateRulesWithSameHeads(rules []*rule.Rule) []*rule.Rule {
	// Create a map from the head predicate to the body formula
	predicateToBodies := map[types.Predicate][]conjunctformulae.ConjFormulae{}
	// Create a map from predicate to head atoms
	predicateToHead := map[types.Predicate][]formula.Formula{}
	// Create a map from predicate to the string format of the rule
	predicateToStringFormat := map[types.Predicate]string{}

	// Iterate over rules and combine rules that have the same head atom
	for _, r := range rules {
		// Get the head's predicate
		p := r.Head[0].GetPredicate()
		// If multiple rules conclude the same atom, we need to reconsolidate the bodies
		// into a single conjoint-formula
		_, exists := predicateToBodies[p]
		// If this predicate has not been seen before, create its corresponding place in the
		// map
		if !exists {
			predicateToBodies[p] = r.Body
			predicateToHead[p] = r.Head
			predicateToStringFormat[p] = r.StringFormat
		} else {
			// Add the body of current rule (there would be only a single conjoint-formula in the body array) to the list
			predicateToBodies[p] = append(predicateToBodies[p], r.Body[0])
			// Add the head of current rule (there would be only a single formula in the head array) to the list
			predicateToHead[p] = append(predicateToHead[p], r.Head[0])
			// Append the string format of the rule's body to the corresponding map
			predicateToStringFormat[p] += (" || " + r.StringFormat)
		}
	}

	// Create a new list of rules with the head predicate appearing only once in each rule
	newRules := []*rule.Rule{}
	for p, b := range predicateToBodies {
		newRules = append(newRules, rule.New(predicateToHead[p], &b, predicateToStringFormat[p]))
	}

	return newRules
}

// InsertSingleAtomBatch inserts a batch of atoms in the program
func (program *Program) InsertSingleAtomBatch(atomBatch *data.AtomBatch) {
	// Do any of stratum in the program have formula with this predicate?
	strata, exists := program.PredicateToStrata[(*atomBatch).Predicate]
	if exists {
		// Found some rules which include this predicate. Insert the batch of
		// atoms into these strata
		for _, s := range strata {
			s.InsertSingleAtomBatch(atomBatch)
		}
	}
}

// InsertMultipleAtomBatches inserts a batch of atoms in the program
func (program *Program) InsertMultipleAtomBatches(atomBatches *[]*data.AtomBatch) {
	// Do any of stratum in the program have formula with this predicate?
	for _, s := range program.Strata {
		s.InsertMultipleAtomBatches(atomBatches)
	}
}

// hasNewConclusions is a recursive function that implements a fixed-point calculation
// on program strata and returns whether the calculation yielded any new conclusions
func (program *Program) hasNewConclusions() bool {
	// Evaluate each stratum in the program to see if they yield any new conclusions
	hasNew := false // Shows whether the evaluation yielded new conclusions
	for _, s := range program.Strata {
		evaluationResult := s.Evaluate()
		// If the stratum evaluation is positive, collect concluded atoms
		// and feed them back to the program
		if evaluationResult == true {
			stratumConcludedAtomBatches := s.CollectAllConcludedAtoms()
			program.InsertMultipleAtomBatches(stratumConcludedAtomBatches)
		}
		hasNew = (evaluationResult || hasNew)
	}

	// If the evaluation yielded no new conclusions, we are done!
	if !hasNew {
		return false
	}

	// Re-evaluate the program
	program.hasNewConclusions()

	// In this point of evaluation, the program execution has certainly yielded new results
	return hasNew
}

// hasValidConclusions checks whether any rule in any stratum still has valid conclusions
func (program *Program) hasValidConclusions() bool {
	// Iterate over program strata
	for _, s := range program.Strata {
		// If a rule in the stratum still has valid conclusions, return true
		if s.HasValidConclusions() {
			return true
		}
	}
	// No rule in no stratum has any valid conclusions
	return false
}

// Evaluate checks the validity of the program strata at the current time point
func (program *Program) Evaluate() bool {
	// the program is valid either if it has new conclusions at the current time point or if
	// some of the conclusions of the past time points are still valid at the present time.
	return program.hasNewConclusions() || program.hasValidConclusions()
}

// CollectConcludedAtoms returns the recent conclusions of the program in form of AtomBatches
func (program *Program) CollectConcludedAtoms() *[]*data.AtomBatch {
	atomBatches := []*data.AtomBatch{}

	for _, s := range program.Strata {
		stratumConcludedAtomBatches := s.CollectRecentAndNonRecentConcludedAtoms()
		atomBatches = append(atomBatches, *stratumConcludedAtomBatches...)
	}

	return &atomBatches
}

// CollectConcludedAtomsInStringFormat returns the concluded atoms in string format
func (program *Program) CollectConcludedAtomsInStringFormat() []string {
	atoms := []string{}
	atomSet := map[string]struct{}{}
	// Iterate over the concluded atoms of the program
	for _, abp := range *program.CollectConcludedAtoms() {
		// Add the string format to the set of atoms
		for _, as := range abp.GetAtomStringFormats() {
			atomSet[as] = struct{}{}
		}
	}
	// Copy atoms from the set to the slice
	for a := range atomSet {
		atoms = append(atoms, a)
	}
	return atoms
}

// Cleanup cleans the substitution tables of formulae in strata
func (program *Program) Cleanup() {
	for _, s := range program.Strata {
		s.Cleanup()
	}
}

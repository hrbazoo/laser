package program

import (
	"fmt"
	"strconv"
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
	"bitbucket.org/hrbazoo/laser/stats"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the tests for the package program
 */

func TestProgramRuleReconsolidation(t *testing.T) {
	assert := cassert.New(t)

	rule1 := rule.Parse("h1(X,Y) := p(X,Y), q(Y,X)")
	rule2 := rule.Parse("h1(XX,YY) := q(YY, XX), r(XX, YY)")

	rules := consolidateRulesWithSameHeads([]*rule.Rule{rule1, rule2})

	assert.Equal(len(rules), 1)
	if len(rules) == 1 {
		assert.Equal(len(rules[0].Head), 2)
		assert.Equal(len(rules[0].Body), 2)

		assert.Equal(rules[0].Head[0], rule1.Head[0])
		assert.Equal(rules[0].Head[1], rule2.Head[0])
		assert.Equal(rules[0].Body[0], rule1.Body[0])
		assert.Equal(rules[0].Body[1], rule2.Body[0])
		assert.Equal(rules[0].StringFormat, "h1(X,Y) := p(X,Y), q(Y,X) || h1(XX,YY) := q(YY, XX), r(XX, YY)")
	}
}

func TestProgramCreation(t *testing.T) {
	assert := cassert.New(t)

	r1 := "h1 := p(X,Y), q(Y,X)"
	r2 := "h2 := not(h1)"

	p := New([]string{r1, r2})

	assert.Equal(len(p.Strata), 2)

	if len(p.Strata) == 2 {
		assert.Equal(len(p.PredicateToStrata[types.Predicate("h1")]), 1)
		assert.Equal(p.PredicateToStrata[types.Predicate("h1")], []*stratum.Stratum{p.Strata[1]})
		assert.Equal(len(p.PredicateToStrata[types.Predicate("p")]), 1)
		assert.Equal(p.PredicateToStrata[types.Predicate("p")], []*stratum.Stratum{p.Strata[0]})
		assert.Equal(len(p.PredicateToStrata[types.Predicate("q")]), 1)
		assert.Equal(p.PredicateToStrata[types.Predicate("q")], []*stratum.Stratum{p.Strata[0]})
	}
}

func TestProgramEvaluation1(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	r1 := "h1 := p(X,Y), q(Y,X)"
	r2 := "h2 := not(h1)"

	p := New([]string{r1, r2})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, true)

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)

}

func TestProgramEvaluation2(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	r1 := "h1 := p(X,Y), q(Y,X)"
	r2 := "h2 := not(h1)"

	p := New([]string{r1, r2})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h1")
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h2")
	}

}

func TestProgramEvaluation3(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	r1 := "h1 := timewindim(p(X,Y),2,1), timewindim(q(Y,X), 3, 1)"
	r2 := "h2 := not(h1)"

	p := New([]string{r1, r2})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h1")
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h1")
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h1")
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h2")
	}
}

func TestProgramEvaluation4(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	p := New([]string{
		"h0(X,Y) := timewindim(p0(X,Y), 1, 1), timewinbox(q0(Y,X), 1, 1)",
		"h1(X,Y) := timewindim(p1(X,Y), 1, 1), timewindim(h0(Y,X), 1, 1)",
		"h2(X,Y) := timewindim(p2(X,Y), 1, 1), timewindim(h1(Y,X), 1, 1)",
	})

	atomBatch := data.NewAtomBatch(&[]string{
		"p0(a,b)",
		"p1(b,a)",
		"q0(b,a)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// Time point 1
	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 2)
	if len(resultAtoms) == 2 {
		assert.Equal(resultAtoms[0] == "h0(a,b)" || resultAtoms[1] == "h0(a,b)", true)
		assert.Equal(resultAtoms[0] == "h1(b,a)" || resultAtoms[1] == "h1(b,a)", true)
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Time point 2
	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h1(b,a)")
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Time point 3
	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Time point 4
	atomBatch = data.NewAtomBatch(&[]string{
		"p0(a,b)",
		"p1(b,a)",
		"q0(b,a)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Time point 5
	atomBatch = data.NewAtomBatch(&[]string{
		"p0(a,b)",
		"p1(b,a)",
		"q0(b,a)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 2)
	if len(resultAtoms) == 2 {
		assert.Equal(resultAtoms[0] == "h0(a,b)" || resultAtoms[1] == "h0(a,b)", true)
		assert.Equal(resultAtoms[0] == "h1(b,a)" || resultAtoms[1] == "h1(b,a)", true)
	}
	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Time point 6
	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0], "h1(b,a)")
	}
	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Time point 7
	// Re-evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)

	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
}

func TestProgramEvaluation5(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	config.StratifyProgram = false // We write the program is stratified order. So, do not stratify it again
	p := New([]string{
		"h0(X,Y) := timewindim(p0(X,Y), 3, 1), timewinbox(q0(Y,X), 3, 1)",
		"h1(X,Y) := timewindim(p1(X,Y), 3, 1), timewinbox(h0(Y,X), 3, 1)",
	})

	atomBatch := data.NewAtomBatch(&[]string{
		"p0(a,b)",
		"p1(b,a)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// Time point 1
	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, false)

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// Time point 2
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
}

func TestProgramEvaluation6(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	config.StratifyProgram = false // We write the program is stratified order. So, do not stratify it again
	p := New([]string{
		"h0(X,Y) := timewindim(p(X,Y), 2, 1), timewinbox(q(Y,X), 10, 1)",
		"h1(X,Y) := timewindim(p(X,Y), 1, 1), timewindim(h0(X,Y), 1, 1)",
	})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// ------------------------ Time point 1 -------------------------
	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, true)
	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 2)
	if len(resultAtoms) == 2 {
		assert.Equal(resultAtoms[0] == "h0(a,b)" || resultAtoms[1] == "h0(a,b)", true)
		assert.Equal(resultAtoms[0] == "h1(a,b)" || resultAtoms[1] == "h1(a,b)", true)
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 2 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0] == "h1(a,b)", true)
	}
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[1].Body[0].InvalidUntilTimePoint, types.NoTimePoint)
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.NoTimePoint)
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.NoTimePoint)

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 3 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(12))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 4 -------------------------
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(12))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 5 -------------------------
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(12))
}

func TestProgramEvaluation7(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	config.StratifyProgram = false // We write the program is stratified order. So, do not stratify it again
	p := New([]string{
		"h0(X,Y) := timewindim(p(X,Y), 2, 1), timewinbox(q(Y,X), 10, 1)",
		"h1(X,Y) := timewindim(p(X,Y), 1, 1), timewindim(h0(X,Y), 1, 1)",
		"h2(X,Y) := timewindim(p(X,Y), 1, 1), timewindim(h1(X,Y), 1, 1)",
	})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// ------------------------ Time point 1 -------------------------
	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, true)
	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 3)
	if len(resultAtoms) == 3 {
		assert.Equal(resultAtoms[0] == "h0(a,b)" || resultAtoms[1] == "h0(a,b)" || resultAtoms[2] == "h0(a,b)", true)
		assert.Equal(resultAtoms[0] == "h1(a,b)" || resultAtoms[1] == "h1(a,b)" || resultAtoms[2] == "h1(a,b)", true)
		assert.Equal(resultAtoms[0] == "h2(a,b)" || resultAtoms[1] == "h2(a,b)" || resultAtoms[2] == "h2(a,b)", true)
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 2 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, true)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 2)
	if len(resultAtoms) == 2 {
		assert.Equal(resultAtoms[0] == "h1(a,b)" || resultAtoms[1] == "h1(a,b)", true)
		assert.Equal(resultAtoms[0] == "h2(a,b)" || resultAtoms[1] == "h2(a,b)", true)
	}
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[1].Body[0].InvalidUntilTimePoint, types.NoTimePoint)
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.NoTimePoint)
	assert.CEqual(p.Strata[0].Rules[2].Body[0].InvalidUntilTimePoint, types.NoTimePoint)
	assert.CEqual(p.Strata[0].Rules[2].InvalidUntilTimePoint, types.NoTimePoint)
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.NoTimePoint)

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 3 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	// assert.Equal(evalResult, false)
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 1)
	if len(resultAtoms) == 1 {
		assert.Equal(resultAtoms[0] == "h2(a,b)", true)
	}

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 4 -------------------------
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[2].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(12))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 5 -------------------------
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[2].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(12))
}

func TestProgramEvaluation8(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	config.StratifyProgram = false // We write the program is stratified order. So, do not stratify it again
	p := New([]string{
		"h0(X,Y) := timewindim(p(X,Y), 2, 1), timewinbox(q(Y,X), 10, 1)",
		"h1(X,Y) := timewindim(p(X,Y), 1, 1), timewindim(h0(X,Y), 1, 1)",
		"h2(X,Y) := timewindim(p(X,Y), 1, 1), timewindim(h1(X,Y), 1, 1)",
	})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// ------------------------ Time point 1 -------------------------
	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[2].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[2].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(11))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 2 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[2].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[2].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(11))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()
}

func TestProgramEvaluation9(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	config.StratifyProgram = false // We write the program is stratified order. So, do not stratify it again
	p := New([]string{
		"h0(X,Y) := timewindim(p(X,Y), 2, 1), timewinbox(q(Y,X), 10, 1)",
		"h1(X,Y) := timewindim(p(X,Y), 1, 1), timewinbox(h0(X,Y), 1, 1)",
		"h2(X,Y) := timewindim(p(X,Y), 1, 1), timewinbox(h1(X,Y), 1, 1)",
	})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
	})

	p.InsertMultipleAtomBatches(&atomBatch)

	// ------------------------ Time point 1 -------------------------
	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[2].Body[0].InvalidUntilTimePoint, types.TimePoint(13))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[2].InvalidUntilTimePoint, types.TimePoint(13))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(11))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 2 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].Body[0].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[2].Body[0].InvalidUntilTimePoint, types.TimePoint(13))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(11))
	assert.CEqual(p.Strata[0].Rules[1].InvalidUntilTimePoint, types.TimePoint(12))
	assert.CEqual(p.Strata[0].Rules[2].InvalidUntilTimePoint, types.TimePoint(13))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(11))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()
}

func TestProgramEvaluation10(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	stats.ExecutionProfile.TotalNumberOfFormulaEvaluations = 0
	config.StratifyProgram = false // We write the program is stratified order. So, do not stratify it again
	p := New([]string{
		"h(X,Y) := timewindim(p(X,Y), 3, 1), timewinbox(q(Y,X), 3, 1)",
	})

	// ------------------------ Time point 1 -------------------------
	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult := p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms := p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(2))
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(4))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 2 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	if config.UseInvalidationPrediction {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(2))
	} else {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(4))
	}
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(4))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 3 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	if config.UseInvalidationPrediction {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(2))
	} else {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(6))
	}
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(4))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 4 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	if config.UseInvalidationPrediction {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(2))
	} else {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(8))
	}
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(4))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(4))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()

	// ------------------------ Time point 5 -------------------------
	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
	})
	p.InsertMultipleAtomBatches(&atomBatch)
	// Evaluate the program
	evalResult = p.Evaluate()
	assert.Equal(evalResult, false)
	// Collect evaluated results
	resultAtoms = p.CollectConcludedAtomsInStringFormat()
	assert.Equal(len(resultAtoms), 0)
	if config.UseInvalidationPrediction {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(4))
	} else {
		assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(10))
	}
	assert.CEqual(p.Strata[0].Rules[0].Body[0].InvalidUntilTimePoint, types.TimePoint(8))
	assert.CEqual(p.Strata[0].Rules[0].InvalidUntilTimePoint, types.TimePoint(8))
	assert.CEqual(p.Strata[0].InvalidUntilTimePoint, types.TimePoint(8))

	// Remove expired records and tokens
	p.Cleanup()
	// Advance to the next time point
	clock.GoToNextTimePoint()
}
func TestProgramEvaluation11(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	const lastTimePoint = 10
	const nAtoms = 1
	const winSize = 2

	stats.ExecutionProfile.TotalNumberOfFormulaEvaluations = 0

	rules := []string{
		fmt.Sprintf("h(X,Y) := timewindim(p(X,Y), %d, 1), timewinbox(q(X,Y), %d, 1)", winSize, winSize),
	}

	prog := New(rules)

	numBoxHolds := 1
	for t := 1; t <= lastTimePoint; t++ {
		var predicates = []types.Predicate{}
		if t > 1 && t%winSize != 0 {
			predicates = []types.Predicate{"p", "q"}
		} else {
			predicates = []types.Predicate{"p"}

		}
		atoms := []string{}
		for i, p := range predicates {
			for j := 0; i < nAtoms; i++ {
				c1 := i
				c2 := j
				atomString := string(p) + "(" + strconv.Itoa(c1) + "," + strconv.Itoa(c2) + ")"
				atoms = append(atoms, atomString)
			}
		}

		atomBatch := data.NewAtomBatch(&atoms)
		prog.InsertMultipleAtomBatches(&atomBatch)

		prog.Evaluate()
		if config.UseInvalidationPrediction {
			assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(numBoxHolds*2))
			if t%(winSize+1) == 0 {
				numBoxHolds++
			}
		} else {
			assert.Equal(stats.ExecutionProfile.TotalNumberOfFormulaEvaluations, uint64(t*2))
		}

		// Remove expired records and tokens
		prog.Cleanup()
		// Advance to the next time point
		clock.GoToNextTimePoint()
	}
}

package program

import (
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation routines that retrieves rule
 * dependencies in the Laser programs.
 */

// retrieveDependencies takes a slice of rules and retrieves dependencies among them.
// Returns two mappings: one between rules that have non-negated dependencies and one
// between rules that have negated dependencies.
// IMPORTANT: this routine assumes that all rules with the same predicate in their head
// formula are already consolidated into a single rule. If this condition is not met, the
// routine's behaviour might be unpredictable and wrong.
func retrieveDependencies(rules []*rule.Rule) (map[*rule.Rule]map[*rule.Rule]struct{}, map[*rule.Rule]map[*rule.Rule]struct{}) {
	headPredicateToRule := map[types.Predicate]*rule.Rule{}
	negatedDependencies := map[*rule.Rule]map[*rule.Rule]struct{}{}
	nonNegatedDependencies := map[*rule.Rule]map[*rule.Rule]struct{}{}

	for _, r := range rules {
		p := r.GetHeadPredicate()
		headPredicateToRule[p] = r
	}

	for _, r := range rules {
		// If the predicate is not already in the mappings, add it to both maps
		_, nonNegatedexists := nonNegatedDependencies[r]
		_, negatedExists := nonNegatedDependencies[r]

		if !nonNegatedexists {
			nonNegatedDependencies[r] = map[*rule.Rule]struct{}{}
		}
		if !negatedExists {
			negatedDependencies[r] = map[*rule.Rule]struct{}{}
		}
		// Iterated over all body parts that are in disjunction relation with each other
		for _, cf := range r.Body {
			// iterated over all formulae that are joined together
			for _, f := range cf.Formulae {
				// Conclude from the predicate of the formula to what other rules this rule depends
				dependency, exists := headPredicateToRule[f.GetPredicate()]
				// If the predicate does not exists in the mapping from the predicate to rule, it means that
				// there is no dependency between this rule and any other rule via this formula. So, ignore the
				// formula and continue
				if !exists {
					continue
				}
				// the formula is not negated?
				if !f.IsNegated() {
					// If we have already established a negated dependency between this rule and the predicate
					// ignore the non-negated dependencies. Otherwise, establish the non-negated dependency
					if _, exists := negatedDependencies[r][dependency]; !exists {
						nonNegatedDependencies[r][dependency] = struct{}{}
					}
				} else { // The formula is negated
					// If there is already a non-negated dependency between these rules, remove it
					// so that we can establish a negated dependency
					if _, exists := nonNegatedDependencies[r][dependency]; exists {
						delete(nonNegatedDependencies[r], dependency)
					}

					// Establish the negated dependency between the predicate and the rule
					negatedDependencies[r][dependency] = struct{}{}
				}
			}
			// If there is no dependencies for a rule, remove it
			if len(negatedDependencies[r]) == 0 {
				delete(negatedDependencies, r)
			}
			if len(nonNegatedDependencies[r]) == 0 {
				delete(nonNegatedDependencies, r)
			}
		}
	}

	return nonNegatedDependencies, negatedDependencies
}

package program

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
	"github.com/stretchr/testify/assert"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the test units of dependency creation functionalities
 * of program package
 */

func TestDependencyNum1(t *testing.T) {
	assert := assert.New(t)

	r1 := rule.Parse("h1 := h2, h3")
	r2 := rule.Parse("h3 := not(h4)")
	r3 := rule.Parse("h2 := h4, h5")
	r4 := rule.Parse("h4 := h5, h6")
	r5 := rule.Parse("h5 := h7")

	rules := consolidateRulesWithSameHeads([]*rule.Rule{r1, r2, r3, r4, r5})
	nonNegatedDependencies, negatedDependencies := retrieveDependencies(rules)

	assert.Equal(len(nonNegatedDependencies), 3)
	assert.Equal(len(negatedDependencies), 1)

	for rule := range nonNegatedDependencies {
		if rule.StringFormat != "h1 := h2, h3" && rule.StringFormat != "h2 := h4, h5" && rule.StringFormat != "h4 := h5, h6" {
			assert.Equal(false, true)
		}
		if rule.StringFormat == "h1 := h2, h3" {
			dependencies := nonNegatedDependencies[rule]
			assert.Equal(len(dependencies), 2)
			for r := range dependencies {
				if r.StringFormat != "h2 := h4, h5" && r.StringFormat != "h3 := not(h4)" {
					assert.Equal(false, true)
				}
			}
		}
		if rule.StringFormat == "h2 := h4, h5" {
			dependencies := nonNegatedDependencies[rule]
			assert.Equal(len(dependencies), 2)
			for r := range dependencies {
				if r.StringFormat != "h4 := h5, h6" && r.StringFormat != "h5 := h7" {
					assert.Equal(false, true)
				}
			}
		}
		if rule.StringFormat == "h4 := h5, h6" {
			dependencies := nonNegatedDependencies[rule]
			assert.Equal(len(dependencies), 1)
			for r := range dependencies {
				if r.StringFormat != "h5 := h7" {
					assert.Equal(false, true)
				}
			}
		}
	}
	for rule := range negatedDependencies {
		assert.Equal(rule.StringFormat, "h3 := not(h4)")
		dependencies := negatedDependencies[rule]
		assert.Equal(len(dependencies), 1)
		for r := range dependencies {
			if r.StringFormat != "h4 := h5, h6" {
				assert.Equal(false, true)
			}
		}
	}
}

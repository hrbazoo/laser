package program

import (
	"sort"

	"bitbucket.org/hrbazoo/laser/program/stratum"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation routines that stratify Laser programs
 */

// stratify takes an array of rules and stratifies them into a number strata. Returns an array
// of strata.
func stratify(inputRules []*rule.Rule) []*stratum.Stratum {
	// We have to consolidate all rules with the same head predicate into a single rule. This is
	// essential for the logic of Laser's advanced optimizations.
	rules := consolidateRulesWithSameHeads(inputRules)
	// Retrive the dependencies among rules. We need this for the stratification
	nonNegatedDependencies, negatedDependencies := retrieveDependencies(rules)
	// If there is cyclic negated relation between rules, throw an error message.
	if hasCyclicalNegatedDependencies(negatedDependencies) {
		panic("ERROR: there are cyclical negated relations between rules.")
	}
	// Stratification Numbers
	sNumberSet := map[*rule.Rule]int{}
	for _, r := range rules {
		sNumberSet[r] = 0
	}
	// We must keep on iterating over rules until no new stratification number is calculated
	for {
		calcNewStratificationNumber := false
		for _, r := range rules {
			// If there is a negated relation between the rule and its dependencies, increment the stratification numbers
			// of its dependencies
			if deps, exists := negatedDependencies[r]; exists {
				for d := range deps {
					// If the stratification number of the rule is littler than it's dependencies,
					// assign a larger stratification number to the dependencies
					if sNumberSet[d] <= sNumberSet[r] {
						calcNewStratificationNumber = true
						sNumberSet[d] = sNumberSet[r] + 1
					}
				}
			}
			if deps, exists := nonNegatedDependencies[r]; exists {
				for d := range deps {
					// If the stratification number of the rule is littler than it's dependencies,
					// assign a larger stratification number to the dependencies
					if sNumberSet[d] < sNumberSet[r] {
						calcNewStratificationNumber = true
						sNumberSet[d] = sNumberSet[r]
					}
				}
			}
		}
		if !calcNewStratificationNumber {
			break
		}
	}

	// Groups rules by their stratification number
	strataRules := map[int][]*rule.Rule{}
	for r, sn := range sNumberSet {
		if _, exists := strataRules[sn]; !exists {
			strataRules[sn] = []*rule.Rule{}
		}
		strataRules[sn] = append(strataRules[sn], r)
	}

	// Get stratification numbers and sort them
	sNumbers := []int{}
	for sn := range strataRules {
		sNumbers = append(sNumbers, sn)
	}
	sort.Ints(sNumbers)

	// Create the strata and return them
	strata := []*stratum.Stratum{}
	for i := len(sNumbers) - 1; i >= 0; i-- {
		strata = append(strata, stratum.New(strataRules[sNumbers[i]]))
	}
	// for _, s := range strata {
	// 	fmt.Printf("--------------------------------\n")
	// 	for _, r := range s.Rules {
	// 		fmt.Printf(">>>>>>>>>>>>>>>>>> %s\n", r.StringFormat)
	// 	}
	// }
	return strata
}

// Takes a number of rules and checks whether there is any cyclical negated dependencies
// between rules
func hasCyclicalNegatedDependencies(ruleNegatedDependencies map[*rule.Rule]map[*rule.Rule]struct{}) bool {
	// If any of dependencies of a particular rule is also in a negated relation with the rule,
	// that establishes a negated cyclic relation which makes stratification impossible. We must
	// not allow that.
	for r, deps := range ruleNegatedDependencies {
		// Iterate over the negated dependencies of this rule
		for d := range deps {
			// If the rule is in the set of rules that are in negated relation with the current dependency,
			// we have found a cyclical negated relation
			if rules, exists := ruleNegatedDependencies[d]; exists {
				if _, exists := rules[r]; exists {
					return true
				}
			}
		}
	}
	return false
}

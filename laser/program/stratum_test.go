package program

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
	"github.com/stretchr/testify/assert"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the tests for the stratification functionalities of Program package
 */

// This function is used to test in cases where the program is expected to panic
func assertPanic(t *testing.T, f func()) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	f()
}
func TestStratificationNum1(t *testing.T) {
	assert := assert.New(t)

	r1 := rule.Parse("h1 := h2")
	r2 := rule.Parse("h2 := h3, h4")
	r3 := rule.Parse("h3 := h5")

	s := stratify([]*rule.Rule{r1, r2, r3})

	assert.Equal(len(s), 1)
	if len(s) == 1 {
		assert.Equal(len(s[0].Rules), 3)
		if len(s[0].Rules) == 3 {
			assert.Equal(true, (s[0].Rules[0].StringFormat == "h3 := h5" || s[0].Rules[0].StringFormat == "h2 := h3, h4" || s[0].Rules[0].StringFormat == "h1 := h2") &&
				(s[0].Rules[1].StringFormat == "h3 := h5" || s[0].Rules[1].StringFormat == "h2 := h3, h4" || s[0].Rules[1].StringFormat == "h1 := h2") &&
				(s[0].Rules[2].StringFormat == "h3 := h5" || s[0].Rules[2].StringFormat == "h2 := h3, h4" || s[0].Rules[2].StringFormat == "h1 := h2"))
		}
	}
}

func TestStratificationNum1_1(t *testing.T) {
	assert := assert.New(t)

	r1 := rule.Parse("h1(X) := h2(X)")
	r2 := rule.Parse("h2(X,Y) := h3(X,Y), h4(Y,X)")
	r3 := rule.Parse("h3(X,Y) := h5(Y,X)")

	s := stratify([]*rule.Rule{r1, r2, r3})

	assert.Equal(len(s), 1)
	if len(s) == 1 {
		assert.Equal(len(s[0].Rules), 3)
		if len(s[0].Rules) == 3 {
			assert.Equal(true, (s[0].Rules[0].StringFormat == "h3(X,Y) := h5(Y,X)" || s[0].Rules[0].StringFormat == "h2(X,Y) := h3(X,Y), h4(Y,X)" || s[0].Rules[0].StringFormat == "h1(X) := h2(X)") &&
				(s[0].Rules[1].StringFormat == "h3(X,Y) := h5(Y,X)" || s[0].Rules[1].StringFormat == "h2(X,Y) := h3(X,Y), h4(Y,X)" || s[0].Rules[1].StringFormat == "h1(X) := h2(X)") &&
				(s[0].Rules[2].StringFormat == "h3(X,Y) := h5(Y,X)" || s[0].Rules[2].StringFormat == "h2(X,Y) := h3(X,Y), h4(Y,X)" || s[0].Rules[2].StringFormat == "h1(X) := h2(X)"))
		}
	}
}

func TestStratificationNum2(t *testing.T) {
	assert := assert.New(t)

	r1 := rule.Parse("h1 := not(h2)")
	r2 := rule.Parse("h2 := h3, h4")
	r3 := rule.Parse("h3 := h5")

	s := stratify([]*rule.Rule{r1, r2, r3})
	assert.Equal(len(s), 2)
	if len(s) == 3 {
		assert.Equal(len(s[0].Rules), 2)
		if len(s[0].Rules) == 2 {
			assert.Equal(true, (s[0].Rules[0].StringFormat == "h3 := h5" || s[0].Rules[0].StringFormat == "h2 := h3, h4") &&
				(s[0].Rules[1].StringFormat == "h3 := h5" || s[0].Rules[1].StringFormat == "h2 := h3, h4"))
		}
		assert.Equal(len(s[1].Rules), 1)
		if len(s[1].Rules) == 1 {
			assert.Equal(s[1].Rules[0].StringFormat, "h1 := not(h2)")
		}
	}
}

func TestStratificationNum3(t *testing.T) {

	r1 := rule.Parse("h1 := not(h2)")
	r2 := rule.Parse("h2 := not(h1)")

	assertPanic(t, func() { stratify([]*rule.Rule{r1, r2}) })
}

func TestStratificationNum4(t *testing.T) {
	assert := assert.New(t)

	r1 := rule.Parse("h1 := not(h2)")
	r2 := rule.Parse("h2 := not(h3)")
	r3 := rule.Parse("h3 := h4, h5")
	r4 := rule.Parse("h5 := h6")

	s := stratify([]*rule.Rule{r1, r2, r3, r4})
	assert.Equal(len(s), 3)
	if len(s) == 3 {
		assert.Equal(len(s[0].Rules), 2)
		if len(s[0].Rules) == 2 {
			assert.Equal(true, (s[0].Rules[0].StringFormat == "h5 := h6" || s[0].Rules[0].StringFormat == "h3 := h4, h5") &&
				(s[0].Rules[1].StringFormat == "h5 := h6" || s[0].Rules[1].StringFormat == "h3 := h4, h5"))
		}
		assert.Equal(len(s[1].Rules), 1)
		if len(s[1].Rules) == 1 {
			assert.Equal(s[1].Rules[0].StringFormat, "h2 := not(h3)")
		}
		assert.Equal(len(s[2].Rules), 1)
		if len(s[2].Rules) == 1 {
			assert.Equal(s[2].Rules[0].StringFormat, "h1 := not(h2)")
		}
	}
}

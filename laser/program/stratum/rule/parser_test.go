package rule

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/timewindim"

	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the test units of rule parser
 */
func TestParserHeadAndBody(t *testing.T) {
	assert := cassert.New(t)
	head, body := separateHeadAndBody("p := q(X,Y), r(Y,Z)")

	assert.Equal(head, "p")
	assert.Equal(body, "q(X,Y), r(Y,Z)")
}

func TestParserTokenize1(t *testing.T) {
	assert := cassert.New(t)
	tokens := tokenize("p(X,Y)")
	rightTokens := []string{"p", "(", "X", ",", "Y", ")"}
	assert.Equal(len(tokens), 6)
	for i, t := range tokens {
		assert.Equal(t, rightTokens[i])
	}
}

func TestParserTokenize2(t *testing.T) {
	assert := cassert.New(t)
	tokens := tokenize("\tp   (X ,   Y\n) ")
	rightTokens := []string{"p", "(", "X", ",", "Y", ")"}
	assert.Equal(len(tokens), len(rightTokens))
	for i, t := range tokens {
		assert.Equal(t, rightTokens[i])
	}
}

func TestParserTokenizer(t *testing.T) {
	assert := cassert.New(t)
	headTokens, bodyTokens := tokenizer("\tp   (X ,   Y\n) := q(X),\tr(X, Y)")
	rightHeadTokens := []string{"p", "(", "X", ",", "Y", ")"}
	rightBodyTokens := []string{"q", "(", "X", ")", ",", "r", "(", "X", ",", "Y", ")"}

	assert.Equal(len(headTokens), len(rightHeadTokens))
	assert.Equal(len(bodyTokens), len(rightBodyTokens))

	for i, t := range headTokens {
		assert.Equal(t, rightHeadTokens[i])
	}
	for i, t := range bodyTokens {
		assert.Equal(t, rightBodyTokens[i])
	}
}

func TestParserParseAtomFormula1(t *testing.T) {
	assert := cassert.New(t)
	tokens := tokenize("\tp   (X ,   Y\n)")

	predicate, variables, nextIndex := parseAtom(tokens)

	assert.Equal(predicate, types.Predicate("p"))
	assert.Equal(len(variables), 2)
	assert.Equal(nextIndex, 6)
	if len(variables) == 2 {
		assert.Equal(variables[0], types.Variable("X"))
		assert.Equal(variables[1], types.Variable("Y"))
	}
}

func TestParserParseAtomFormula2(t *testing.T) {
	assert := cassert.New(t)
	tokens := tokenize("\tp   (X)")

	predicate, variables, nextIndex := parseAtom(tokens)

	assert.Equal(predicate, types.Predicate("p"))
	assert.Equal(len(variables), 1)
	assert.Equal(nextIndex, 4)
	if len(variables) == 1 {
		assert.Equal(variables[0], types.Variable("X"))
	}
}

func TestParserParseAtomFormula3(t *testing.T) {
	assert := cassert.New(t)
	tokens := tokenize("\tp")

	predicate, variables, nextIndex := parseAtom(tokens)

	assert.Equal(predicate, types.Predicate("p"))
	assert.Equal(nextIndex, 1)
	assert.Equal(len(variables), 0)
}

func TestParserParseFormula1(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("\ttimewindim(p   (X,Y), 3, 1)")

	predicate, variables, winLen, stepSize, nextIndex := parseTimeWinOperator(tokens[1:])

	assert.Equal(predicate, types.Predicate("p"))
	assert.Equal(len(variables), 2)
	assert.Equal(nextIndex, 12) // -1 because the timewindim is not sent to parseTimeWinOperator
	if len(variables) == 2 {
		assert.Equal(variables[0], types.Variable("X"))
		assert.Equal(variables[1], types.Variable("Y"))
	}
	assert.Equal(winLen, 3)
	assert.Equal(stepSize, 1)
}

func TestParserParseFormula2(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("\ttimewindim(p, 3, 1)")

	predicate, variables, winLen, stepSize, nextIndex := parseTimeWinOperator(tokens[1:])

	assert.Equal(predicate, types.Predicate("p"))
	assert.Equal(len(variables), 0)
	assert.Equal(nextIndex, 7) // -1 because the timewindim is not sent to parseTimeWinOperator
	assert.Equal(winLen, 3)
	assert.Equal(stepSize, 1)
}

func TestParserParseFormula3(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("\ttimewindim(p, 3, 1)")

	f, nextIndex := parseFormula(tokens)
	assert.Equal(f.GetPredicate(), types.Predicate("p"))
	assert.Equal(len(f.GetVariableToRecordPositions()), 0)
	assert.Equal(nextIndex, 8) // -1 because the timewindim is not sent to parseTimeWinOperator
	assert.Equal(f.(*timewindim.TimeWindowDiamondFormula).WindowLength, 3)
}

func TestParserParseFormula4(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("\ttimewindim(p   (X,Y), 3, 1)")

	f, nextIndex := parseFormula(tokens)
	assert.Equal(f.GetPredicate(), types.Predicate("p"))
	assert.Equal(len(f.GetVariableToRecordPositions()), 2)
	assert.Equal(nextIndex, 13)
	assert.Equal(f.(*timewindim.TimeWindowDiamondFormula).WindowLength, 3)
	if len(f.GetVariableToRecordPositions()) == 2 {
		i, ok := f.GetVariableToRecordPositions()["X"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 0)
		i, ok = f.GetVariableToRecordPositions()["Y"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 1)
	}
}

func TestParserParseFormula5(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("\ttimewindim(p   (X,Y, X), 3, 1)")

	f, nextIndex := parseFormula(tokens)
	assert.Equal(f.GetPredicate(), types.Predicate("p"))
	assert.Equal(len(f.GetVariableToRecordPositions()), 2)
	assert.Equal(nextIndex, 15)
	assert.Equal(f.(*timewindim.TimeWindowDiamondFormula).WindowLength, 3)
	if len(f.GetVariableToRecordPositions()) == 2 {
		i, ok := f.GetVariableToRecordPositions()["X"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 0)
		assert.Equal(i[1], 2)
		i, ok = f.GetVariableToRecordPositions()["Y"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 1)
	}
}

func TestParserParseBody1(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("\ttimewindim(p   (X,Y, X), 3, 1), q(X,Y), r")

	cf := parseRuleBody(tokens)
	assert.Equal(len(cf.Formulae), 3)

	if len(cf.Formulae) == 3 {
		assert.Equal(cf.Formulae[0].GetPredicate(), types.Predicate("p"))
		assert.Equal(len(cf.Formulae[0].GetVariableToRecordPositions()), 2)
		assert.Equal(cf.Formulae[0].(*timewindim.TimeWindowDiamondFormula).WindowLength, 3)
		if len(cf.Formulae[0].GetVariableToRecordPositions()) == 2 {
			i, ok := cf.Formulae[0].GetVariableToRecordPositions()["X"]
			assert.Equal(ok, true)
			assert.Equal(i[0], 0)
			assert.Equal(i[1], 2)
			i, ok = cf.Formulae[0].GetVariableToRecordPositions()["Y"]
			assert.Equal(ok, true)
			assert.Equal(i[0], 1)
		}

		assert.Equal(cf.Formulae[1].GetPredicate(), types.Predicate("q"))
		assert.Equal(len(cf.Formulae[1].GetVariableToRecordPositions()), 2)
		if len(cf.Formulae[1].GetVariableToRecordPositions()) == 2 {
			i, ok := cf.Formulae[1].GetVariableToRecordPositions()["X"]
			assert.Equal(ok, true)
			assert.Equal(i[0], 0)
			i, ok = cf.Formulae[1].GetVariableToRecordPositions()["Y"]
			assert.Equal(ok, true)
			assert.Equal(i[0], 1)
		}

		assert.Equal(cf.Formulae[2].GetPredicate(), types.Predicate("r"))
		assert.Equal(len(cf.Formulae[2].GetVariableToRecordPositions()), 0)
	}
}

func TestParserParseBody2(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("\tr")

	cf := parseRuleBody(tokens)
	assert.Equal(len(cf.Formulae), 1)

	if len(cf.Formulae) == 1 {
		assert.Equal(cf.Formulae[0].GetPredicate(), types.Predicate("r"))
		assert.Equal(len(cf.Formulae[0].GetVariableToRecordPositions()), 0)
	}
}

func TestParserParseBody3(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("q(X,Y), r")

	cf := parseRuleBody(tokens)
	assert.Equal(len(cf.Formulae), 2)

	if len(cf.Formulae) == 2 {
		assert.Equal(cf.Formulae[0].GetPredicate(), types.Predicate("q"))
		assert.Equal(len(cf.Formulae[0].GetVariableToRecordPositions()), 2)
		if len(cf.Formulae[0].GetVariableToRecordPositions()) == 2 {
			i, ok := cf.Formulae[0].GetVariableToRecordPositions()["X"]
			assert.Equal(ok, true)
			assert.Equal(i[0], 0)
			i, ok = cf.Formulae[0].GetVariableToRecordPositions()["Y"]
			assert.Equal(ok, true)
			assert.Equal(i[0], 1)
		}

		assert.Equal(cf.Formulae[1].GetPredicate(), types.Predicate("r"))
		assert.Equal(len(cf.Formulae[1].GetVariableToRecordPositions()), 0)
	}
}

func TestParserParseHead1(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("h(X,Y)")

	f := parseRuleHead(tokens)

	assert.Equal(f.GetPredicate(), types.Predicate("h"))
	assert.Equal(len(f.GetVariableToRecordPositions()), 2)
	if len(f.GetVariableToRecordPositions()) == 2 {
		i, ok := f.GetVariableToRecordPositions()["X"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 0)
		i, ok = f.GetVariableToRecordPositions()["Y"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 1)
	}
}

func TestParserParseHead2(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("h(X)")

	f := parseRuleHead(tokens)

	assert.Equal(f.GetPredicate(), types.Predicate("h"))
	assert.Equal(len(f.GetVariableToRecordPositions()), 1)
	if len(f.GetVariableToRecordPositions()) == 1 {
		i, ok := f.GetVariableToRecordPositions()["X"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 0)
	}
}

func TestParserParseHead3(t *testing.T) {
	assert := cassert.New(t)

	tokens := tokenize("h")

	f := parseRuleHead(tokens)

	assert.Equal(f.GetPredicate(), types.Predicate("h"))
	assert.Equal(len(f.GetVariableToRecordPositions()), 0)
}

func TestParserRule1(t *testing.T) {
	assert := cassert.New(t)

	r := Parse("h(X,Y) := p(X,Y), q(Y,X)")

	assert.Equal(r.Head[0].GetPredicate(), types.Predicate("h"))
	assert.Equal(len(r.Head[0].GetVariableToRecordPositions()), 2)
	if len(r.Head[0].GetVariableToRecordPositions()) == 2 {
		i, ok := r.Head[0].GetVariableToRecordPositions()["X"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 0)
		i, ok = r.Head[0].GetVariableToRecordPositions()["Y"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 1)
	}

	assert.Equal(len(r.Body), 1)
	if len(r.Body) == 1 {
		cf := r.Body[0].Formulae
		assert.Equal(len(cf), 2)
		if len(cf) == 2 {
			assert.Equal(cf[0].GetPredicate(), types.Predicate("p"))
			assert.Equal(cf[1].GetPredicate(), types.Predicate("q"))
			if len(cf[0].GetVariableToRecordPositions()) == 2 {
				i, ok := cf[0].GetVariableToRecordPositions()["X"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 0)
				i, ok = cf[0].GetVariableToRecordPositions()["Y"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 1)
			}
			if len(cf[1].GetVariableToRecordPositions()) == 2 {
				i, ok := cf[1].GetVariableToRecordPositions()["Y"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 0)
				i, ok = cf[1].GetVariableToRecordPositions()["X"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 1)
			}
		}
	}
}

func TestParserRule2(t *testing.T) {
	assert := cassert.New(t)

	r := Parse("h(X) := timewindim(p(X,Y), 1, 1), q(Y,X)")

	assert.Equal(r.Head[0].GetPredicate(), types.Predicate("h"))
	assert.Equal(len(r.Head[0].GetVariableToRecordPositions()), 1)
	if len(r.Head[0].GetVariableToRecordPositions()) == 1 {
		i, ok := r.Head[0].GetVariableToRecordPositions()["X"]
		assert.Equal(ok, true)
		assert.Equal(i[0], 0)
	}

	assert.Equal(len(r.Body), 1)
	if len(r.Body) == 1 {
		cf := r.Body[0].Formulae
		assert.Equal(len(cf), 2)
		if len(cf) == 2 {
			assert.Equal(cf[0].GetPredicate(), types.Predicate("p"))
			assert.Equal(cf[1].GetPredicate(), types.Predicate("q"))
			assert.Equal(cf[0].(*timewindim.TimeWindowDiamondFormula).WindowLength, 1)
			if len(cf[0].GetVariableToRecordPositions()) == 2 {
				i, ok := cf[0].GetVariableToRecordPositions()["X"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 0)
				i, ok = cf[0].GetVariableToRecordPositions()["Y"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 1)
			}
			if len(cf[1].GetVariableToRecordPositions()) == 2 {
				i, ok := cf[1].GetVariableToRecordPositions()["Y"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 0)
				i, ok = cf[1].GetVariableToRecordPositions()["X"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 1)
			}
		}
	}
}

func TestParserRule3(t *testing.T) {
	assert := cassert.New(t)

	r := Parse("h := timewindim(p(X,Y), 1, 1)")

	assert.Equal(r.Head[0].GetPredicate(), types.Predicate("h"))
	assert.Equal(len(r.Head[0].GetVariableToRecordPositions()), 0)

	assert.Equal(len(r.Body), 1)
	if len(r.Body) == 1 {
		cf := r.Body[0].Formulae
		assert.Equal(len(cf), 1)
		if len(cf) == 1 {
			assert.Equal(cf[0].GetPredicate(), types.Predicate("p"))
			assert.Equal(cf[0].(*timewindim.TimeWindowDiamondFormula).WindowLength, 1)
			if len(cf[0].GetVariableToRecordPositions()) == 2 {
				i, ok := cf[0].GetVariableToRecordPositions()["X"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 0)
				i, ok = cf[0].GetVariableToRecordPositions()["Y"]
				assert.Equal(ok, true)
				assert.Equal(i[0], 1)
			}
		}
	}
}

func TestParserRule4(t *testing.T) {
	assert := cassert.New(t)

	r := Parse("h := not(p)")

	assert.Equal(r.Head[0].GetPredicate(), types.Predicate("h"))
	assert.Equal(len(r.Head[0].GetVariableToRecordPositions()), 0)

	assert.Equal(len(r.Body), 1)
	if len(r.Body) == 1 {
		cf := r.Body[0].Formulae
		assert.Equal(len(cf), 1)
		assert.Equal(cf[0].GetFormulaType(), formula.NotFormulaTypeName)
		if len(cf) == 1 {
			assert.Equal(cf[0].GetPredicate(), types.Predicate("p"))
		}
	}
}

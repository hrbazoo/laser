package join

import (
	"math"

	"bitbucket.org/hrbazoo/laser/clock"

	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of join operations that involve substitution tables
 */

// Takes two formulae and returns the variable names that are common between them
func findCommonVariable(table1 *substitutiontable.SubstitutionTable, table2 *substitutiontable.SubstitutionTable) *[]types.Variable {
	t1Vars := table1.VariableToRecordIndex // Variables of the first table
	t2Vars := table2.VariableToRecordIndex // Variables of the second table

	commonVariables := []types.Variable{} // Holds common variables of the two formulae

	// Find out what variables from the second formula exist in the first formula
	for v := range t1Vars {
		// If the variable exists in the first formula, add it to the list of common variables
		if _, exists := t2Vars[v]; exists {
			commonVariables = append(commonVariables, v)
		}
	}

	// Return common variables
	return &commonVariables
}

// Takes an array (slice) of sorted substitution tables and picks the two smallest tables that
// have at least one variables in common along with their common variables. Returns the two selected tables
func chooseSmallestTablesWithCommonVariables(tables *[]*substitutiontable.SubstitutionTable) (*substitutiontable.SubstitutionTable, *substitutiontable.SubstitutionTable, *[]types.Variable) {
	// We expect at least two elements in the array
	if len(*tables) < 2 {
		panic("Didn't expect an array of tables that is smaller than two elements long")
	}

	// If there are only two elements in the array, return them immediately
	if len(*tables) == 2 {
		return (*tables)[0], (*tables)[1], findCommonVariable((*tables)[0], (*tables)[1])
	}

	// Iterate over tables, beginning from the smallest to the largest and pick the two
	// smallest tables that have at least one common variable. Then, the smaller table is
	// returned as the left table in the join and larger one as the right table.
	for leftIndex, leftTable := range *tables {
		for _, rightTable := range (*tables)[leftIndex+1:] {
			commonVariables := findCommonVariable(leftTable, rightTable)
			if len(*commonVariables) > 0 {
				return leftTable, rightTable, commonVariables
			}
		}
	}

	// Couldn't find tables with common variables? Return the first two tables with a nil as common variables
	return (*tables)[0], (*tables)[1], nil
}

// Takes the two tables that are going to be joined together along with the common variables among them and
// creates the table that can store the results of joining the two tables together
func makeResultSubstitutionTable(leftTable *substitutiontable.SubstitutionTable, rightTable *substitutiontable.SubstitutionTable, jointVariables *[]types.Variable) *substitutiontable.SubstitutionTable {
	leftTableVariableToRecordPositions := leftTable.VariableToRecordIndex
	rightTableVariableToRecordPositions := rightTable.VariableToRecordIndex

	allVariables := []types.Variable{}
	resultRecordVariableToPosition := map[types.Variable]struct{}{}

	// Determine the variables of the join result table. First, insert variables that are involved in the join
	for _, v := range *jointVariables {
		allVariables = append(allVariables, v)
		resultRecordVariableToPosition[v] = struct{}{}
	}
	// Add the uninvolved variables of the left table
	for v := range leftTableVariableToRecordPositions {
		if _, exists := resultRecordVariableToPosition[v]; !exists {
			allVariables = append(allVariables, v)
			resultRecordVariableToPosition[v] = struct{}{}
		}
	}
	// Add the uninvolved variables of the right table
	for v := range rightTableVariableToRecordPositions {
		if _, exists := resultRecordVariableToPosition[v]; !exists {
			allVariables = append(allVariables, v)
			resultRecordVariableToPosition[v] = struct{}{}
		}
	}

	// Create the result substitution table
	resultTable := substitutiontable.New(allVariables)

	// Return the result substitution table
	return resultTable
}

// makeResultSubstitutionRecord takes the map from the variable to the record positions of the result table of the join
// as well as the map from the variables to record positions of either participating tables in the table and the left & right
// records in the current join operations and creates the record which results from joining the two records.
func makeResultSubstitutionRecord(resultTableVariableToRecordPositions *map[types.Variable][]int,
	leftTableVariableToRecordIndex *map[types.Variable][]int,
	rightTableVariableToRecordIndex *map[types.Variable][]int,
	leftRecord *substitutiontable.SubstitutionRecord,
	rightRecord *substitutiontable.SubstitutionRecord) *substitutiontable.SubstitutionRecord {

	// Substitutions of the resulting record
	resultRecordSubstitutions := make([]types.Substitution, len(*resultTableVariableToRecordPositions))

	// Insert substitutions of the left record in the result record
	for v, leftRecordIndex := range *leftTableVariableToRecordIndex {
		// If there are more than one instance of the same variable in the record (and thus, we may have more than substitution positions),
		// they must contain the same substitution so that we reach this point in join. Therefore, we can safely assume that the substitution
		// in the first index(=0) is the substitution that we insert in the resulting record.
		resultRecordIndex := (*resultTableVariableToRecordPositions)[v][0]
		// Add the substitution to the result record
		resultRecordSubstitutions[resultRecordIndex] = leftRecord.Substitutions[leftRecordIndex[0]]
	}

	// Insert substitutions of the right record in the result record
	for v, rightRecordIndex := range *rightTableVariableToRecordIndex {
		// If there are more than one instance of the same variable in the record (and thus, we may have more than substitution positions),
		// they must contain the same substitution so that we reach this point in join. Therefore, we can safely assume that the substitution
		// in the first index(=0) is the substitution that we insert in the resulting record.
		resultRecordIndex := (*resultTableVariableToRecordPositions)[v][0]
		// Add the substitution to the result record
		resultRecordSubstitutions[resultRecordIndex] = rightRecord.Substitutions[rightRecordIndex[0]]
	}

	// Here, we calculate the consideration and horizon time of the resulting record. The aim is to find the longest time period in which the
	// record certainly holds. Therefore, this time period includes maximum of the consideration times of both records as well as the minimum
	// of the horizon times of both records. Within this time period, the join result certainly holds (because both the left and the right records hold)
	resultRecordConsiderationTime := types.TimePoint(math.Max(float64(leftRecord.ConsiderationTime), float64(rightRecord.ConsiderationTime)))
	resultRecordHorizonTime := types.TimePoint(math.Min(float64(leftRecord.HorizonTime), float64(rightRecord.HorizonTime)))
	// If the horizon time of the record is somewhere in the past, set it to the current time point
	if resultRecordHorizonTime < clock.GetNow() {
		resultRecordHorizonTime = clock.GetNow()
	}

	// Create the result record
	resultRecord := substitutiontable.CreateSubstitutionRecord(&resultRecordSubstitutions, types.TimePoint(resultRecordConsiderationTime), types.TimePoint(resultRecordHorizonTime))

	// Return the result record
	return resultRecord
}

// Takes a (sorted) list of substitution tables, two tables from the list which have been joined together, and one table which
// contains the join result of the two tables. Then, the routine removes the two joined tables from the list and inserts the
// join result in the list such that the list remains sorted.
func insertResultTableAndRemoveJoinedTables(tables *[]*substitutiontable.SubstitutionTable,
	leftTable *substitutiontable.SubstitutionTable,
	rightTable *substitutiontable.SubstitutionTable,
	joinResult *substitutiontable.SubstitutionTable) *[]*substitutiontable.SubstitutionTable {

	if len(*tables) < 2 {
		panic("ERROR: too few tables in the re-sorting of tables")
	}
	// Remove the joined tables from the list of tables and insert the result table in the list
	// such that the new list is still sorted.
	joinResultSize := joinResult.Size()

	// The new list that stores the result. It would be one element shorter than its current length
	newTableList := make([]*substitutiontable.SubstitutionTable, len(*tables)-1)

	i := 0                       // Indicates the current index in the new list of tables
	insertedResultTable := false // Indicates whether the result-table has been inserted in the new list or not
	for _, t := range *tables {
		// Do not include tables that we just joined together
		if t == leftTable || t == rightTable {
			continue
		}

		// Insert the result table in the list such that the list remails sorted (only if it has not already been inserted)
		if !insertedResultTable && t.Size() > joinResultSize {
			newTableList[i] = joinResult // Insert the result table
			insertedResultTable = true   // Remember that we inserted the result table
			i++
		}
		// Add the table to the list
		newTableList[i] = t
		i++
	}
	// When the join result table is larger than all other tables in the list, we have to add it
	// to the list right here. It is kind of appended to the list
	if i < len(newTableList) {
		newTableList[i] = joinResult
	}

	return &newTableList
}

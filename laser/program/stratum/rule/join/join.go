package join

import (
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/conjunctformulae"
	"bitbucket.org/hrbazoo/laser/stats"

	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation join operations of formulae in Laser 2.0
 */

// makeIndexFromRecentRecords takes a substitution table and a number of variables that participate in the join and
// creates an index from the recent substitutions in a table
func makeIndexFromRecentRecords(table *substitutiontable.SubstitutionTable, joinVariables *[]types.Variable) *map[string][]*substitutiontable.SubstitutionRecord {
	index := map[string][]*substitutiontable.SubstitutionRecord{}

	// Create and index of recent substitution records
	for r := range table.RecentSubstitutionRecords {
		// Create the key
		key := keyGen(r, &table.VariableToRecordIndex, joinVariables)
		// Put the record in the index using the ke
		index[key] = append(index[key], r)
	}

	return &index
}

// This implements a semi-naive join algorithm, i.e., the new records of the left table are joined to the new records of
// the right table and vice-versa and the old records of the left table are joined to the right records in the left table.
// The function returns a substitution table with the results of the join.
func joinTablesRecentToAll(resultTable *substitutiontable.SubstitutionTable,
	leftTable *substitutiontable.SubstitutionTable,
	rightTable *substitutiontable.SubstitutionTable,
	joinVariables *[]types.Variable) {
	// First, some sanity checks
	if leftTable == nil || rightTable == nil {
		panic("The substitution tables that participate in the join are nil")
	}

	// Create an index of recent records in the left table
	index := makeIndexFromRecentRecords(leftTable, joinVariables)

	// Join all substitution records in the right table with the recent records in the left table
	for rightRecord := range rightTable.Records {

		// Ignore all records that would hold only in the future
		if rightRecord.ConsiderationTime > clock.GetNow() {
			continue
		}

		// Generate the key
		key := keyGen(rightRecord, &rightTable.VariableToRecordIndex, joinVariables)
		// If the key already exists in the index, there is a join
		if records, exists := (*index)[key]; exists {
			// Generate the join results
			for _, leftRecord := range records {
				// Ignore the record if it would only hold in the future
				if leftRecord.ConsiderationTime <= clock.GetNow() {
					resultRecord := makeResultSubstitutionRecord(&resultTable.VariableToRecordIndex, &leftTable.VariableToRecordIndex, &rightTable.VariableToRecordIndex, leftRecord, rightRecord)
					if !resultTable.ContainsSubstitutions(&resultRecord.Substitutions, resultRecord.ConsiderationTime, resultRecord.HorizonTime) {
						// Insert the join results in the result table
						resultTable.Insert(resultRecord)
					}
				}
			}
		}
	}
}

// Implements a semi-naive join algorithm i.e., the new recors of the left are joined with all records in the right and vice versa
func performJoin(leftTable *substitutiontable.SubstitutionTable, rightTable *substitutiontable.SubstitutionTable, joinVariables *[]types.Variable) *substitutiontable.SubstitutionTable {
	// Increment the number of joins
	stats.ExecutionProfile.TotalNumberOfJoinOperations++
	// The new records of the left table must be join to the right table and the new records of the
	// new records of the right table must be joined to the left table
	resultTable := makeResultSubstitutionTable(leftTable, rightTable, joinVariables)

	// Join new records of the left table to all records of the right table
	joinTablesRecentToAll(resultTable, leftTable, rightTable, joinVariables)
	// Join new records of the right table to all records of the left table
	joinTablesRecentToAll(resultTable, rightTable, leftTable, joinVariables)
	// Return the merged results
	return resultTable
}

// internalJoin recursively joins a list of tables together until nothing else remains
func internalJoin(tables *[]*substitutiontable.SubstitutionTable) *substitutiontable.SubstitutionTable {
	// If there is no table in the list, return nil
	if len(*tables) == 0 {
		panic("Cannot perform join on an empty list of substitution tables")
	}

	// If there are fewer than two tables in the list, return the first one
	if len(*tables) == 1 {
		return (*tables)[0]
	}

	// First the smallest tables to join together
	leftTable, rightTable, commonVariables := chooseSmallestTablesWithCommonVariables(tables)

	// Join tables together and collect the results
	joinResult := performJoin(leftTable, rightTable, commonVariables)

	// Remove the joined tables from the list of tables and insert the result table in the list
	// such that the new list is still sorted.
	newTableList := insertResultTableAndRemoveJoinedTables(tables, leftTable, rightTable, joinResult)

	// Recursively join the new list of tables
	return internalJoin(newTableList)

}

// Join takes a slice of formulae, performs a hash-join on their outputs and returns
// the results
func Join(cf *conjunctformulae.ConjFormulae) *substitutiontable.SubstitutionTable {

	// First, sort the formulae by the number of records they have concluded to make sure
	// smallest substitution tables are joined first.
	sortFormula(&cf.Formulae)

	// Retrieve substitution tables that contain the results of formulae.
	substitutionTables := make([]*substitutiontable.SubstitutionTable, len(cf.Formulae))
	for i, f := range cf.Formulae {
		substitutionTables[i] = f.GetEvaluationResults()
	}

	return internalJoin(&substitutionTables) // Perform the join and return the results

}

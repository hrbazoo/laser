package join

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"

	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/conjunctformulae"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
	timewindim "bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/timewindim"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit tests of join operations
 */

// TestJoinCommonVariables tests whether join can find common variables correctly or not?
func TestJoinCommonVariables(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the first formula
	f1Vars := []types.Variable{"A", "B"}
	// Variables of the second formula
	f2Vars := []types.Variable{"B", "C"}

	// The first formula in the join
	var f1 formula.Formula = timewindim.Create(predicate, f1Vars, 2)
	// The second formula in the join
	var f2 formula.Formula = timewindim.Create(predicate, f2Vars, 2)

	// Find the common variables
	commonVariables := findCommonVariable(f1.GetEvaluationResults(), f2.GetEvaluationResults())

	// There is only one common variable
	assert.Equal(len(*commonVariables), 1)
	// "B" is the common variable
	assert.Equal((*commonVariables)[0], types.Variable("B"))

	// Variables of the first formula
	f1Vars = []types.Variable{"A", "B"}
	// Variables of the second formula
	f2Vars = []types.Variable{"C", "D"}

	// The first formula in the join
	f1 = timewindim.Create(predicate, f1Vars, 2)
	// The second formula in the join
	f2 = timewindim.Create(predicate, f2Vars, 2)

	// Find the common variables
	commonVariables = findCommonVariable(f1.GetEvaluationResults(), f2.GetEvaluationResults())

	// There is only one common variable
	assert.Equal(len(*commonVariables), 0)

	// Variables of the first formula
	f1Vars = []types.Variable{"A", "B"}
	// Variables of the second formula
	f2Vars = []types.Variable{"B", "A", "C"}

	// The first formula in the join
	f1 = timewindim.Create(predicate, f1Vars, 2)
	// The second formula in the join
	f2 = timewindim.Create(predicate, f2Vars, 2)

	// Find the common variables
	commonVariables = findCommonVariable(f1.GetEvaluationResults(), f2.GetEvaluationResults())

	// There is only one common variable
	assert.Equal(len(*commonVariables), 2)
}
func TestJoinKeyGen(t *testing.T) {
	assert := cassert.New(t)

	// The list of variables
	variables1 := [...]types.Variable{"A", "B", "B"}
	variables2 := [...]types.Variable{"B"}

	// Create the empty substitution table
	substitutionTable1 := substitutiontable.New(variables1[:])
	substitutionTable2 := substitutiontable.New(variables2[:])

	// The first record for the first substitution table
	st1rec1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{types.Substitution("a1"), types.Substitution("b1"), types.Substitution("b1")}, types.NoTimePoint, types.NoTimePoint)
	// The second record for the first substitution table
	st1rec2 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{types.Substitution("a2"), types.Substitution("b2"), types.Substitution("b1")}, types.NoTimePoint, types.NoTimePoint)

	// The second record for the first substitution table
	st2rec1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b1"}, types.NoTimePoint, types.NoTimePoint)

	// Insert the two records of the first substitution table
	substitutionTable1.Insert(st1rec1)
	substitutionTable1.Insert(st1rec2)

	// Insert the record of the second substitution table
	substitutionTable2.Insert(st2rec1)

	// Create the join key
	key := keyGen(st1rec1, &substitutionTable1.VariableToRecordIndex, &[]types.Variable{"B"})
	// The key must include "b1"
	assert.Equal(key, "b1")

	// Create the join key
	key = keyGen(st1rec2, &substitutionTable1.VariableToRecordIndex, &[]types.Variable{"B"})
	// The key must be empty because constants belonging to variable "B" do not match
	assert.Equal(key, "")

	// Create the join key
	key = keyGen(st1rec1, &substitutionTable1.VariableToRecordIndex, &[]types.Variable{"A", "B"})
	// The key must be the concatenation of the constants
	assert.Equal(key, "a1b1")
}
func TestJoinSortTables(t *testing.T) {
	assert := cassert.New(t)

	predicate := types.Predicate("p")
	variables := [...]types.Variable{"A", "B"}

	// Create the empty substitution table
	f1 := timewindim.Create(predicate, variables[:], 3)
	f2 := timewindim.Create(predicate, variables[:], 3)
	f3 := timewindim.Create(predicate, variables[:], 3)
	f4 := timewindim.Create(predicate, variables[:], 3)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, 0, 1)
	f1.Insert(record)
	f2.Insert(record)
	f3.Insert(record)
	f4.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b2"}, 0, 1)
	f1.Insert(record)
	f2.Insert(record)
	f3.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a3", "b3"}, 0, 1)
	f1.Insert(record)
	f3.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a4", "b4"}, 0, 1)
	f1.Insert(record)

	cf := conjunctformulae.CreateConjFormulae(&[]formula.Formula{f1, f2, f3, f4}, types.UnknownTimePoint, types.UnknownTupleCounter)
	tables := []*substitutiontable.SubstitutionTable{f1.GetEvaluationResults(), f2.GetEvaluationResults(), f3.GetEvaluationResults(), f4.GetEvaluationResults()}
	sortFormula(&cf.Formulae)

	assert.Equal(tables[0], f3.GetEvaluationResults())
	assert.Equal(tables[1], f2.GetEvaluationResults())
	assert.Equal(tables[2], f3.GetEvaluationResults())
	assert.Equal(tables[3], f1.GetEvaluationResults())
}

func TestJoinJoinOrder1(t *testing.T) {
	assert := cassert.New(t)

	predicate := types.Predicate("p")
	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"C", "D"}
	table3Variables := [...]types.Variable{"B", "C"}

	// Create the empty substitution table
	f3 := timewindim.Create(predicate, table1Variables[:], 3)
	f2 := timewindim.Create(predicate, table2Variables[:], 3)
	f1 := timewindim.Create(predicate, table3Variables[:], 3)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b2"}, types.StartTimePoint, 1)
	f3.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c1", "d1"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c2", "d2"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c3", "d3"}, types.StartTimePoint, 1)
	f2.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b1", "c1"}, types.StartTimePoint, 1)
	f1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "c2"}, types.StartTimePoint, 1)
	f1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "c3"}, types.StartTimePoint, 1)
	f1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b4", "c4"}, types.StartTimePoint, 1)
	f1.Insert(record)

	cf := conjunctformulae.CreateConjFormulae(&[]formula.Formula{f1, f2, f3}, types.UnknownTimePoint, types.UnknownTupleCounter)

	// Sort tables by the number of their substitution records
	sortFormula(&cf.Formulae)

	assert.Equal(cf.Formulae[0], f3)
	assert.Equal(cf.Formulae[1], f2)
	assert.Equal(cf.Formulae[2], f1)

	tables := make([]*substitutiontable.SubstitutionTable, 3)
	tables[0] = cf.Formulae[0].GetEvaluationResults()
	tables[1] = cf.Formulae[1].GetEvaluationResults()
	tables[2] = cf.Formulae[2].GetEvaluationResults()

	// Find smallest tables to join that have at least one common variables
	leftTable, rightTable, commonVariables := chooseSmallestTablesWithCommonVariables(&tables)

	assert.Equal(leftTable, tables[0])
	assert.Equal(rightTable, tables[2])
	assert.Equal(*commonVariables, []types.Variable{"B"})
}

// Tests ordering join participants and choosing the best ones when there is no common variables
func TestJoinJoinOrder2(t *testing.T) {
	assert := cassert.New(t)

	predicate := types.Predicate("p")
	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"C", "D"}
	table3Variables := [...]types.Variable{"E", "F"}

	// Create the empty substitution table
	f1 := timewindim.Create(predicate, table1Variables[:], 3)
	f2 := timewindim.Create(predicate, table2Variables[:], 3)
	f3 := timewindim.Create(predicate, table3Variables[:], 3)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	f1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b2"}, types.StartTimePoint, 1)
	f1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c1", "d1"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c2", "d2"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c3", "d3"}, types.StartTimePoint, 1)
	f2.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e1", "f1"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e2", "f2"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e3", "f3"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e4", "f4"}, types.StartTimePoint, 1)
	f3.Insert(record)

	cf := conjunctformulae.CreateConjFormulae(&[]formula.Formula{f1, f2, f3}, types.UnknownTimePoint, types.UnknownTupleCounter)
	tables := []*substitutiontable.SubstitutionTable{f1.GetEvaluationResults(), f2.GetEvaluationResults(), f3.GetEvaluationResults()}

	// Sort tables by the number of their substitution records
	sortFormula(&cf.Formulae)

	assert.Equal(tables[0], f1.GetEvaluationResults())
	assert.Equal(tables[1], f2.GetEvaluationResults())
	assert.Equal(tables[2], f3.GetEvaluationResults())

	// Find smallest tables to join that have at least one common variables
	leftTable, rightTable, commonVariables := chooseSmallestTablesWithCommonVariables(&tables)

	assert.Equal(leftTable, f1.GetEvaluationResults())
	assert.Equal(rightTable, f2.GetEvaluationResults())
	assert.Equal(commonVariables, (*[]types.Variable)(nil))

}

// Tests ordering join participants and choosing the best ones when the list of participants is already sorted
func TestJoinJoinOrder3(t *testing.T) {
	assert := cassert.New(t)

	predicate := types.Predicate("p")
	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "A"}
	table3Variables := [...]types.Variable{"E", "F"}

	// Create the empty substitution table
	f1 := timewindim.Create(predicate, table1Variables[:], 3)
	f2 := timewindim.Create(predicate, table2Variables[:], 3)
	f3 := timewindim.Create(predicate, table3Variables[:], 3)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	f1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b2"}, types.StartTimePoint, 1)
	f1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c1", "d1"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c2", "d2"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c3", "d3"}, types.StartTimePoint, 1)
	f2.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e1", "f1"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e2", "f2"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e3", "f3"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e4", "f4"}, types.StartTimePoint, 1)
	f3.Insert(record)

	cf := conjunctformulae.CreateConjFormulae(&[]formula.Formula{f1, f2, f3}, types.UnknownTimePoint, types.UnknownTupleCounter)
	tables := []*substitutiontable.SubstitutionTable{f1.GetEvaluationResults(), f2.GetEvaluationResults(), f3.GetEvaluationResults()}

	// Sort tables by the number of their substitution records
	sortFormula(&cf.Formulae)

	assert.Equal(tables[0], f1.GetEvaluationResults())
	assert.Equal(tables[1], f2.GetEvaluationResults())
	assert.Equal(tables[2], f3.GetEvaluationResults())

	// Find smallest tables to join that have at least one common variables
	leftTable, rightTable, commonVariables := chooseSmallestTablesWithCommonVariables(&tables)

	assert.Equal(leftTable, f1.GetEvaluationResults())
	assert.Equal(rightTable, f2.GetEvaluationResults())
	assert.Equal((*commonVariables)[0] == "A" || (*commonVariables)[1] == "A", true)
	assert.Equal((*commonVariables)[0] == "B" || (*commonVariables)[1] == "B", true)

}

// Tests ordering join participants and choosing the best one when all participants contain the same
// number of items
func TestJoinJoinOrder4(t *testing.T) {
	assert := cassert.New(t)

	predicate := types.Predicate("p")
	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "A"}
	table3Variables := [...]types.Variable{"E", "F"}

	// Create the empty substitution table
	f1 := timewindim.Create(predicate, table1Variables[:], 3)
	f2 := timewindim.Create(predicate, table2Variables[:], 3)
	f3 := timewindim.Create(predicate, table3Variables[:], 3)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	f1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b2"}, types.StartTimePoint, 1)
	f1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c1", "d1"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c2", "d2"}, types.StartTimePoint, 1)
	f2.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e1", "f1"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"e2", "f2"}, types.StartTimePoint, 1)
	f3.Insert(record)

	cf := conjunctformulae.CreateConjFormulae(&[]formula.Formula{f1, f2, f3}, types.UnknownTimePoint, types.UnknownTupleCounter)
	tables := []*substitutiontable.SubstitutionTable{f1.GetEvaluationResults(), f2.GetEvaluationResults(), f3.GetEvaluationResults()}

	// Sort tables by the number of their substitution records
	sortFormula(&cf.Formulae)

	assert.Equal(tables[0], f1.GetEvaluationResults())
	assert.Equal(tables[1], f2.GetEvaluationResults())
	assert.Equal(tables[2], f3.GetEvaluationResults())

	// Find smallest tables to join that have at least one common variables
	leftTable, rightTable, commonVariables := chooseSmallestTablesWithCommonVariables(&tables)

	assert.Equal(leftTable, f1.GetEvaluationResults())
	assert.Equal(rightTable, f2.GetEvaluationResults())
	assert.Equal((*commonVariables)[0] == "A" || (*commonVariables)[1] == "A", true)
	assert.Equal((*commonVariables)[0] == "B" || (*commonVariables)[1] == "B", true)

}

// Tests join the recent records of the left table to all records of the right table
func TestJoinRecentToAll1(t *testing.T) {
	assert := cassert.New(t)

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "A"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])

	commonVariables := findCommonVariable(table1, table2)
	resultTable := makeResultSubstitutionTable(table1, table2, commonVariables)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	table1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b2"}, types.StartTimePoint, 1)
	table1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b1", "a1"}, types.StartTimePoint, 1)
	table2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "a2"}, types.StartTimePoint, 1)
	table2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "a3"}, types.StartTimePoint, 1)
	table2.Insert(record)

	joinTablesRecentToAll(resultTable, table1, table2, commonVariables)

	assert.Equal(resultTable.Size(), 2)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 2)

}

// Tests join the recent records of the left table to all records of the right table
func TestJoinRecentToAll2(t *testing.T) {
	assert := cassert.New(t)

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "A"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])

	commonVariables := findCommonVariable(table1, table2)
	resultTable := makeResultSubstitutionTable(table1, table2, commonVariables)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	table1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3"}, types.StartTimePoint, 1)
	table1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b1", "a1"}, types.StartTimePoint, 1)
	table2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "a2"}, types.StartTimePoint, 1)
	table2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "a3"}, types.StartTimePoint, 1)
	table2.Insert(record)

	joinTablesRecentToAll(resultTable, table1, table2, commonVariables)

	assert.Equal(resultTable.Size(), 1)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 1)

}

// Tests join the recent records of the left table to all records of the right table
func TestJoinRecentToAll3(t *testing.T) {
	assert := cassert.New(t)

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "A"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])

	commonVariables := findCommonVariable(table1, table2)
	resultTable := makeResultSubstitutionTable(table1, table2, commonVariables)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2"}, types.StartTimePoint, 1)
	table1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3"}, types.StartTimePoint, 1)
	table1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b1", "a1"}, types.StartTimePoint, 1)
	table2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "a2"}, types.StartTimePoint, 1)
	table2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "a3"}, types.StartTimePoint, 1)
	table2.Insert(record)

	joinTablesRecentToAll(resultTable, table1, table2, commonVariables)

	assert.Equal(resultTable.Size(), 0)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 0)

}

// Tests joining recent records of the left table with all records of the right table and vice-versa and
// the collection of resulting records
func TestJoinLeftAndRight1(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "C"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])

	commonVariables := findCommonVariable(table1, table2)

	var record *substitutiontable.SubstitutionRecord

	// This would be the only old record in the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2"}, types.StartTimePoint, 1)
	table1.Insert(record)
	table1.DeleteRecentRecords()

	// This would be the only recent record of the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3"}, types.StartTimePoint, 1)
	table1.Insert(record)

	// This would be the only old record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "c1"}, types.StartTimePoint, 1)
	table2.Insert(record)
	table2.DeleteRecentRecords()
	// This would be the only recent record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "c2"}, types.StartTimePoint, 1)
	table2.Insert(record)

	// Join the tables
	resultTable := performJoin(table1, table2, commonVariables)

	// These records constitute the expected outcome of the join operation. Because the recent records only contain
	// "b3" as shared substitution, the resultRecord1 is not yielded in the join
	resultRecord1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2", "c1"}, types.StartTimePoint, 1)
	resultRecord2 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3", "c2"}, types.StartTimePoint, 1)
	varToRecordIndex := map[types.Variable][]int{"A": []int{0}, "B": []int{1}, "C": []int{2}}

	// The join must yield two result records
	assert.Equal(resultTable.Size(), 1)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 1)
	assert.Equal(len(resultTable.RecentInvalidationTokens), 0)
	assert.Equal(resultTable.Contains(resultRecord1, &varToRecordIndex), false)
	assert.Equal(resultTable.Contains(resultRecord2, &varToRecordIndex), true)
}

// Tests joining recent records of the left table with all records of the right table and vice-versa and
// the collection of resulting records
func TestJoinLeftAndRight2(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "C"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])

	commonVariables := findCommonVariable(table1, table2)

	var record *substitutiontable.SubstitutionRecord

	// This would be the only old record in the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2"}, types.StartTimePoint, 1)
	table1.Insert(record)

	// This would be the only recent record of the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3"}, types.StartTimePoint, 1)
	table1.Insert(record)

	// This would be the only old record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "c1"}, types.StartTimePoint, 1)
	table2.Insert(record)
	// This would be the only recent record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "c2"}, types.StartTimePoint, 1)
	table2.Insert(record)

	// Join the tables
	resultTable := performJoin(table1, table2, commonVariables)

	// These records constitute the expected outcome of the join operation
	resultRecord1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2", "c1"}, types.StartTimePoint, 1)
	resultRecord2 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3", "c2"}, types.StartTimePoint, 1)
	varToRecordIndex := map[types.Variable][]int{"A": []int{0}, "B": []int{1}, "C": []int{2}}

	// The join must yield two result records
	assert.Equal(resultTable.Size(), 2)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 2)
	assert.Equal(len(resultTable.RecentInvalidationTokens), 0)
	assert.Equal(resultTable.Contains(resultRecord1, &varToRecordIndex), true)
	assert.Equal(resultTable.Contains(resultRecord2, &varToRecordIndex), true)
}

// Tests joining recent records of the left table with all records of the right table and vice-versa and
// the collection of resulting records when the horizon time of joined records differ
func TestJoinLeftAndRight3(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "C"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])

	commonVariables := findCommonVariable(table1, table2)

	var record *substitutiontable.SubstitutionRecord

	// This would be the only old record in the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2"}, types.StartTimePoint, 1)
	table1.Insert(record)

	// This would be the only recent record of the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3"}, types.StartTimePoint, 1)
	table1.Insert(record)

	// This would be the only old record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "c1"}, types.StartTimePoint, 2)
	table2.Insert(record)
	// This would be the only recent record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "c2"}, types.StartTimePoint, 2)
	table2.Insert(record)

	// Join the tables
	resultTable := performJoin(table1, table2, commonVariables)

	// These records constitute the expected outcome of the join operation
	resultRecord1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2", "c1"}, types.StartTimePoint, 1)
	resultRecord2 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3", "c2"}, types.StartTimePoint, 1)
	varToRecordIndex := map[types.Variable][]int{"A": []int{0}, "B": []int{1}, "C": []int{2}}

	// The join must yield two result records
	assert.Equal(resultTable.Size(), 2)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 2)
	assert.Equal(len(resultTable.RecentInvalidationTokens), 0)
	assert.Equal(resultTable.Contains(resultRecord1, &varToRecordIndex), true)
	assert.Equal(resultTable.Contains(resultRecord2, &varToRecordIndex), true)
}

// Tests joining recent records of the left table with all records of the right table and vice-versa and
// the collection of resulting records when one of the records in the join would only hold in a the future time point
func TestJoinLeftAndRight4(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "C"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])

	commonVariables := findCommonVariable(table1, table2)

	var record *substitutiontable.SubstitutionRecord

	// This would be the only old record in the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2"}, types.StartTimePoint, 1)
	table1.Insert(record)

	// This would be the only recent record of the first table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3"}, types.TimePoint(int(types.StartTimePoint)+1), 3)
	table1.Insert(record)

	// This would be the only old record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "c1"}, types.StartTimePoint, 2)
	table2.Insert(record)
	// This would be the only recent record in the second table
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b3", "c2"}, types.StartTimePoint, 2)
	table2.Insert(record)

	// Join the tables
	resultTable := performJoin(table1, table2, commonVariables)

	// These records constitute the expected outcome of the join operation
	resultRecord1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b2", "c1"}, types.StartTimePoint, 1)
	resultRecord2 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b3", "c2"}, types.StartTimePoint, 1)
	varToRecordIndex := map[types.Variable][]int{"A": []int{0}, "B": []int{1}, "C": []int{2}}

	// The join must yield two result records
	assert.Equal(resultTable.Size(), 1)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 1)
	assert.Equal(len(resultTable.RecentInvalidationTokens), 0)
	assert.Equal(resultTable.Contains(resultRecord1, &varToRecordIndex), true)
	assert.Equal(resultTable.Contains(resultRecord2, &varToRecordIndex), false)
}

// TestJoinSubTable1 tests whether a list of substitution tables are correctly joined together
func TestJoinSubTables(t *testing.T) {
	assert := cassert.New(t)

	// Variables are not important for this test
	var1 := [...]types.Variable{"A", "B"}
	var2 := [...]types.Variable{"B", "C"}
	var3 := [...]types.Variable{"C", "D"}
	var4 := [...]types.Variable{"D"}

	table1 := substitutiontable.New(var1[:])
	table2 := substitutiontable.New(var2[:])
	table3 := substitutiontable.New(var3[:])
	table4 := substitutiontable.New(var4[:])

	tables := []*substitutiontable.SubstitutionTable{table1, table2, table3, table4}

	var record *substitutiontable.SubstitutionRecord
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a", "b"}, types.StartTimePoint, 1)
	table1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b", "c"}, types.StartTimePoint, 1)
	table2.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c", "d"}, types.StartTimePoint, 1)
	table3.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"d"}, types.StartTimePoint, 1)
	table4.Insert(record)

	resultTable := internalJoin(&tables)

	assert.Equal(resultTable.Size(), 1)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 1)
	assert.Equal(len(resultTable.RecentInvalidationTokens), 0)
	assert.Equal(len(resultTable.VariableToRecordIndex), 4)
}

func TestJoin1(t *testing.T) {
	assert := cassert.New(t)

	predicate := types.Predicate("p")
	var1 := [...]types.Variable{"A", "B"}
	var2 := [...]types.Variable{"B", "C"}
	var3 := [...]types.Variable{"C", "B"}

	// Create the empty substitution table
	f1 := timewindim.Create(predicate, var1[:], 3)
	f2 := timewindim.Create(predicate, var2[:], 3)
	f3 := timewindim.Create(predicate, var3[:], 3)

	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	f1.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b1"}, types.StartTimePoint, 1)
	f1.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b1", "c1"}, types.StartTimePoint, 1)
	f2.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b2", "c2"}, types.StartTimePoint, 2)
	f2.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c1", "b1"}, types.StartTimePoint, 1)
	f3.Insert(record)
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"c2", "b2"}, types.StartTimePoint, 2)
	f3.Insert(record)

	cf := conjunctformulae.CreateConjFormulae(&[]formula.Formula{f1, f2, f3}, types.UnknownTimePoint, types.UnknownTupleCounter)
	resultTable := Join(cf)

	// These records constitute the expected outcome of the join operation
	resultRecord1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1", "c1"}, types.StartTimePoint, 4)
	resultRecord2 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a2", "b1", "c1"}, types.StartTimePoint, 4)
	varToRecordIndex := map[types.Variable][]int{"A": []int{0}, "B": []int{1}, "C": []int{2}}

	// The join must yield two result records
	assert.Equal(resultTable.Size(), 2)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 2)
	assert.Equal(len(resultTable.RecentInvalidationTokens), 0)
	assert.Equal(resultTable.Contains(resultRecord1, &varToRecordIndex), true)
	assert.Equal(resultTable.Contains(resultRecord2, &varToRecordIndex), true)
}

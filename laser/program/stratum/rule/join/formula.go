package join

import (
	"sort"

	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of formula-related join operations in Laser 2.0
 */

// Takes an array(slice) of formula and sorts them based on the number of records that exists
// in their substitution tables
func sortFormula(f *[]formula.Formula) {
	sort.Slice(*f, func(i, j int) bool {
		return (*f)[i].GetEvaluationResults().Size() < (*f)[j].GetEvaluationResults().Size()
	})
}

package join

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit tests of substitution table operations of the join implementation
 */

// TestJoinSubTableMakeResultRecord tests whether the result records of join operations are correctly created
func TestJoinSubTableMakeResultRecord(t *testing.T) {
	assert := cassert.New(t)

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "C"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])
	commonVariables := findCommonVariable(table1, table2)

	resultTable := makeResultSubstitutionTable(table1, table2, commonVariables)

	leftRecord := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, types.StartTimePoint, 1)
	rightRecord := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b1", "c1"}, types.StartTimePoint, 1)
	resultRecord := makeResultSubstitutionRecord(&resultTable.VariableToRecordIndex, &table1.VariableToRecordIndex, &table2.VariableToRecordIndex, leftRecord, rightRecord)

	variableToRecordIndex := resultTable.VariableToRecordIndex

	var varIndexA, varIndexB, varIndexC []int
	var exists = false

	assert.Equal(len(resultRecord.Substitutions), 3)

	varIndexA, exists = variableToRecordIndex[types.Variable("A")]
	assert.Equal(exists, true)

	varIndexB, exists = variableToRecordIndex[types.Variable("B")]
	assert.Equal(exists, true)

	varIndexC, exists = variableToRecordIndex[types.Variable("C")]
	assert.Equal(exists, true)

	assert.Equal(resultRecord.Substitutions[varIndexA[0]], types.Substitution("a1"))
	assert.Equal(resultRecord.Substitutions[varIndexB[0]], types.Substitution("b1"))
	assert.Equal(resultRecord.Substitutions[varIndexC[0]], types.Substitution("c1"))
}

// TestJoinSubTableMakeResultTable tests whether the result table of the join operation is created correctly
func TestJoinSubTableMakeResultTable(t *testing.T) {
	assert := cassert.New(t)

	table1Variables := [...]types.Variable{"A", "B"}
	table2Variables := [...]types.Variable{"B", "C"}

	table1 := substitutiontable.New(table1Variables[:])
	table2 := substitutiontable.New(table2Variables[:])
	commonVariables := findCommonVariable(table1, table2)

	resultTable := makeResultSubstitutionTable(table1, table2, commonVariables)

	resultTableVariables := map[types.Variable]bool{}

	for v := range resultTable.VariableToRecordIndex {
		resultTableVariables[v] = true
	}

	assert.Equal(len(resultTable.VariableToRecordIndex), 3)
	assert.Equal(resultTable.NumberOfVariables, 3)
	assert.Equal(resultTableVariables["A"], true)
	assert.Equal(resultTableVariables["B"], true)
	assert.Equal(resultTableVariables["C"], true)
	assert.Equal(resultTable.Size(), 0)
	assert.Equal(len(resultTable.RecentInvalidationTokens), 0)
	assert.Equal(len(resultTable.RecentSubstitutionRecords), 0)
}

// TestJoinSubTableMakeResultTable1 tests whether the result table of the join operation is created correctly
func TestJoinSubTableResortJoiningTables1(t *testing.T) {
	assert := cassert.New(t)

	// Variables are not important for this test
	variables := [...]types.Variable{}

	table1 := substitutiontable.New(variables[:])
	table2 := substitutiontable.New(variables[:])
	table3 := substitutiontable.New(variables[:])
	resultTable := substitutiontable.New(variables[:])

	tables := []*substitutiontable.SubstitutionTable{table1, table2, table3}
	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table1.Insert(record)
	table2.Insert(record)
	table3.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table2.Insert(record)
	table3.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table3.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	resultTable.Insert(record)

	tables = *insertResultTableAndRemoveJoinedTables(&tables, table1, table2, resultTable)

	assert.Equal(len(tables), 2)
	assert.Equal(tables[0], table3)
	assert.Equal(tables[1], resultTable)
}

// TestJoinSubTableMakeResultTable2 tests whether the result table of the join operation is created correctly
func TestJoinSubTableResortJoiningTables2(t *testing.T) {
	assert := cassert.New(t)

	// Variables are not important for this test
	variables := [...]types.Variable{}

	table1 := substitutiontable.New(variables[:])
	table2 := substitutiontable.New(variables[:])
	table3 := substitutiontable.New(variables[:])
	resultTable := substitutiontable.New(variables[:])

	tables := []*substitutiontable.SubstitutionTable{table1, table2, table3}
	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table1.Insert(record)
	table2.Insert(record)
	table3.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table2.Insert(record)
	table3.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table3.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table3.Insert(record)

	tables = *insertResultTableAndRemoveJoinedTables(&tables, table1, table2, resultTable)

	assert.Equal(len(tables), 2)
	assert.Equal(tables[0], resultTable)
	assert.Equal(tables[1], table3)
}

// TestJoinSubTableMakeResultTable3 tests whether the result table of the join operation is created correctly
func TestJoinSubTableResortJoiningTables3(t *testing.T) {
	assert := cassert.New(t)

	// Variables are not important for this test
	variables := [...]types.Variable{}

	table1 := substitutiontable.New(variables[:])
	table2 := substitutiontable.New(variables[:])
	table3 := substitutiontable.New(variables[:])
	table4 := substitutiontable.New(variables[:])
	resultTable := substitutiontable.New(variables[:])

	tables := []*substitutiontable.SubstitutionTable{table1, table2, table3, table4}
	var record *substitutiontable.SubstitutionRecord

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table1.Insert(record)
	table2.Insert(record)
	table3.Insert(record)
	table4.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table2.Insert(record)
	table3.Insert(record)
	table4.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table3.Insert(record)
	table4.Insert(record)
	resultTable.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	resultTable.Insert(record)
	table4.Insert(record)

	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, types.StartTimePoint, 1)
	table4.Insert(record)

	tables = *insertResultTableAndRemoveJoinedTables(&tables, table1, table2, resultTable)

	assert.Equal(len(tables), 3)
	assert.Equal(tables[0], table3)
	assert.Equal(tables[1], resultTable)
	assert.Equal(tables[2], table4)
}

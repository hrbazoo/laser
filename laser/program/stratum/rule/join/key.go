package join

import (
	"strings"

	st "bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of join operations that involve key generation
 * in joining of two substitution tables
 */

// Takes a record, the map from variable names to position of substitutions in the record, and
// a number of variables and generates a unique key from the substitutions of variables in the record.
// Returns the generated key
func keyGen(r *st.SubstitutionRecord, variableToRecordIndex *map[types.Variable][]int, variables *[]types.Variable) string {
	var key strings.Builder
	for _, v := range *variables {
		indexes := (*variableToRecordIndex)[v]
		if len(indexes) == 1 {
			key.WriteString(string(r.Substitutions[indexes[0]]))
		} else if len(indexes) > 1 { // happens when a variable appears more than ones in a formula
			// We must check to make sure all substitutions of the variable have exactly the same value
			val1 := r.Substitutions[indexes[0]]
			allEqual := true // Will remain true by the end of the following loop if all values are the same
			// Check if all substitutions of the variable have exactly the same value
			for _, i := range indexes[1:] {
				valn := r.Substitutions[i] // Get the nth value
				// If values don't match, raise the flag and break out of the loop.
				// We must discard the record
				if valn != val1 {
					allEqual = false
					break
				}
			}
			// If one of the values is not equal to the rest, the join will fail
			// anyways. So, no point in continueing with the record.
			if !allEqual {
				continue
			}
			// If all values are equal, simply add the first one to the key
			key.WriteString(string(val1))
		} else { // Must never happen that a variable has no index in the substitution record
			panic("No index for a variable")
		}
	}
	// Return the generated key
	return key.String()
}

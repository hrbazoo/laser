package atom

import (
	"math"

	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	st "bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of formulae of form: p(X)
 */

// AtomFormula defines the structure of Laser formulae in form of p(x)
type AtomFormula struct {
	predicate               types.Predicate       // The predicate of the formula
	substitutionTable       *st.SubstitutionTable // The substitution table used to store variable substitutions
	resultSubstitutionTable *st.SubstitutionTable // The substitution table used to store variable substitutions of formula conclusions
}

// Create takes the predicate and the variable names used in the formula and
// creates a TimeWindowDiamondFormula formula
func Create(pred types.Predicate, variables []types.Variable) *AtomFormula {
	// Create the formula and return its pointer
	return &AtomFormula{
		predicate:               pred,
		substitutionTable:       st.New(variables),
		resultSubstitutionTable: st.New(variables),
	}
}

// InsertResult inserts a result record into the result substitution table. This functionality
// is supported only for formulae that are used in the head of rules.
func (f *AtomFormula) InsertResult(inputRecord *st.SubstitutionRecord) {
	if !f.substitutionTable.ContainsSubstitutions(&inputRecord.Substitutions, inputRecord.ConsiderationTime, inputRecord.HorizonTime) {
		f.resultSubstitutionTable.Insert(inputRecord)
	}
}

// Insert takes a substitution record and stores it in the substitution table of the formula
func (f *AtomFormula) Insert(inputRecord *st.SubstitutionRecord) {
	if !f.substitutionTable.ContainsSubstitutions(&inputRecord.Substitutions, inputRecord.ConsiderationTime, inputRecord.HorizonTime) {
		// store it in the substitution table
		f.substitutionTable.Insert(inputRecord)
		// If the consideration time of the current record is not in the future, it must
		// immediately go to the result substitution table.
		// NOTE: Invalidation tokens are not stored in the result substitution table
		if inputRecord.ConsiderationTime >= types.StartTimePoint && clock.GetNow() >= inputRecord.ConsiderationTime {
			f.resultSubstitutionTable.Insert(inputRecord)
		}
	}
}

// GetPredicate returns formula's predicate
func (f *AtomFormula) GetPredicate() types.Predicate {
	return f.predicate
}

// Evaluate evaluates the formula to determine whether it holds at the current
// time point or not. Returns a boolean value that indicates whether the formula holds or not
func (f *AtomFormula) Evaluate() bool {
	now := clock.GetNow()

	// There are often fewer invalidation tokens than substitution records. Therefore, if
	// the program is configured to use invalidation prediction algorithm, it pays to
	// first check if we can invalidate the formula by finding an invalidation token that
	// does so.
	// If there is an invalidation token in the substitution table with a consideration
	// time smaller than the current time point, that means that the invalidation token
	// spans over a time period larger than the one time point. In that case, the whole
	// formula certainly does not hold. If anything like that found, return immediately.
	if config.UseInvalidationPrediction {
		// If there are invalidation tokens in the substitution table (not the result substitution
		// table), check to see if the consideration time of any of them is smaller or equal to the
		// current time point. If that is the case, create an invalidation token and add it to the
		// result substitution table.
		for rec := range f.substitutionTable.GetInvalidationTokens() {
			if float64(int(now)) >= math.Abs(float64(rec.ConsiderationTime)) {
				// Insert the invalidation record in the result substitution table and
				// return false to indicate the invalidity of the formula
				f.resultSubstitutionTable.Insert(rec)
				return false
			}
		}
	}

	// If invalidation tokens do not invalidate the formula, we must check existing
	// records in the substitution table to determine the validity of the formula. If
	// there is a record in the substitution table whose consideration time is smaller
	// than the current time point, the formula is validated.
	for record := range f.substitutionTable.Records {
		if record.ConsiderationTime <= now {
			return true
		}
	}

	// The formula does not hold. If there is no invalidation token, we must insert one in the substitution table
	if config.UseInvalidationPrediction && f.substitutionTable.CountInvalidationTokens() == 0 {
		// Substitution tables are empty and the formula does not hold. So, the formula must return an invalidation token
		f.resultSubstitutionTable.Insert(st.CreateInvalidationToken(clock.GetNow(), clock.GetNow()))
	}

	return false
}

// Cleanup removes all substitution and invalidation records as well as
// expired unified records that have horizon time smaller than a certain
// time point.
func (f *AtomFormula) Cleanup() {
	t := clock.GetNow()
	// Clean up the substitution table
	f.substitutionTable.DeleteRecordsByHorizonTime(t)
	// Clean up the result substitution table
	f.resultSubstitutionTable.DeleteRecordsByHorizonTime(t)
	// Empty the list of recent records
	f.substitutionTable.DeleteRecentRecords()
	// Empty the list of recent records in the result substitution table
	f.resultSubstitutionTable.DeleteRecentRecords()

	// If there are already substitutions for the next time point in the substitution table
	// add them already in the result substitution table
	for record := range f.substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(int(t) + 1)) {
		f.resultSubstitutionTable.Insert(record)
	}
}

// Equals takes another formula and returns true if this formula is equal to the other formula. Returns
// false otherwise
func (f *AtomFormula) Equals(other formula.Formula) bool {
	// If the type of the two formulae does not match, return false
	if f.GetFormulaType() != other.GetFormulaType() {
		return false
	}

	// If predicates of the two formulae do not match, return false
	if f.predicate != other.GetPredicate() {
		return false
	}

	// If there is different number of variables in the two formulae, return false
	if f.substitutionTable.NumberOfVariables != other.GetEvaluationResults().NumberOfVariables {
		return false
	}

	return true
}

// GetVariableToRecordPositions returns a map from variable names to the position of coresponding substitutions in
// records of substitution table of the formula
func (f *AtomFormula) GetVariableToRecordPositions() map[types.Variable][]int {
	return f.substitutionTable.VariableToRecordIndex
}

// GetEvaluationResults returns the results of the formula's evaluation at current time point
// NOTE: Calling this function before calling Evaluate() would return an empty set
func (f *AtomFormula) GetEvaluationResults() *st.SubstitutionTable {
	return f.resultSubstitutionTable
}

// GetFormulaType returns formula.AtomFormula to indicate the type of this formula
func (*AtomFormula) GetFormulaType() formula.FormulaType {
	return formula.AtomFormulaTypeName
}

// IsNegated determines whether the formula is negated.
func (*AtomFormula) IsNegated() bool {
	// Returns false because p(X) is not negated
	return false
}

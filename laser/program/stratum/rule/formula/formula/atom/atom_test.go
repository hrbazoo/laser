package atom

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/timewindim"

	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit tests of atomFormulae in Laser stream reasoner.
 */

// TestAtomFormulaNum1 tests whether the creation of new formula
// works as expected
func TestAtomFormulaNum1(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables)

	// Check to make sure properties are set correctly
	assert.Equal(f.predicate, predicate) // The predicate
	// Check if the substitutiontable's variable-to-index map has correct length
	assert.Equal(len(f.substitutionTable.VariableToRecordIndex), len(variables))
	// Check if each variable is mapped to the right index value
	assert.Equal(f.substitutionTable.VariableToRecordIndex["A"], []int{0})
	assert.Equal(f.substitutionTable.VariableToRecordIndex["B"], []int{1})
}

// TestAtomFormulaNum2 tests whether the Insert function works as expected
func TestAtomFormulaNum2(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables)

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := considerationTime
	// The tuple counter of the record we feed to the formula
	considerationTupleCounter := types.NoTupleCounter
	// The horizon counter of the record we feed to the formula
	horizonCounter := types.NoTupleCounter
	// Create records
	record := substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the record is stored correctly in the
	// formula's substitution table or not

	// size of table must now be one
	assert.Equal(f.substitutionTable.Size(), 1)
	// Test if the record is stored correctly
	records := f.substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(1))
	assert.Equal(len(records), 1)

	// Check if the horizon time of the record is set correctly by the formula. Because consideration time
	// and the horizon time of the record are the same, getting the records with the horizon time that is
	// equal to the consideration time must return the record we just inserted in the table.
	f.substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(int(considerationTime)))

	// The size of table must be zero now
	assert.Equal(f.substitutionTable.Size(), 0)

	// Now, we test if the horizon time is calculated correctly if the record's horizon time
	// is larger than the sum of consideration time and the window's length

	// The horizon time of the new record would be larger than the sum of consideration time
	// and the window's length
	horizonTime = types.TimePoint(int(considerationTime) + 1)
	// Create records
	record = substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the record is stored correctly in the
	// formula's substitution table or not

	// size of table must now be one
	assert.Equal(f.substitutionTable.Size(), 1)
	// Test if the record is stored correctly
	records = f.substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(1))
	assert.Equal(len(records), 1)

	// Because the horizon time of the record extends beyond the time wondow, the formula
	// must not change the horizon time of the window
	// We do this by trying to remove the record by guessing the correct horizon table
	f.substitutionTable.DeleteRecordsByHorizonTime(horizonTime)

	// The size of table must be zero now
	assert.Equal(f.substitutionTable.Size(), 0)
}

// TestAtomFormulaNum3 tests whether the Evaluate function works as expected
func TestAtomFormulaNum3(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables)

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := considerationTime
	// The tuple counter of the record we feed to the formula
	considerationTupleCounter := types.NoTupleCounter
	// The horizon counter of the record we feed to the formula
	horizonCounter := types.NoTupleCounter
	// Create records
	record := substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the formula evaluation works as expected or not?
	// So, first call the Evaluate() function with current time point
	ok := f.Evaluate()

	// the formula must hold at this point
	assert.Equal(ok, true)

	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	// Remove expired substitution records
	f.Cleanup()

	// Test whether recent records are deleted after cleanup
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	// Go to the next time point
	clock.GoToNextTimePoint()

	// Evaluated the formula again. It must not hold
	ok = f.Evaluate()
	// The formula must not hold at this point
	assert.Equal(ok, false)
	// Now, the formula must return the invalidation token, because there is nothing in the substitution table, the formula
	// must return a new invalidation token at every time point. Therefore, there always is a recent invalidation token hereforth.
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
}

// TestAtomFormulaNum4 tests whether the Evaluate function works as expected
func TestAtomFormulaNum4(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables)

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.TimePoint(3)
	// The horizon time of the record we feed to the formula
	horizonTime := types.TimePoint(5)
	// The tuple counter of the record we feed to the formula
	considerationTupleCounter := types.NoTupleCounter
	// The horizon counter of the record we feed to the formula
	horizonCounter := types.NoTupleCounter
	// Create records
	record := substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the formula evaluation works as expected or not?
	// Because the atom we added to the formula begins to hold only at the time point 4,
	// the evaluation must fail right now.
	ok := f.Evaluate()
	assert.Equal(ok, false)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
	if config.UseInvalidationPrediction {
		assert.Equal(f.GetEvaluationResults().Size(), 1)
	} else {
		assert.Equal(f.GetEvaluationResults().Size(), 0)
	}
	assert.Equal(f.substitutionTable.Size(), 1)
	f.Cleanup()
	clock.GoToNextTimePoint()

	// Still fail
	ok = f.Evaluate()
	assert.Equal(ok, false)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
	if config.UseInvalidationPrediction {
		assert.Equal(f.GetEvaluationResults().Size(), 1)
	} else {
		assert.Equal(f.GetEvaluationResults().Size(), 0)
	}
	assert.Equal(f.substitutionTable.Size(), 1)
	f.Cleanup()
	clock.GoToNextTimePoint()

	// Must hold now
	ok = f.Evaluate()
	assert.Equal(ok, true)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)
	assert.Equal(f.GetEvaluationResults().Size(), 1)
	assert.Equal(f.substitutionTable.Size(), 1)
	f.Cleanup()
	clock.GoToNextTimePoint()

	// Must still hold
	ok = f.Evaluate()
	assert.Equal(ok, true)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)
	assert.Equal(f.GetEvaluationResults().Size(), 1)
	assert.Equal(f.substitutionTable.Size(), 1)
	f.Cleanup()
	clock.GoToNextTimePoint()

	// Must still hold
	ok = f.Evaluate()
	assert.Equal(ok, true)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)
	assert.Equal(f.GetEvaluationResults().Size(), 1)
	assert.Equal(f.substitutionTable.Size(), 1)
	f.Cleanup()
	clock.GoToNextTimePoint()

	// Must not hold any more
	ok = f.Evaluate()
	assert.Equal(ok, false)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
	if config.UseInvalidationPrediction {
		assert.Equal(f.GetEvaluationResults().Size(), 1)
	} else {
		assert.Equal(f.GetEvaluationResults().Size(), 0)
	}
	assert.Equal(f.substitutionTable.Size(), 0)
	f.Cleanup()
	clock.GoToNextTimePoint()
}

func TestAtomFormulaNum5(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create a formula
	f := Create(predicate, variables)

	assert.Equal(f.GetFormulaType(), formula.AtomFormulaTypeName)
}

func TestAtomFormulaNum6(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create the first formula
	f1 := Create(predicate, variables)
	// Create the second formula
	var f2 formula.Formula = Create(predicate, variables)

	assert.Equal(f1.Equals(f2), true)
}

func TestAtomFormulaNum7(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// The first set variables
	var1 := []types.Variable{"A", "B"}
	var2 := []types.Variable{"A"}

	// Create the first formula
	f1 := Create(predicate, var1)
	// Create the second formula
	var f2 formula.Formula = Create(predicate, var2)

	assert.Equal(f1.Equals(f2), false)
}

func TestAtomFormulaNum8(t *testing.T) {
	assert := cassert.New(t)

	// The first predicate
	pred1 := types.Predicate("p")
	// The second predicate
	pred2 := types.Predicate("q")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create the first formula
	f1 := Create(pred1, variables)
	// Create the second formula
	var f2 formula.Formula = Create(pred2, variables)

	assert.Equal(f1.Equals(f2), false)
}

func TestAtomFormulaNum9(t *testing.T) {
	assert := cassert.New(t)

	// The predicate
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// Create the first formula
	f1 := Create(predicate, variables)
	// Create the second formula
	var f2 formula.Formula = timewindim.Create(predicate, variables, 1)

	assert.Equal(f1.Equals(f2), false)
}

package not

import (
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	st "bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of Laser's incremental evaluation
 * of NOT formulae.
 */

// NotFormula defines the structure of Laser NOT formulae
type NotFormula struct {
	predicate               types.Predicate                     // The predicate of the formula
	substitutionTable       *st.SubstitutionTable               // The substitution table used to store variable substitutions
	resultSubstitutionTable *st.SubstitutionTable               // The substitution table used to store variable substitutions of formula conclusions
	results                 map[*st.SubstitutionRecord]struct{} // Keeps the output of formula after each evaluation
}

// Create takes the predicate and the variable names used in the formula and
// creates a NotFormula formula
func Create(pred types.Predicate) *NotFormula {
	// Create the formula and return its pointer
	return &NotFormula{
		predicate:               pred,
		substitutionTable:       st.New([]types.Variable{}),
		resultSubstitutionTable: st.New([]types.Variable{}),
		results:                 make(map[*st.SubstitutionRecord]struct{}, 0),
	}
}

// InsertResult inserts a result record into the result substitution table. This functionality
// is supported only for formulae that are used in the head of rules.
func (f *NotFormula) InsertResult(inputRecord *st.SubstitutionRecord) {
	panic("InsertResult into NotFormula is not supported")
}

// Insert takes a slice of substitutions and stores them
// in the substitution table of the formula
func (f *NotFormula) Insert(inputRecord *st.SubstitutionRecord) {
	// Create a new record with the content of input records
	record := *inputRecord
	// store the record in the substitution table
	f.substitutionTable.Insert(&record)
}

// GetPredicate returns formula's predicate
func (f *NotFormula) GetPredicate() types.Predicate {
	return f.predicate
}

// Evaluate evaluates the formula to determine whether it holds at the current
// time point or not. Returns a boolean value that indicates whether the formula holds or not
func (f *NotFormula) Evaluate() bool {
	now := clock.GetNow()

	// If the program is configured to use the invalidation prediction algorithm, it pays to first, iterate over
	// the invalidation tokens. If there are invalidation tokens, the formula does not hold. Return immediately
	if len(f.resultSubstitutionTable.InvalidationTokens) > 0 {
		return false
	}

	holds := true
	// Iterate over record in the substitution table
	for record := range f.substitutionTable.Records {
		// If the records are not invalidation tokens, the formula does not hold as long as the records
		// are valid. So, we must create and store an invalidation tokens in the substitution table (only
		// if the program is configured to use invalidation prediction algorithm)
		if record.ConsiderationTime > 0 {
			if config.UseInvalidationPrediction {
				f.resultSubstitutionTable.Insert(st.CreateInvalidationToken(record.ConsiderationTime, record.HorizonTime))
			}
			holds = false
		}
	}

	// If the formula holds, create a record in the substitution table to indicate the validity of the
	// formula at this time point
	if holds {
		record := st.CreateSubstitutionRecord(&[]types.Substitution{}, now, now)
		f.resultSubstitutionTable.Insert(record)
	}

	// Return the evaluation result
	return holds
}

// Cleanup removes all substitution and invalidation records as well as
// expired unified records that have horizon time smaller than a certain
// time point.
func (f *NotFormula) Cleanup() {
	t := clock.GetNow()
	// Clean up the substitution table
	f.substitutionTable.DeleteRecordsByHorizonTime(t)
	f.resultSubstitutionTable.DeleteRecordsByHorizonTime(t)
	f.substitutionTable.DeleteRecentRecords()
	f.resultSubstitutionTable.DeleteRecentRecords()
}

// Equals takes another formula and returns true if this formula is equal to the other formula. Returns
// false otherwise
func (f *NotFormula) Equals(other formula.Formula) bool {
	// If two formulae are of two different types, return false
	if f.GetFormulaType() != other.GetFormulaType() {
		return false
	}

	// If the predicate of the two formulae do not match, return false
	if f.GetPredicate() != other.GetPredicate() {
		return false
	}

	// We don't expect variables in the negated formula, but in case there are variables,
	// if two formulae have different number of variables, return false
	if f.substitutionTable.NumberOfVariables != other.GetEvaluationResults().NumberOfVariables {
		return false
	}

	// We don't expect variables in the negated formula, but in case there are variables,
	// if two formulae have different number of variables, return false
	if f.resultSubstitutionTable.NumberOfVariables != other.GetEvaluationResults().NumberOfVariables {
		return false
	}

	// All conditions are met, return true
	return true
}

// GetVariableToRecordPositions returns a map from variable names to the position of coresponding substitutions in
// records of substitution table of the formula
func (f *NotFormula) GetVariableToRecordPositions() map[types.Variable][]int {
	return f.substitutionTable.VariableToRecordIndex
}

// GetEvaluationResults returns the results of the formula's evaluation at current time point
// NOTE: For compatibility reasons, this function must never be called before evaluating the formula
func (f *NotFormula) GetEvaluationResults() *st.SubstitutionTable {
	// For the negation, we don't need a result substitution table. We can do everything with a
	// normal substitution table.
	return f.resultSubstitutionTable
}

// GetFormulaType returns formula.NotFormulaTypeName to indicate the type of this formula
func (*NotFormula) GetFormulaType() formula.FormulaType {
	return formula.NotFormulaTypeName
}

// IsNegated determines whether the formula is negated.
func (*NotFormula) IsNegated() bool {
	// Returns true because NOT(p(X)) is negated
	return true
}

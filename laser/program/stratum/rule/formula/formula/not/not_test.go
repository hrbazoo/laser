package not

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit tests of NOT formula in Laser stream reasoner.
 */

// TestNotNum1 tests whether the creation of NOT formula works as expected
func TestNotNum1(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate)

	// Check to make sure properties are set correctly
	assert.Equal(f.predicate, predicate) // The predicate
	// Check if the substitutiontable's variable-to-index map has correct length
	assert.Equal(len(f.substitutionTable.VariableToRecordIndex), 0)
}

func TestNotNum2(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Create the first formula
	var f1 formula.Formula = Create(predicate)
	// Create the second formula
	var f2 formula.Formula = Create(predicate)

	assert.Equal(f1.Equals(f2), true)
}

func TestNotNum3(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()
	// Predicate of the formula
	predicate1 := types.Predicate("p")

	// Create the first formula
	var f formula.Formula = Create(predicate1)
	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := types.TimePoint(int(considerationTime) + 1)
	// Create the record to store in the substitution table of the formula
	record := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{}, considerationTime, horizonTime)

	// Make sure the substitution tables are clean
	f.Cleanup()

	// Evaluate the formula. Since the substitution table is empty, the formula must hold
	ok := f.Evaluate()
	assert.Equal(ok, true)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	// Make sure the substitution tables are clean
	f.Cleanup()

	f.Insert(record)
	// Evaluate the formula. Since the substitution table is not empty, the formula must NOT hold
	ok = f.Evaluate()
	assert.Equal(ok, false)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)

	// Make sure the substitution tables are clean
	f.Cleanup()
	// Evaluate the formula. Since the substitution table is not empty, the formula must NOT hold
	ok = f.Evaluate()
	assert.Equal(ok, false)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().InvalidationTokens), 1)

	// Make sure the substitution tables are clean
	f.Cleanup()

	// Increment the clock counter
	clock.GoToNextTimePoint()
	// Evaluate the formula. Since the substitution table is not empty, the formula must NOT hold
	ok = f.Evaluate()
	assert.Equal(ok, false)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().InvalidationTokens), 1)

	// Make sure the substitution tables are clean
	f.Cleanup()

	// Increment the clock counter
	clock.GoToNextTimePoint()
	// Evaluate the formula. Since the substitution table is empty, the formula must hold
	ok = f.Evaluate()
	assert.Equal(ok, true)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	assert.CEqual(len(f.GetEvaluationResults().InvalidationTokens), 0)

	// Make sure the substitution tables are clean
	f.Cleanup()

	// Increment the clock counter
	clock.GoToNextTimePoint()
	// Evaluate the formula. Since the substitution table is empty, the formula must hold
	ok = f.Evaluate()
	assert.Equal(ok, true)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	assert.CEqual(len(f.GetEvaluationResults().InvalidationTokens), 0)

	// Make sure the substitution tables are clean
	f.Cleanup()

	// Insert the record in the formula's substitution table
	f.Insert(record)
	// Evaluate the formula. Since the substitution table is not empty, the formula must NOT hold
	ok = f.Evaluate()
	assert.Equal(ok, false)
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
}

package formula

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of a parent type "Formula" that is the super-class of all other formulae in Laser 2.0
 */

import (
	st "bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
)

// FormulaType defines the type in which a formula type is represented in the source code. This is used
// by formulae in the source code to report what type they are
type FormulaType string

// TimeWinDimFormulaTypeName represents the type of formula of type ⊞◇p(x)
const TimeWinDimFormulaTypeName = FormulaType("TimeWindDimFormula")

// TimeWinBoxFormulaTypeName represents the type of formula of type ⊞֍p(x)
const TimeWinBoxFormulaTypeName = FormulaType("TimeWinBoxFormula")

// AtomFormulaTypeName represents the type of formula of type p(x)
const AtomFormulaTypeName = FormulaType("AtomFormula")

// NotFormulaTypeName represents the type of formula of type not(p(X))
const NotFormulaTypeName = FormulaType("NotFormulaTypeName")

// Formula is the super-class (or parent type) of all other formulae in Laser 2.0
type Formula interface {
	Insert(inputRecord *st.SubstitutionRecord)
	InsertResult(inputRecord *st.SubstitutionRecord)
	GetPredicate() types.Predicate
	Evaluate() bool
	Equals(f Formula) bool
	IsNegated() bool
	Cleanup()
	GetVariableToRecordPositions() map[types.Variable][]int
	GetEvaluationResults() *st.SubstitutionTable
	GetFormulaType() FormulaType
}

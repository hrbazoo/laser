package timewindim

import (
	"math"

	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	st "bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of Laser's incremental evaluation
 * of formulae in the form of ⊞◇p(x), i.e., time-window, followed by a diamond
 * and a predicate.
 */

// TimeWindowDiamondFormula defines the structure of Laser formulae in form of
// ⊞◇p(x)
type TimeWindowDiamondFormula struct {
	predicate               types.Predicate       // The predicate of the formula
	substitutionTable       *st.SubstitutionTable // The substitution table used to store variable substitutions
	resultSubstitutionTable *st.SubstitutionTable // The substitution table used to store variable substitutions of formula conclusions
	WindowLength            int                   // The length of the time window
}

// Create takes the predicate and the variable names used in the formula and
// creates a TimeWindowDiamondFormula formula
func Create(pred types.Predicate, variables []types.Variable, winLen int) *TimeWindowDiamondFormula {
	// Create the formula and return its pointer
	return &TimeWindowDiamondFormula{
		predicate:               pred,
		substitutionTable:       st.New(variables),
		resultSubstitutionTable: st.New(variables),
		WindowLength:            winLen,
	}
}

// InsertResult inserts a result record into the result substitution table. This functionality
// is supported only for formulae that are used in the head of rules.
func (f *TimeWindowDiamondFormula) InsertResult(inputRecord *st.SubstitutionRecord) {
	panic("InsertResult into TimeWindowDiamondFormula is not supported")
}

// Insert takes a substitution record and stores it in the substitution table of the formula
func (f *TimeWindowDiamondFormula) Insert(inputRecord *st.SubstitutionRecord) {
	// Create a new record with the content of input records
	record := *inputRecord
	// If the input record is an invalidation token and the program is configured to not use the invalidation
	// prediction algorithm, this is a bug that must be fixed. Therefore, the program panics!
	if !config.UseInvalidationPrediction && record.ConsiderationTime < 0 {
		panic("Unexpected invalidation token in timewindim while the program did not expect invalidation tokens")
	}
	// We must extend the horizon time by the size of window if input is NOT an
	// invalidation token. If the input record is an invalidation token, simply
	// store it as it is in the substitution table
	if record.ConsiderationTime > 0 { // If the record is not an invalidation token
		// The new horizon time of the record that we write in the substitution table would be the maximum
		// of the consideration time plus the length of window and the horizon time of the input record
		record.HorizonTime = types.TimePoint(math.Max(float64(record.ConsiderationTime)+float64(f.WindowLength), float64(record.HorizonTime)))

	}
	// store it in the substitution table
	f.substitutionTable.Insert(&record)
	// If the consideration time of the current record is not in the future, it must
	// immediately go to the result substitution table.
	// NOTE: Invalidation tokens are not stored in the result substitution table
	if record.ConsiderationTime >= types.StartTimePoint && clock.GetNow() >= record.ConsiderationTime {
		f.resultSubstitutionTable.Insert(&record)
	}
}

// GetPredicate returns formula's predicate
func (f *TimeWindowDiamondFormula) GetPredicate() types.Predicate {
	return f.predicate
}

// Evaluate evaluates the formula to determine whether it holds at the current
// time point or not. Returns a boolean value that indicates whether the formula holds or not
func (f *TimeWindowDiamondFormula) Evaluate() bool {
	now := clock.GetNow()
	winLen := f.WindowLength // The length of the window

	// There are often fewer invalidation tokens than substitution records. Therefore, if
	// the program is allowed to use the invalidation prediction algorithm, it pays to
	// first check if we can invalidate the formula by finding an invalidation token that
	// does so.
	// If there is an invalidation token in the substitution table with a consideration
	// time smaller than the current time point minus the size of window, that means that
	// the invalidation token spans a time period larger than the whole window
	// In that case, the whole formula certainly does not hold. If anything like that found,
	// return immediately.
	if config.UseInvalidationPrediction {
		for rec := range f.substitutionTable.GetInvalidationTokens() {
			if f.substitutionTable.CountSubstitutionRecords() == 0 && math.Abs(float64(rec.ConsiderationTime)) <= float64(int(now)) {
				// Insert the invalidation record in the result substitution table and
				// return false to indicate the invalidity of the formula
				f.resultSubstitutionTable.Insert(rec)
				return false
			}
			if float64(int(now)-winLen) >= math.Abs(float64(rec.ConsiderationTime)) {

				// Insert the invalidation record in the result substitution table and
				// return false to indicate the invalidity of the formula
				f.resultSubstitutionTable.Insert(rec)
				return false
			}
		}
	}

	// If invalidation tokens do not invalidate the formula, we must check existing
	// records in the substitution table to determine the validity of the formula. If any
	// substitution record exists in the result substitution table, the validity of the formula is confirmed
	// No need for any further action.
	if f.resultSubstitutionTable.CountSubstitutionRecords() > 0 {
		// Results are already in the result substitution table.
		return true
	}

	if config.UseInvalidationPrediction {
		// Substitution tables are empty and the formula does not hold. So, the formula must return an invalidation token
		if f.substitutionTable.CountInvalidationTokens() == 0 {
			f.resultSubstitutionTable.Insert(st.CreateInvalidationToken(clock.GetNow(), clock.GetNow()))
		}
	}

	return false
}

// Cleanup removes all substitution and invalidation records as well as
// expired unified records that have horizon time smaller than a certain
// time point.
func (f *TimeWindowDiamondFormula) Cleanup() {
	t := clock.GetNow()
	// Clean up the substitution table
	f.substitutionTable.DeleteRecordsByHorizonTime(t)
	// Clean up the result substitution table
	f.resultSubstitutionTable.DeleteRecordsByHorizonTime(t)
	// Empty the list of recent records
	f.substitutionTable.DeleteRecentRecords()
	// Empty the list of recent records in the result substitution table
	f.resultSubstitutionTable.DeleteRecentRecords()

	// If there are already substitutions for the next time point in the substitution table
	// add them already in the result substitution table
	for record := range f.substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(int(t) + 1)) {
		f.resultSubstitutionTable.Insert(record)
	}
}

// Equals takes another formula and returns true if this formula is equal to the other formula. Returns
// false otherwise
func (f *TimeWindowDiamondFormula) Equals(other formula.Formula) bool {
	// If two formulae are of two different types, return false
	if f.GetFormulaType() != other.GetFormulaType() {
		return false
	}

	// If the predicate of the two formulae do not match, return false
	if f.GetPredicate() != other.GetPredicate() {
		return false
	}

	// If the length of windows do not match, return false
	if f.WindowLength != other.(*TimeWindowDiamondFormula).WindowLength {
		return false
	}

	// If two formulae have different number of variables, return false
	if f.substitutionTable.NumberOfVariables != other.GetEvaluationResults().NumberOfVariables {
		return false
	}

	// All conditions are met, return true
	return true
}

// GetVariableToRecordPositions returns a map from variable names to the position of coresponding substitutions in
// records of substitution table of the formula
func (f *TimeWindowDiamondFormula) GetVariableToRecordPositions() map[types.Variable][]int {
	return f.substitutionTable.VariableToRecordIndex
}

// GetEvaluationResults returns the results of the formula's evaluation at current time point
// NOTE: Calling this function before calling Evaluate() would return an empty set
func (f *TimeWindowDiamondFormula) GetEvaluationResults() *st.SubstitutionTable {
	return f.resultSubstitutionTable
}

// GetFormulaType returns formula.TimeWinDimFormula to indicate the type of this formula
func (*TimeWindowDiamondFormula) GetFormulaType() formula.FormulaType {
	return formula.TimeWinDimFormulaTypeName
}

// IsNegated determines whether the formula is negated.
func (*TimeWindowDiamondFormula) IsNegated() bool {
	// Returns false, because formulae of form ⊞◇p(x) are not negated
	return false
}

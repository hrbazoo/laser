package timewindim

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/atom"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit tests of timewin_diamond formulae in
 * Laser stream reasoner.
 */

// TestTimewinDiamondNum1 tests whether the creation of new formula
// works as expected
func TestTimewinDiamondNum1(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Check to make sure the window's properties are set correctly
	assert.Equal(f.WindowLength, winLength) // Length of the window
	assert.Equal(f.predicate, predicate)    // The predicate
	// Check if the substitutiontable's variable-to-index map has correct length
	assert.Equal(len(f.substitutionTable.VariableToRecordIndex), len(variables))
	// Check if each variable is mapped to the right index value
	assert.Equal(f.substitutionTable.VariableToRecordIndex["A"], []int{0})
	assert.Equal(f.substitutionTable.VariableToRecordIndex["B"], []int{1})
}

// TestTimewinDiamondNum2 tests whether the Insert function
// works as expected
func TestTimewinDiamondNum2(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := considerationTime
	// The tuple counter of the record we feed to the formula
	considerationTupleCounter := types.NoTupleCounter
	// The horizon counter of the record we feed to the formula
	horizonCounter := types.NoTupleCounter
	// Create records
	record := substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the record is stored correctly in the
	// formula's substitution table or not

	// size of table must now be one
	assert.Equal(f.substitutionTable.Size(), 1)
	// Test if the record is stored correctly
	records := f.substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(1))
	assert.Equal(len(records), 1)

	// Check if the horizon time of the record is set correctly by the formula. Because the
	// horizon time of the record is within the boundaries of the time window, the formula
	// must replace the record's horizon time with a new value which is equal to the addition
	// of the consideration time plus the window length. To check this, we try to remove the
	// record by guessing the correct horizon table
	f.substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(int(considerationTime) + winLength))

	// The size of table must be zero now
	assert.Equal(f.substitutionTable.Size(), 0)

	// Now, we test if the horizon time is calculated correctly if the record's horizon time
	// is larger than the sum of consideration time and the window's length

	// The horizon time of the new record would be larger than the sum of consideration time
	// and the window's length
	horizonTime = types.TimePoint(int(considerationTime) + winLength + 1)
	// Create records
	record = substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the record is stored correctly in the
	// formula's substitution table or not

	// size of table must now be one
	assert.Equal(f.substitutionTable.Size(), 1)
	// Test if the record is stored correctly
	records = f.substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(1))
	assert.Equal(len(records), 1)

	// Because the horizon time of the record extends beyond the time wondow, the formula
	// must not change the horizon time of the window
	// We do this by trying to remove the record by guessing the correct horizon table
	f.substitutionTable.DeleteRecordsByHorizonTime(horizonTime)

	// The size of table must be zero now
	assert.Equal(f.substitutionTable.Size(), 0)
}

// TestTimewinDiamondNum3 tests whether the Evaluate function
// works as expected
func TestTimewinDiamondNum3(t *testing.T) {
	cassert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := considerationTime
	// The tuple counter of the record we feed to the formula
	considerationTupleCounter := types.NoTupleCounter
	// The horizon counter of the record we feed to the formula
	horizonCounter := types.NoTupleCounter
	// Create records
	record := substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Current time point
	now := clock.GetNow()
	// Check if and for how long the formula holds
	for ; now <= types.TimePoint(int(considerationTime)+winLength); now = clock.GoToNextTimePoint() {
		// Now, we are going to check whether the formula evaluation works as expected or not?
		// So, first call the Evaluate() function with current time point
		ok := f.Evaluate()

		// the formula must hold at this point
		cassert.Equal(ok, true)

		// At the first time point, Evaluate() must return a single record
		if now == types.StartTimePoint {
			cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
			cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)
		} else { // At other time points, it must not return any records
			cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
			cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

		}

		// Remove expired substitution records
		f.Cleanup()
	}

	ok := f.Evaluate()

	// The formula must not hold at this point
	cassert.Equal(ok, false)
	// Now, the formula must return the invalidation token, because there is nothing in the substitution table, the formula
	// must return a new invalidation token at every time point. Therefore, there always is a recent invalidation token hereforth.
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
}

// TestTimewinDiamondNum4 tests whether the Evaluate function
// works as expected
func TestTimewinDiamondNum4(t *testing.T) {
	assert := cassert.New(t)
	// If the program is configured to not use invalidation prediction algorithm skip this test
	if !config.UseInvalidationPrediction {
		t.Skip()
	}
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Test whether GetPredicate works as expected
	assert.Equal(f.GetPredicate(), types.Predicate("p"))

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := types.TimePoint(int(considerationTime) + winLength)
	// The tuple counter of the record we feed to the formula
	considerationTupleCounter := types.NoTupleCounter
	// The horizon counter of the record we feed to the formula
	horizonCounter := types.NoTupleCounter
	// Create records
	record := substitutiontable.SubstitutionRecord{
		Substitutions:        []types.Substitution{},
		ConsiderationTime:    types.TimePoint(-considerationTime),
		ConsiderationCounter: types.TupleCounter(-considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the invalidation token to the formula
	f.Insert(&record)

	// Now, we are going to check whether the formula evaluation works as expected or not?

	// So, first call the Evaluate() function in the first time point
	ok := f.Evaluate()
	// The formula must not hold at this point
	assert.Equal(ok, false)
	// There is only a single invalidation token in the substitution table right now, but that
	// must not be returned
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.Equal(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Evaluate the formula in the subsequent time point.
	ok = f.Evaluate()
	// The formula must not hold at this point
	assert.Equal(ok, false)
	// There is only a single invalidation token in the substitution table right now, but that
	// must not be returned
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.Equal(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Evaluate the formula in the subsequent time point
	ok = f.Evaluate()
	// The formula must not hold at this point
	assert.Equal(ok, false)
	// Now, the formula must return the invalidation token, because there is nothing in the substitution table, the formula
	// must return a new invalidation token at every time point. Therefore, there always is a recent invalidation token hereforth.
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.Equal(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Evaluate the formula in the subsequent time point
	ok = f.Evaluate()
	// The formula must not hold at this point
	assert.Equal(ok, false)
	// Now, the formula must return the invalidation token, because there is nothing in the substitution table, the formula
	// must return a new invalidation token at every time point. Therefore, there always is a recent invalidation token hereforth.
	assert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	assert.Equal(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
}

// TestTimewinDiamondNum5 tests whether the Evaluate function
// works as expected. The only difference is that here we insert
// both a normal substitution record and an invalidation token into
// the substitution table.
func TestTimewinDiamondNum5(t *testing.T) {
	cassert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := types.TimePoint(int(considerationTime) + 1)

	// Create records
	record := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{types.Substitution('a'), types.Substitution('b')}, considerationTime, horizonTime)
	token := substitutiontable.CreateInvalidationToken(types.TimePoint(int(considerationTime)+1), types.TimePoint(int(considerationTime)+winLength+1))

	// Make sure all substitution tables are clean
	f.Cleanup()

	// Insert the substitution record to the formula
	f.Insert(record)
	// Insert the invalidation token to the formula (only if the program is configured to use invalidation prediction algorithm)
	if config.UseInvalidationPrediction {
		f.Insert(token)
	}

	ok := f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	ok = f.Evaluate()
	cassert.Equal(ok, false)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1) // The invalidation token spans the whole window now

	// Create another record
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{types.Substitution('a'), types.Substitution('b')}, types.TimePoint(6), types.TimePoint(7))

	// Insert the new record in the substitution table
	f.Insert(record)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Formula must hold and return one record
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Formula still holds but must not return anything
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Formula still holds but must not return any new record
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Formula still holds but must not return any new record
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Formula must not hold any more
	ok = f.Evaluate()
	cassert.Equal(ok, false)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)

	clock.Reset()

	// Test what happens if Evaluate() correctly processes a record that will hold in the future.
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{types.Substitution('a'), types.Substitution('b')}, types.TimePoint(3), types.TimePoint(7))

	f.Cleanup()

	// Formula must not hold any more
	ok = f.Evaluate()
	cassert.Equal(ok, false)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)

	f.Cleanup()
	clock.Reset()

	// Test what happens if Evaluate() correctly processes a record that will hold in the future.
	record = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{types.Substitution('a'), types.Substitution('b')}, types.TimePoint(2), types.TimePoint(3))

	// Insert the new record in the substitution table. This record holds in the future
	f.Insert(record)

	// As the record only holds in the future, the formula must not hold at this time point
	ok = f.Evaluate()
	cassert.Equal(ok, false)
	cassert.Equal(f.substitutionTable.Size(), 1)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Now the formula must hold
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(f.substitutionTable.Size(), 1)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 1)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Now the formula must hold
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(f.substitutionTable.Size(), 1)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Now the formula must still hold
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(f.substitutionTable.Size(), 1)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Now the formula must still hold because the length of window 3
	ok = f.Evaluate()
	cassert.Equal(ok, true)
	cassert.Equal(f.substitutionTable.Size(), 1)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 0)

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Now the formula must not hold any longer
	ok = f.Evaluate()
	cassert.Equal(ok, false)
	cassert.Equal(f.substitutionTable.Size(), 0)
	cassert.Equal(len(f.GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.CEqual(len(f.GetEvaluationResults().RecentInvalidationTokens), 1)
}

func TestTimewinDiamondNum6(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f1 := Create(predicate, variables, winLength)
	var f2 formula.Formula = Create(predicate, variables, winLength)

	assert.Equal(f1.Equals(f2), true)
}

func TestTimewinDiamondNum7(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	var1 := []types.Variable{"A", "B"}
	var2 := []types.Variable{"A", "C"}

	// The length of the window
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f1 := Create(predicate, var1, winLength)
	var f2 formula.Formula = Create(predicate, var2, winLength)

	assert.Equal(f1.Equals(f2), true)
}

func TestTimewinDiamondNum8(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	var1 := []types.Variable{"A", "B"}
	var2 := []types.Variable{"A"}

	// The length of the window
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f1 := Create(predicate, var1, winLength)
	var f2 formula.Formula = Create(predicate, var2, winLength)

	assert.Equal(f1.Equals(f2), false)
}

func TestTimewinDiamondNum9(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	var1 := []types.Variable{"A", "B"}
	var2 := []types.Variable{}

	// The length of the window
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f1 := Create(predicate, var1, winLength)
	var f2 formula.Formula = Create(predicate, var2, winLength)

	assert.Equal(f1.Equals(f2), false)
}

func TestTimewinDiamondNum10(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	var1 := []types.Variable{}
	var2 := []types.Variable{}

	// The length of the window
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f1 := Create(predicate, var1, winLength)
	var f2 formula.Formula = Create(predicate, var2, winLength)

	assert.Equal(f1.Equals(f2), true)
}

func TestTimewinDiamondNum11(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	pred1 := types.Predicate("p")
	pred2 := types.Predicate("q")

	// Variables of the formula
	var1 := []types.Variable{}
	var2 := []types.Variable{}

	// The length of the window
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f1 := Create(pred1, var1, winLength)
	var f2 formula.Formula = Create(pred2, var2, winLength)

	assert.Equal(f1.Equals(f2), false)
}

func TestTimewinDiamondNum12(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	var1 := []types.Variable{"A", "B"}
	var2 := []types.Variable{"A", "C"}

	// The length of windows
	winLength1 := 3
	winLength2 := 2

	// Create a formula and check whether it is created correctly or now
	f1 := Create(predicate, var1, winLength1)
	var f2 formula.Formula = Create(predicate, var2, winLength2)

	assert.Equal(f1.Equals(f2), false)
}

func TestTimewinDiamondNum13(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	vars := []types.Variable{"A", "B"}

	// The length of windows
	winLength := 3

	// Create a formula and check whether it is created correctly or now
	f1 := Create(predicate, vars, winLength)
	var f2 formula.Formula = atom.Create(predicate, vars)

	assert.Equal(f1.Equals(f2), false)
}

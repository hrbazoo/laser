package timewinbox

import (
	"math"

	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"

	st "bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of Laser's incremental evaluation
 * of formulae in the form of ⊞֍p(x), i.e., time-window, followed by a box
 * and a predicate.
 */

// TimeWindowBoxFormula defines the structure of Laser formulae in form of
// ⊞֍p(x)
type TimeWindowBoxFormula struct {
	predicate                     types.Predicate                                       // The predicate of the formula
	substitutionTable             *st.SubstitutionTable                                 // The substitution table that stores substitution records
	resultSubstitutionTable       *st.SubstitutionTable                                 // The substitution table that stores result substitutions records
	recordsGroupedBySubstitutions map[string]map[types.TimePoint]*st.SubstitutionRecord // Keeps substitution records which have the same substitutions in different time intervals
	WindowLength                  int                                                   // The length of the time window
}

// Create takes the predicate and the variable names used in the formula and
// creates a TimeWindowBoxFormula formula
func Create(pred types.Predicate, variables []types.Variable, wLen int) *TimeWindowBoxFormula {
	// Create the formula and return its pointer
	return &TimeWindowBoxFormula{
		predicate:                     pred,
		substitutionTable:             st.New(variables),
		resultSubstitutionTable:       st.New(variables),
		recordsGroupedBySubstitutions: map[string]map[types.TimePoint]*st.SubstitutionRecord{},
		WindowLength:                  wLen,
	}
}

// Takes a substitution record and creates a string key from that
// The generated string is such that it is only unique w.r.t. to
// substitutions and ignores consideration-time/horizon-time differences
func keyGen(record *st.SubstitutionRecord) string {
	key := ""
	// Go through substitutions and append them to the string
	for _, s := range record.Substitutions {
		key += string(s)
	}

	return key
}

// InsertResult inserts a result record into the result substitution table. This functionality
// is supported only for formulae that are used in the head of rules.
func (f *TimeWindowBoxFormula) InsertResult(inputRecord *st.SubstitutionRecord) {
	panic("InsertResult into TimeWindowBoxFormula is not supported")
}

// Insert takes a slice of substitutions and stores them
// in the substitution table of the formula
func (f *TimeWindowBoxFormula) Insert(inputRecord *st.SubstitutionRecord) {
	// Create a new record with the content of input records
	record := *inputRecord

	record.HorizonTime = types.TimePoint(int(record.HorizonTime) + f.WindowLength)

	// Store the record in the substitution table
	f.substitutionTable.Insert(&record)

	// If the record is not an invalidation token, group the records with similar substitutions
	if record.ConsiderationTime > 0 {
		// Generate a key to group records together
		key := keyGen(&record)
		// Does the substitution table have a record with such a key?
		if similarRecords, exists := f.recordsGroupedBySubstitutions[key]; exists {
			// If a record with the same substitutions and same consideration time already exists, only
			// add it to the group if it can extend the horizon time of the record
			if r, ok := similarRecords[record.ConsiderationTime]; ok {
				if r.HorizonTime < record.HorizonTime {
					similarRecords[record.ConsiderationTime] = &record
				}
			} else { // No record with such consideration time exists in the group, add the mapping from
				similarRecords[record.ConsiderationTime] = &record
			}
		} else {
			// If such a record does not already exist in the set of unified records, we have to add it
			f.recordsGroupedBySubstitutions[key] = map[types.TimePoint]*st.SubstitutionRecord{}
			f.recordsGroupedBySubstitutions[key][record.ConsiderationTime] = &record
		}
	}
}

// Evaluate evaluates the formula to determine whether it holds at the current
// time point or not. Returns a boolean value that indicates whether the formula holds or not
func (f *TimeWindowBoxFormula) Evaluate() bool {
	now := clock.GetNow()
	winLen := f.WindowLength // The length of the window

	// The existence of one invalidation token is enough to invalidate the formula. So, when the
	// program is configured to use invalidation prediction algorithm, it pays if we first check
	// to see if there is any invalidation token in the substitution table. If found, we will
	// return a "false" value to show that the formula does not hold.
	// NOTE: for efficiency, we NEVER store invalidation tokens in the normal substitution table.
	// instead, we only store invalidation tokens in the result substitution table.
	if config.UseInvalidationPrediction {
		if f.substitutionTable.CountInvalidationTokens() > 0 {
			for r := range f.substitutionTable.GetInvalidationTokens() {
				f.resultSubstitutionTable.Insert(r)
			}
			return false
		}
		if f.resultSubstitutionTable.CountInvalidationTokens() > 0 {
			for r := range f.resultSubstitutionTable.GetInvalidationTokens() {
				f.resultSubstitutionTable.Insert(r)
			}
			return false
		}
	}

	// If any of existing similar records exists in every time point in the whole window, it validated
	// the formula and we must return it. If the formula does not hold, we need to remember the last time point
	// at which the formula failed. From that time point, for a period as long as the length of window
	// the formula surely does not hold.
	invalidityTimePoint := types.NoTimePoint
	for _, records := range f.recordsGroupedBySubstitutions {
		failed := false
		// At every time point in the window a record with this predicate and these substitutions must exist
		for t := now; t > 0 && (int(t) >= int(now)-f.WindowLength); t-- {
			if _, exists := records[t]; !exists {
				// If the record does not exist at this time point, set the flag.
				failed = true
				invalidityTimePoint = t
				break
			}
		}
		if !failed {
			// Create the result substitution record. The consideration and horizon times are set to the current time point
			f.resultSubstitutionTable.Insert(st.CreateSubstitutionRecord(&records[now].Substitutions, now, now))
		}
	}

	// if found any record that validates the formula, return a True boolean value
	if f.resultSubstitutionTable.CountSubstitutionRecords() > 0 {
		return true
	}

	// The formula does not hold, so, return an invalidation token (if the program is configured to use invalidation prediction
	// algorithm) as well as False boolean value
	if config.UseInvalidationPrediction {
		// If the time point in which the formula becomes invalid is not found, it could be the result of having no substitution
		// record in the substitution table. If that is the case, the formula is invalid as of now. Otherwise, something is wrong
		// with the algorithm, so, we panic!!
		if invalidityTimePoint == types.NoTimePoint && f.substitutionTable.CountSubstitutionRecords() > 0 {
			panic("It must never happen that the TimeWinBox is invalid without a proper invalidity time point being found")
		} else if invalidityTimePoint == types.NoTimePoint && f.substitutionTable.CountSubstitutionRecords() == 0 {
			invalidityTimePoint = now
		}
		// The invalidation token that is written in the result substitution record indicates the invalidity of the formula
		// from the time point in which the formula became invalid
		itct := invalidityTimePoint
		// Add the length of window to the time of invalidity to get the horizon time of the invalidation token
		itht := int(invalidityTimePoint) + winLen
		// If the horizon time of the invalidation token is smaller than the current time point, use the current time point as
		// the horizon time.
		it := st.CreateInvalidationToken(itct, types.TimePoint(math.Max(float64(itht), float64(now))))
		f.resultSubstitutionTable.Insert(it)
	}
	return false
}

// Cleanup removes all substitution and invalidation records as well as
// expired unified records that have horizon time smaller than a certain
// time point.
func (f *TimeWindowBoxFormula) Cleanup() {
	now := clock.GetNow()
	// Cleanup the substitution table
	records := f.substitutionTable.GetRecordsByHorizonTime(now)

	// Iterate over grouped records
	for r := range records {
		key := keyGen(r) // Generate the key

		// If the key exists in the set of grouped records
		if similarRecords, exists := f.recordsGroupedBySubstitutions[key]; exists {
			// Create a map from time to record which will include non-expired records
			newSimilarRecords := map[types.TimePoint]*st.SubstitutionRecord{}
			// Iterate over records in the each group to filter out the expired records
			for _, r := range similarRecords {
				// If the record has expired, ignore it
				if r.HorizonTime < now {
					continue
				}
				// A mapping from the consideration time of this record already exists? If so, check whether the
				// horizon time of the new record can extend the horizon time of the current mapping
				if record, ok := newSimilarRecords[r.ConsiderationTime]; ok {
					// If the horizon time of current record is larger than the horizon time of an existing record,
					// expend the horizon time
					if r.HorizonTime < record.HorizonTime {
						newSimilarRecords[r.ConsiderationTime] = record
					}
				} else { // Mapping from the consideration time to record does not exist. Create the mapping
					newSimilarRecords[r.ConsiderationTime] = r
				}
			}
			// Assing the new mappings to the group created for these substitutions.
			// If there is no records for this key, remove it
			if len(newSimilarRecords) == 0 {
				delete(f.recordsGroupedBySubstitutions, key)
			} else {
				f.recordsGroupedBySubstitutions[key] = newSimilarRecords
			}
		}
	}

	// Delete the records from the substitution tables
	f.substitutionTable.DeleteRecordsByHorizonTime(now)
	f.substitutionTable.DeleteRecentRecords()
	f.resultSubstitutionTable.DeleteRecordsByHorizonTime(now)
	f.resultSubstitutionTable.DeleteRecentRecords()
}

// GetPredicate returns formula's predicate
func (f *TimeWindowBoxFormula) GetPredicate() types.Predicate {
	return f.predicate
}

// Equals takes another formula and returns true if this formula is equal to the other formula. Returns
// false otherwise
func (f *TimeWindowBoxFormula) Equals(other formula.Formula) bool {
	// If two formulae are of two different types, return false
	if f.GetFormulaType() != other.GetFormulaType() {
		return false
	}

	// If the predicate of the two formulae do not match, return false
	if f.GetPredicate() != other.GetPredicate() {
		return false
	}

	// If the length of windows do not match, return false
	if f.WindowLength != other.(*TimeWindowBoxFormula).WindowLength {
		return false
	}

	// If two formulae have different number of variables, return false
	if f.substitutionTable.NumberOfVariables != other.GetEvaluationResults().NumberOfVariables {
		return false
	}

	// All conditions are met, return true
	return true
}

// GetVariableToRecordPositions returns a map from variable names to the position of coresponding substitutions in
// records of substitution table of the formula
func (f *TimeWindowBoxFormula) GetVariableToRecordPositions() map[types.Variable][]int {
	return f.substitutionTable.VariableToRecordIndex
}

// GetEvaluationResults returns the results of the formula's evaluation at current time point
// NOTE: Calling this function before calling Evaluate() would return an empty set
func (f *TimeWindowBoxFormula) GetEvaluationResults() *st.SubstitutionTable {
	return f.resultSubstitutionTable
}

// GetFormulaType returns formula.TimeWinBoxFormula to indicate the type of this formula
func (*TimeWindowBoxFormula) GetFormulaType() formula.FormulaType {
	return formula.TimeWinBoxFormulaTypeName
}

// IsNegated determines whether the formula is negated.
func (*TimeWindowBoxFormula) IsNegated() bool {
	// Returns false, because formulae of form ⊞֍p(x) are not negated
	return false
}

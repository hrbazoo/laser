package timewinbox

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	st "bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit tests of timeWindowBox formulae in
 * Laser stream reasoner.
 */

// TestTimewinBoxNum1 tests whether the creation of new formula
// works as expected
func TestTimewinBoxCreate1(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Check to make sure the window's properties are set correctly
	assert.Equal(f.WindowLength, winLength) // Length of the window
	assert.Equal(f.predicate, predicate)    // The predicate
	// Check if the substitutiontable's variable-to-index map has correct length
	assert.Equal(len(f.substitutionTable.VariableToRecordIndex), len(variables))
	// Check if each variable is mapped to the right index value
	assert.Equal(f.substitutionTable.VariableToRecordIndex["A"], []int{0})
	assert.Equal(f.substitutionTable.VariableToRecordIndex["B"], []int{1})
}

// TestTimewinBoxNum2 tests whether the Insert function works as expected
func TestTimewinBoxInsert2(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Now, we are going to create the input record and feed it to the formula

	// The consideration time of the record we feed to the formula
	considerationTime := types.StartTimePoint
	// The horizon time of the record we feed to the formula
	horizonTime := considerationTime
	// The tuple counter of the record we feed to the formula
	considerationTupleCounter := types.NoTupleCounter
	// The horizon counter of the record we feed to the formula
	horizonCounter := types.NoTupleCounter
	// Create records
	record := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the record is stored correctly in the
	// formula's substitution table or not

	// size of table must now be one
	assert.Equal(f.substitutionTable.Size(), 1)
	// Test if the record is stored correctly
	records := f.substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(1))
	assert.Equal(len(records), 1)

	// Check if the horizon time of the record is set correctly by the formula. The formula
	// must extend the horizon time to the end of the window
	records = f.substitutionTable.GetRecordsByHorizonTime(types.TimePoint(int(horizonTime) + f.WindowLength))

	// The size of table must be zero now
	assert.Equal(len(records), 1)

	// Now, we test if the horizon time is calculated correctly if the record's horizon time
	// is larger than the sum of consideration time and the window's length

	// The horizon time of the new record would be larger than the sum of consideration time
	// and the window's length
	horizonTime = types.TimePoint(int(considerationTime) + winLength + 1)
	// Create records
	record = st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(considerationTime),
		ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
		HorizonTime:          types.TimePoint(horizonTime),
		HorizonCounter:       types.TupleCounter(horizonCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we are going to check whether the record is stored correctly in the
	// formula's substitution table or not

	// size of table must now be two
	assert.Equal(f.substitutionTable.Size(), 2)

	// Test if the record is stored correctly
	records = f.substitutionTable.GetRecordsByHorizonTime(types.TimePoint(int(horizonTime) + f.WindowLength))
	assert.Equal(len(records), 1)

	// Test if both records are in the substitution table
	records = f.substitutionTable.GetRecordsByConsiderationTime(considerationTime)
	assert.Equal(len(records), 2)

	// Remove the second record from the table
	f.substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(int(horizonTime) + f.WindowLength))
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 1)
	// Remove the first record from the table
	f.substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(1 + f.WindowLength))
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 0)

	// There must be no record in the substitution table right now
	assert.Equal(f.substitutionTable.Size(), 0)

	if config.UseInvalidationPrediction {
		// Create an invalidation token
		record = st.SubstitutionRecord{
			Substitutions:        []types.Substitution{},
			ConsiderationTime:    types.TimePoint(-1),
			ConsiderationCounter: types.TupleCounter(considerationTupleCounter),
			HorizonTime:          types.TimePoint(1),
			HorizonCounter:       types.TupleCounter(horizonCounter),
		}

		// Insert the invalidation token in the substitution table
		f.Insert(&record)

		// The must be no ordinary substitution record in the substitution table
		assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 0)
		// There must be one invalidation token in the substitution table
		assert.Equal(f.substitutionTable.Size(), 1)

		// Check whether the formula has extended the horizon time of the invalidation token
		// to the end of window
		records = f.substitutionTable.GetRecordsByHorizonTime(types.TimePoint(1 + winLength))
		// There must be one record
		assert.Equal(len(records), 1)

		// Remove the last elements from the substitution table
		f.substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(1 + winLength))
		// There must be no record in the substitution table right now
		assert.Equal(f.substitutionTable.Size(), 0)
	}
}
func TestTimewinBoxInsert3(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Create records
	r1 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(1),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(2),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}
	r2 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(2),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}
	r3 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(3),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(4),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r1)
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.substitutionTable.CountInvalidationTokens(), 0)
	}
	assert.Equal(len(f.recordsGroupedBySubstitutions), 1)
	assert.Equal(len(f.recordsGroupedBySubstitutions["a1b1"]), 1)

	f.Insert(&r2)
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 2)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.substitutionTable.CountInvalidationTokens(), 0)
	}
	assert.Equal(len(f.recordsGroupedBySubstitutions), 1)
	assert.Equal(len(f.recordsGroupedBySubstitutions["a1b1"]), 2)

	f.Insert(&r3)
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 3)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.substitutionTable.CountInvalidationTokens(), 0)
	}
	assert.Equal(len(f.recordsGroupedBySubstitutions), 1)
	assert.Equal(len(f.recordsGroupedBySubstitutions["a1b1"]), 3)
}

func TestTimewinBoxInsert4(t *testing.T) {
	assert := cassert.New(t)

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Create records
	r1 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(1),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(2),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}
	r2 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(2),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}
	r3 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(3),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(4),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r3)
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.substitutionTable.CountInvalidationTokens(), 0)
	}
	assert.Equal(len(f.recordsGroupedBySubstitutions), 1)
	assert.Equal(len(f.recordsGroupedBySubstitutions["a1b1"]), 1)

	f.Insert(&r1)
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 2)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.substitutionTable.CountInvalidationTokens(), 0)
	}
	assert.Equal(len(f.recordsGroupedBySubstitutions), 1)
	assert.Equal(len(f.recordsGroupedBySubstitutions["a1b1"]), 2)

	f.Insert(&r2)
	assert.Equal(f.substitutionTable.CountSubstitutionRecords(), 3)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.substitutionTable.CountInvalidationTokens(), 0)
	}
	assert.Equal(len(f.recordsGroupedBySubstitutions), 1)
	assert.Equal(len(f.recordsGroupedBySubstitutions["a1b1"]), 3)
}

// TestTimewinBoxNum1 tests whether the Evaluation function works as expected
func TestTimewinBoxEval1(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Now, we are going to create the input record and feed it to the formula

	// Create records
	record := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(1),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(winLength + 1),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we evaluate the formula and it must hold
	holds := f.Evaluate()
	assert.Equal(holds, true)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 0)
	}
	for r := range f.GetEvaluationResults().Records {
		assert.Equal(r.Substitutions, []types.Substitution{"a1", "b1"})
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}
}

func TestTimewinBoxEval2(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Now, we are going to create the input record and feed it to the formula

	// Create records
	record := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow()),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we evaluate the formula and it must hold
	holds := f.Evaluate()
	assert.Equal(holds, true)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 0)
	}
	for r := range f.GetEvaluationResults().Records {
		assert.Equal(r.Substitutions, []types.Substitution{"a1", "b1"})
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r1 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow()),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r1)
	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r2 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow()),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r2)
	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r3 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow()),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r3)
	holds = f.Evaluate()

	assert.Equal(holds, true)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 0)
	}
}

func TestTimewinBoxEval3(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	// Predicate of the formula
	predicate := types.Predicate("p")

	// Variables of the formula
	variables := []types.Variable{"A", "B"}

	// The length of the window
	winLength := 2

	// Create a formula and check whether it is created correctly or now
	f := Create(predicate, variables, winLength)

	// Now, we are going to create the input record and feed it to the formula

	// Create records
	record := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow()),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	// Feed the substitution record to the formula
	f.Insert(&record)

	// Now, we evaluate the formula and it must hold
	holds := f.Evaluate()
	assert.Equal(holds, true)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 0)
	}
	for r := range f.GetEvaluationResults().Records {
		assert.Equal(r.Substitutions, []types.Substitution{"a1", "b1"})
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}
	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()
	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r1 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow() + 3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r1)
	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r2 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow() + 3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r2)
	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r3 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow() + 3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r3)
	holds = f.Evaluate()

	assert.Equal(holds, true)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 0)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r4 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow() + 3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r4)
	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r5 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow() + 3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r5)
	holds = f.Evaluate()

	assert.Equal(holds, false)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 1)
	}

	f.Cleanup()
	clock.GoToNextTimePoint()

	// Create records
	r6 := st.SubstitutionRecord{
		Substitutions:        []types.Substitution{"a1", "b1"},
		ConsiderationTime:    types.TimePoint(clock.GetNow()),
		ConsiderationCounter: types.TupleCounter(types.NoTupleCounter),
		HorizonTime:          types.TimePoint(clock.GetNow() + 3),
		HorizonCounter:       types.TupleCounter(types.NoTupleCounter),
	}

	f.Insert(&r6)
	holds = f.Evaluate()

	assert.Equal(holds, true)
	assert.Equal(f.GetEvaluationResults().CountSubstitutionRecords(), 1)
	if config.UseInvalidationPrediction {
		assert.CEqual(f.GetEvaluationResults().CountInvalidationTokens(), 0)
	}
}

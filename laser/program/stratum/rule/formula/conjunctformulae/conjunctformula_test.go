package conjunctformulae

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/atom"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the test units of ConjunctFormulae
 */

func TestConjuncFormula1(t *testing.T) {
	assert := cassert.New(t)

	predicate1 := types.Predicate("p")
	predicate2 := types.Predicate("q")
	variable1 := []types.Variable{"X", "Y"}
	variable2 := []types.Variable{"Y", "Z"}

	atomFormula1 := atom.Create(predicate1, variable1)
	atomFormula2 := atom.Create(predicate2, variable2)

	formulae1 := []formula.Formula{atomFormula1, atomFormula2}
	formulae2 := []formula.Formula{atomFormula1, atomFormula2}
	cf1 := CreateConjFormulae(&formulae1, types.NoTimePoint, types.NoTupleCounter)
	cf2 := CreateConjFormulae(&formulae2, types.NoTimePoint, types.NoTupleCounter)

	assert.Equal(cf1.Equals(cf2), true)
}

func TestConjuncFormula2(t *testing.T) {
	assert := cassert.New(t)

	predicate1 := types.Predicate("p")
	predicate2 := types.Predicate("q")
	predicate3 := types.Predicate("r")
	variable1 := []types.Variable{"X", "Y"}
	variable2 := []types.Variable{"Y", "Z"}

	atomFormula1 := atom.Create(predicate1, variable1)
	atomFormula2 := atom.Create(predicate2, variable2)
	atomFormula3 := atom.Create(predicate3, variable2)

	formulae1 := []formula.Formula{atomFormula1, atomFormula2}
	formulae2 := []formula.Formula{atomFormula1, atomFormula3}
	cf1 := CreateConjFormulae(&formulae1, types.NoTimePoint, types.NoTupleCounter)
	cf2 := CreateConjFormulae(&formulae2, types.NoTimePoint, types.NoTupleCounter)

	assert.Equal(cf1.Equals(cf2), false)
}

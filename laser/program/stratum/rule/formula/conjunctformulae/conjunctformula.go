package conjunctformulae

import (
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the definition of ConjunctFormulae which is an array of
 * formulae that are all in conjunction relation with each other.
 */

// ConjFormulae is a slice of formula in conjunction relation with each other
type ConjFormulae struct {
	Formulae                 []formula.Formula
	InvalidUntilTimePoint    types.TimePoint
	InvalidUntilTupleCounter types.TupleCounter
}

// CreateConjFormulae takes a list of formulae, a time point until which the conjunctted formula does not hold
// as well as a tuple counter until which the conjunctted formulae does not hold, creates the conjunctted formulae and
// returns it.
func CreateConjFormulae(formulae *[]formula.Formula, invalidUntilTimePoint types.TimePoint, invalidUntilTupleCounter types.TupleCounter) *ConjFormulae {
	return &ConjFormulae{
		Formulae:                 *formulae,
		InvalidUntilTimePoint:    invalidUntilTimePoint,
		InvalidUntilTupleCounter: invalidUntilTupleCounter,
	}
}

// Equals method of a ConjunctFormulae takes another conjunct-formulae and returns
// true if the two are equal and false otherwise.
// IMPORTANT: this method does not take into account special cases such as p(X,Y), p(Y,X) == p(X,X)
func (cf *ConjFormulae) Equals(other *ConjFormulae) bool {
	if len(cf.Formulae) != len(other.Formulae) {
		return false
	}

	// Check whether all predicates in one conjunct-formula also exists in the other
	equalityIndex1 := map[int]struct{}{}
	equalityIndex2 := map[int]struct{}{}
	// If conjunct-formulae don't match, return false
	for i1, f1 := range cf.Formulae {
		equalityIndex1[i1] = struct{}{}
		for i2, f2 := range other.Formulae {
			if f1.Equals(f2) {
				equalityIndex2[i2] = struct{}{}
			}
		}
	}

	// The two sets must have exactly the same number of indexes
	if len(equalityIndex1) != len(equalityIndex2) {
		return false
	}
	return true
}

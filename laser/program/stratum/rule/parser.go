package rule

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/conjunctformulae"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/atom"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/not"

	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/timewinbox"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/timewindim"

	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of rule parser in Laser 2.0
 */

// Rules in Laser 2.0 follow the following structure
// <head atom formula> := <body formula 1>, <body formula 2>, ...

// These are the reserved delims
var delims = map[rune]struct{}{
	',': struct{}{},
	'(': struct{}{},
	')': struct{}{},
}

// TimeWindowDiamondOperator is how timeWinDiamond operator is represented in the rule
const TimeWindowDiamondOperator = "timewindim"

// TimeWindowBoxOperator is how timeWinBox operator is represented in the rule
const TimeWindowBoxOperator = "timewinbox"

var windowOperators = map[string]struct{}{
	"timewindim": struct{}{},
	"timewinbox": struct{}{},
}

// negationOperator is how negation is represented in the rule
var negationOperator = "not"

// RuleConclusionSign is what separates the head formula from the rule's body
const RuleConclusionSign = ":="

// Determines whether the given string is the name of a window operator or not
func isWindowOperator(s string) bool {
	if _, exists := windowOperators[s]; exists {
		return true
	}
	return false
}

// Prints an error message and aborts the program
func syntaxError(msg string) {
	panic(msg)
}

// Takes a list of tokens and an index value. Increments the index value and throws a syntax error
// if the index value becomes larger than the length of token list.
func goToNextToken(tokens *[]string, idx *int) {
	(*idx)++
	if len(*tokens) < *idx {
		syntaxError(fmt.Sprintf("Not enough tokens to parse: \"%s\"", strings.Join(*tokens, "")))
	}
}

// Takes a string value, a list of tokens and the index of a token in the list and
// throws a system error if the string value does not match with the token pointed at by the index.
func expect(token string, tokens *[]string, idx int, msg string) {
	if (*tokens)[idx] != token {
		syntaxError(msg)
	}
}

// Takes a string value, a list of tokens and the index of a token in the list and
// throws a system error if the string value matches with the token pointed at by the index.
func unexpect(token string, tokens *[]string, idx int, msg string) {
	if (*tokens)[idx] == token {
		syntaxError(msg)
	}
}

// Takes a rule, and returns two separate strings: one for the head and one for the body
func separateHeadAndBody(rule string) (string, string) {
	// Splits the string with the conclusionSing as the separator
	parts := strings.Split(rule, RuleConclusionSign)
	// There must be exactly two pieces after the split. In any other case it must return
	// an error message
	if len(parts) < 2 {
		s := fmt.Sprintf("No head found in rule: \"%s\"!", rule)
		panic(s)
	}
	if len(parts) > 2 {
		s := fmt.Sprintf("Multiple conclusion signs found in rule:\"%s\"!", rule)
		panic(s)
	}
	// Remove the redundant white spaces and return the pieces
	return strings.TrimSpace(parts[0]), strings.TrimSpace(parts[1])
}

// Takes a rule, separates the head and the body and returns two lists of tokens,
// one holding the tokens of the head and the other holding the tokens of the body
func tokenizer(rule string) ([]string, []string) {
	headString, bodyString := separateHeadAndBody(rule)
	return tokenize(headString), tokenize(bodyString)
}

// Takes a string to tokenize and returns a list of tokens extracted from that string.
func tokenize(s string) []string {
	tokens := []string{}
	buf := ""
	for _, r := range s {
		// Ignore white space characters
		if r == ' ' || r == '\t' || r == '\n' {
			continue
		}

		if _, exists := delims[r]; exists {
			// If there is no character in the buffer, ignore it
			if len(buf) > 0 {
				tokens = append(tokens, buf)
			}
			tokens = append(tokens, string(r))
			buf = ""
			continue
		}
		// append the character to the string
		buf += string(r)
	}

	// If the buffer is not empty, add it as the last token to the list
	if len(buf) > 0 {
		tokens = append(tokens, buf)
	}
	return tokens
}

// Parses atom formula of the form p, or p(X,...)
func parseAtom(atomTokens []string) (types.Predicate, []types.Variable, int) {
	if len(atomTokens) == 0 {
		msg := fmt.Sprintf("No token found in the atom \"%s\"", strings.Join(atomTokens, ""))
		panic(msg)
	}

	unexpect(",", &atomTokens, 0, fmt.Sprintf("Unexpected \",\" at the beginning of atom formula: \"%s\"\n", strings.Join(atomTokens, "")))
	// The first token must always be the predicate
	predicate := types.Predicate(atomTokens[0])

	// If there are fewer than two tokens in the atom, it means that the formula consists only
	// of a single predicate with no variable. So, return the predicate with an empty list of
	// variables
	if len(atomTokens) < 2 || atomTokens[1] == "," {
		vars := make([]types.Variable, 0)
		return predicate, vars, 1
	}

	// Immediately after the predicate must be an open parenthesis
	expect("(", &atomTokens, 1, fmt.Sprintf("Expected '(', but found %s\n", atomTokens[1]))

	// The rest of this function involves looping over the list of tokens in order to parse the
	// list of variables between the open and close parenthesis.
	vars := []types.Variable{}
	lastIndex := len(atomTokens) // The default last index in the list of tokens
	for i := 2; i < len(atomTokens); i++ {
		token := atomTokens[i]

		// This should indicate the end of parsing
		if token == ")" {
			// If there is an open parenthesis but no variable, throw an error
			if len(vars) == 0 {
				msg := fmt.Sprintf("Expected variables or constants in atom \"%s\" but found nothing\n", strings.Join(atomTokens, ""))
				panic(msg)
			}
			// Update the last index
			lastIndex = i + 1
			// We are done with parsing. Return the results
			break
		}

		// Anything that is not a comma or close parenthesis must be a variable at this point
		if token != "," {
			// Add it to the list of variables if it is an upper-case string. Otherwise, throw an error
			// message and abort the process
			if token == strings.ToUpper(token) {
				vars = append(vars, types.Variable(token))
			} else { // Throw an error message and abort
				msg := fmt.Sprintf("Unexpected token \"%s\" in atom \"%s\"\n", token, strings.Join(atomTokens, ""))
				panic(msg)
			}
		}
	}

	if len(vars) > 0 {
		expect(")", &atomTokens, lastIndex-1, fmt.Sprintf("Expected \")\" at the end of variable list in atom formula in: \"%s\"\n", strings.Join(atomTokens, "")))
		unexpect(",", &atomTokens, lastIndex-2, fmt.Sprintf("Unexpected \",\" before \")\" at the end of variable list in atom formula in: \"%s\"\n", strings.Join(atomTokens, "")))
	}

	return predicate, vars, lastIndex
}

// Takes a list of tokens that belong to a window operator and parses the tokens and returns the predicate,
// variables, length of the window as well as the step size. Furthermore, it also returns the index to token that
// immediately follows the window operator.
func parseTimeWinOperator(tokens []string) (types.Predicate, []types.Variable, int, int, int) {
	// We need at least 7 tokens to be able to parse a window operator
	if len(tokens) < 7 {
		msg := fmt.Sprintf("No enough tokens to parse time window formula: \"%s\"", strings.Join(tokens, ""))
		panic(msg)
	}

	// The name of window operator is not included in the token list, so, the first token must be an open parenthesis
	if tokens[0] != "(" {
		syntaxError(fmt.Sprintf("Expected '(' but found \"%s\" in formula: \"%s\"", tokens[0], strings.Join(tokens, "")))
	}

	// Parse the atom formula
	predicate, variables, tokenIndexIncr := parseAtom(tokens[1:])

	// Calculate the current token index. It is the sum of already processed tokens (one) and the number of tokens consumed
	// in parse the atomFormula
	curTokenIndex := 1 + tokenIndexIncr

	// Expect a ","
	expect(",", &tokens, curTokenIndex, fmt.Sprintf("Expected ',' but found \"%s\" in formula: \"%s\"", tokens[curTokenIndex], strings.Join(tokens, "")))

	// Go to the next token
	goToNextToken(&tokens, &curTokenIndex)

	// Now, there must be the window length (an integer value)
	winLength, err := strconv.Atoi(tokens[curTokenIndex])
	if err != nil {
		syntaxError(fmt.Sprintf("Expected an integer value for window length but found \"%s\" in parse formula: \"%s\"", tokens[curTokenIndex], strings.Join(tokens, "")))
	}

	// Go to the next token
	goToNextToken(&tokens, &curTokenIndex)

	// Expect a ","
	expect(",", &tokens, curTokenIndex, fmt.Sprintf("Expected ',' after window length but found \"%s\" in formula: \"%s\"", tokens[curTokenIndex], strings.Join(tokens, "")))

	// Go to the next token
	goToNextToken(&tokens, &curTokenIndex)

	// Now, there must be a step size (an integer value)
	stepSize, err := strconv.Atoi(tokens[curTokenIndex])
	if err != nil {
		syntaxError(fmt.Sprintf("Expected an integer value for step size but found \"%s\" in parse formula: \"%s\"", tokens[curTokenIndex], strings.Join(tokens, "")))
	}

	// Go to the next token
	goToNextToken(&tokens, &curTokenIndex)

	// Expect a ")"
	expect(")", &tokens, curTokenIndex, fmt.Sprintf("Expected ')' after step size but found \"%s\" in formula: \"%s\"", tokens[curTokenIndex], strings.Join(tokens, "")))

	return predicate, variables, winLength, stepSize, curTokenIndex + 1
}

// Takes a list of tokens and parses the negation formula. Negation in the current version only supports
// formulae of type not(p) without any variables. Returns the predicate that is negated along with the
// index to the token that immediately follows the formulae
func parseNegation(tokens []string) (types.Predicate, int) {
	// We need at least 3 tokens to be able to parse the negate formula
	if len(tokens) < 3 {
		msg := fmt.Sprintf("No enough tokens to parse negation formula: \"%s\"", strings.Join(tokens, ""))
		panic(msg)
	}

	// The NOT operator is not included in the token list, so, the first token must be an open parenthesis
	if tokens[0] != "(" {
		syntaxError(fmt.Sprintf("Expected '(' but found \"%s\" in formula: \"%s\"", tokens[0], strings.Join(tokens, "")))
	}

	curTokenIndex := 0

	// Go to the next token
	goToNextToken(&tokens, &curTokenIndex)

	// Current token must be a predicate name. Anything else is unexpected
	unexpect("(", &tokens, curTokenIndex, fmt.Sprintf("Unexpected '(' in formula: \"%s\"", strings.Join(tokens, "")))
	unexpect(")", &tokens, curTokenIndex, fmt.Sprintf("Unexpected '(' in formula: \"%s\"", strings.Join(tokens, "")))
	unexpect(",", &tokens, curTokenIndex, fmt.Sprintf("Unexpected '(' in formula: \"%s\"", strings.Join(tokens, "")))

	// Keep the current token as the predicate
	predicate := types.Predicate(tokens[curTokenIndex])

	// Go to the next token
	goToNextToken(&tokens, &curTokenIndex)

	// Expect a ")"
	expect(")", &tokens, curTokenIndex, fmt.Sprintf("Expected ')' but found \"%s\" in formula: \"%s\"", tokens[curTokenIndex], strings.Join(tokens, "")))

	// Go to the next token
	goToNextToken(&tokens, &curTokenIndex)

	return predicate, curTokenIndex + 1
}

// Takes a list of tokens and parses the first formula that matches the tokens. Returns the formula and the
// index of tokens that immediately follows the formula.
func parseFormula(tokens []string) (formula.Formula, int) {
	// No token? Throw a syntax error
	if len(tokens) == 0 {
		msg := fmt.Sprintf("No token found in the atom \"%s\"", strings.Join(tokens, ""))
		panic(msg)
	}

	// Is this a window operator?
	if isWindowOperator(tokens[0]) {
		// Skip the first token (which indicates the type of time-window operator) and retrieve the predicate, variables,
		// window length, step size, and the token after the window operator.
		predicate, vars, winLen, _, indexIncr := parseTimeWinOperator(tokens[1:])
		// Using the first token, determine what time-window operator it is.
		switch windowOperator := tokens[0]; windowOperator {
		case TimeWindowDiamondOperator:
			return timewindim.Create(predicate, vars, winLen), indexIncr + 1 // +1 because the first token is operator name
		case TimeWindowBoxOperator:
			return timewinbox.Create(predicate, vars, winLen), indexIncr + 1 // +1 because the first token is operator name
		}
	}

	// Is this a negation formula?
	if tokens[0] == negationOperator {
		// Skip the first token (which is the negation operator) and parse the rest
		predicate, indexIncr := parseNegation(tokens[1:])
		return not.Create(predicate), indexIncr + 1 // +1 because the first token is operator name
	}

	// If the tokens do not constitute a window operator, they most probably constitute a AtomFormula. So, try to parse an
	// atom formula
	predicate, vars, indexIncr := parseAtom(tokens)
	return atom.Create(predicate, vars), indexIncr
}

// Takes a list of rule's body tokens and returns a ConjunctFormulae
func parseRuleBody(tokens []string) *conjunctformulae.ConjFormulae {
	formulae := []formula.Formula{}

	// To parse the body of the rule, we must parse one formula after another in the following loop
	for curIndex := 0; ; {
		// Parse the next formula
		f, i := parseFormula(tokens[curIndex:])

		// Update the index of current token in the list of tokens
		curIndex += i

		// Add the formula to the list of already parsed formulae
		formulae = append(formulae, f)

		// Reached the end of the rule? Return!
		if curIndex >= len(tokens) {
			break
		}

		// If there are additional tokens in the rule, after each formula there must be a ","
		expect(",", &tokens, curIndex, fmt.Sprintf("Expected \",\" after formula but found \"%s\" in the rule\"%s\"!\n", tokens[curIndex], strings.Join(tokens, "")))

		curIndex++ // Skip the ","
	}

	// Create the conjunctFormulae to be returned by the function
	conjunctFormulae := conjunctformulae.CreateConjFormulae(&formulae, types.NoTimePoint, types.NoTupleCounter)

	return conjunctFormulae
}

// Takes a list of rule's head tokens and returns a formula
func parseRuleHead(tokens []string) formula.Formula {
	f, _ := parseFormula(tokens)
	return f
}

// Parse takes a rule's string and parses it. Returns the Rule struct
func Parse(rule string) *Rule {
	// Tokenize head and body of the rule
	headTokens, bodyTokens := tokenizer(rule)
	// parse rule's head
	headFormula := parseRuleHead(headTokens)
	// parse rule's body
	bodyConjunctFormulae := parseRuleBody(bodyTokens)
	// Create and return the Rule struct
	return New([]formula.Formula{headFormula}, &[]conjunctformulae.ConjFormulae{*bodyConjunctFormulae}, rule)
}

package rule

import (
	"fmt"
	"sync"

	"bitbucket.org/hrbazoo/laser/data"
	"bitbucket.org/hrbazoo/laser/stats"

	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/conjunctformulae"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/join"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of Rule in Laser
 */

// Rule includes the information of a Laser rule
type Rule struct {
	// The head(s) of the formula. In case multiple rules are reconciled into a single one (because their
	// head predicates are the same), we keep separate copies of the heads as their variable names might be
	// different, e.g., h(X,Y,Y) vs h(XX,Z,YY). The head in each position of the slice belongs to the body in
	// the same position in the slice of bodies.
	Head []formula.Formula
	// A set of joined formula that are in disjunction relation with each other. Normally, there is only
	// one head and a body that contains a number of formula in conjunction relation. However, when more
	// than one set of joined formulae conclude the same head formula, we consolidate all of those in a single rule
	// with the joined formulae in disjunction relation with each other
	Body []conjunctformulae.ConjFormulae
	// Shows whether the formula has been evaluated at this time point or not
	EvaluatedAtCurrentTimePoint bool
	// The unparsed string format of the rule
	StringFormat string
	// Shows for how long the rule will not hold
	InvalidUntilTimePoint types.TimePoint
}

// New creates a new rule
func New(head []formula.Formula, body *[]conjunctformulae.ConjFormulae, stringFormat string) *Rule {
	return &Rule{
		Head:                        head,
		Body:                        *body,
		EvaluatedAtCurrentTimePoint: false,
		StringFormat:                stringFormat,
		InvalidUntilTimePoint:       types.NoTimePoint,
	}
}

// Evaluate evaluates the rule formulae and return true if at least one of the
// rule formulae holds and false otherwise
func (rule *Rule) Evaluate() bool {
	now := clock.GetNow()
	// The results of each disjunctted part of the rule will be stored in this variable
	results := make([]*substitutiontable.SubstitutionTable, len(rule.Body))

	// Execute the join on each conjunctted part of the rule
	for i := 0; i < len(rule.Body); i++ {
		// Get current conjunct formula
		conjunctFormulae := &rule.Body[i]
		// If the whole conjunct formula is guaranteed to be invalid at this time point,
		// no need to evaluate each individual formula inside it
		// NOTE: We can only perform this optimization if the program has been configured to use the invalidation
		// prediction algorithm. Furthermore, we can only skip further evaluation if this is the first time this rule
		// is evaluated at this time point. If the rule has already been evaluated at the current time point, we must
		// keep re-evaluating it because it might have temporarily been flagged as false but the fix-point evaluation
		// of rules might change the evaluation result
		if config.UseInvalidationPrediction && conjunctFormulae.InvalidUntilTimePoint >= now && !rule.EvaluatedAtCurrentTimePoint {
			// Increment the counter that keeps the total number of skipped formulae
			incrementTotalNumberOfSkippedFormulae()
			continue
		}
		// If we cannot rule out the possibility of the whole conjunct formula to be true,
		// we must evaluate each individual formula in it
		conjunctFormulaeHolds := true
		// For each formula in the conjunct-formula, evaluate it
		for _, f := range conjunctFormulae.Formulae {
			// Update the stats about the total number of evaluated formulae
			incrementTotalNumberOfFormulaEvaluations()
			// Evaluate the formula
			if !f.Evaluate() {
				// If the program is configured to use invalidation prediction algorithm, when a formula fails, we need to
				// iterate over invalidation tokens to find out for how long the formula does not hold. We can leverage
				// this information to avoid some of the subsequent evaluations of the conjunct-formula because if one formula
				// in the conjunct-formula fails, the whole conjunct-formula would fail.
				if config.UseInvalidationPrediction {
					invalidUntil := types.NoTimePoint // Determines the longest period in which the conjunctFormula does not hold
					// Loop over the invalidation tokens to find out the longest duration in which the conjunctFormula does not hold
					for it := range f.GetEvaluationResults().GetInvalidationTokens() {
						if -it.ConsiderationTime <= now && it.HorizonTime >= invalidUntil {
							invalidUntil = it.HorizonTime
						}
					}
					conjunctFormulae.InvalidUntilTimePoint = invalidUntil // Set the invalidity duration of conjunct formula
				}
				// Set the flag to show that the conjunct formula does not hold
				conjunctFormulaeHolds = false
				// If one formula fails, the whole conjunct formula fails. So, no need to continue. Break out of the loop
				break
			}
		}
		// Rule has been evaluated. So, set the flag accordingly
		rule.EvaluatedAtCurrentTimePoint = true
		// If all formulae that are in conjunction relation with each other hold, we must
		// perform the join operation between them
		if conjunctFormulaeHolds {
			results[i] = join.Join(conjunctFormulae)
		}

		if results[i] != nil && results[i].Size() > 0 {
			// Insert the evaluation results in the head formula
			headVariableToPositionMap := rule.Head[i].GetEvaluationResults().VariableToRecordIndex
			joinResultVariableToPositionMap := results[i].VariableToRecordIndex
			for record := range results[i].Records {
				headSubstitutions := make([]types.Substitution, rule.Head[i].GetEvaluationResults().NumberOfVariables)
				for v, headIndexes := range headVariableToPositionMap {
					for _, hi := range headIndexes {
						resultIndexes, exists := joinResultVariableToPositionMap[v]
						if !exists {
							msg := fmt.Sprintf("Variable %s exists in the head but not in the join results\n", v)
							panic(msg)
						}
						headSubstitutions[hi] = record.Substitutions[resultIndexes[0]]
					}
				}
				headRecord := substitutiontable.CreateSubstitutionRecord(&headSubstitutions, record.ConsiderationTime, record.HorizonTime)
				rule.Head[i].Insert(headRecord)
			}
		} else {
			// The join did not yield any results. So, there are a few possibilities here:
			// First, it could be because the join really failed. If that is the case, we need to create an invalidation token
			// and insert it in the substitution table of the head.
			// Second, it could be because the there was no new input records in the stream to join at this time point, but that
			// does not imply the invalidity of the body.
			// How do we discern these possibilities from one another? We need to check the type of the head formula. If the head formula
			// is an atom, if there is any substitution record in the head, it means that the whole rule still holds.
			if config.UseInvalidationPrediction {
				// If the head of the rule is an atom formula, check if there is any substitution record in it's resultSubstitutionTable
				if rule.Head[i].GetFormulaType() == formula.AtomFormulaTypeName {
					if conjunctFormulae.InvalidUntilTimePoint == types.NoTimePoint && rule.Head[i].GetEvaluationResults().CountSubstitutionRecords() == 0 {
						// The following condition holds only when the rule fails as a result of join failures
						conjunctFormulae.InvalidUntilTimePoint = now
						headInvalidationToken := substitutiontable.CreateInvalidationToken(now, conjunctFormulae.InvalidUntilTimePoint)
						rule.Head[i].InsertResult(headInvalidationToken)
					} else if conjunctFormulae.InvalidUntilTimePoint != types.NoTimePoint && rule.Head[i].GetEvaluationResults().CountSubstitutionRecords() == 0 {
						// This happens when the body of the rule fails
						headInvalidationToken := substitutiontable.CreateInvalidationToken(now, conjunctFormulae.InvalidUntilTimePoint)
						rule.Head[i].InsertResult(headInvalidationToken)
					} else if conjunctFormulae.InvalidUntilTimePoint != types.NoTimePoint && rule.Head[i].GetEvaluationResults().CountSubstitutionRecords() != 0 {
						// fmt.Println("---------------------------------")
						// fmt.Println(rule.StringFormat)
						// fmt.Println("Conjuct invalid untile ", conjunctFormulae.InvalidUntilTimePoint, "Head substitutions ", rule.Head[i].GetEvaluationResults().Records)
						// for item := range rule.Head[i].GetEvaluationResults().Records {
						// 	fmt.Println(">>>>>>>>>>>>> ", item)
						// }
						// // It must never happen that the body of the rule does not hold but the head still has valid substitution records.
						// panic("If a rule is invalid, its head atom formula must not contain any substitution records")
					}
				} else {
					panic("Head formulae other simple AtomFormula are not yet supported")
				}
			}
		}
	}
	// If one of the head substitution tables has one or more substitution records and there is
	// no invalidation tokens whose consideration time is littler or equal to the current time point,
	// the rule holds.
	for i := 0; i < len(rule.Head); i++ {
		var holds bool
		// If the program is configured to use invalidation prediction algorithm, we can leverage the existence of
		// invalidation tokens to more efficiently find out whether the formula holds or not.
		if config.UseInvalidationPrediction {
			holds = rule.Head[i].GetEvaluationResults().CountSubstitutionRecords() > 0
			for it := range rule.Head[i].GetEvaluationResults().InvalidationTokens {
				// Consideration time of the token is littler or equal to now? The rule does not hold then
				if -it.ConsiderationTime <= now {
					rule.InvalidUntilTimePoint = it.HorizonTime // The rule would be certainly invalid until this time point
					holds = false
					break
				}
			}
		} else {
			// If the program is configured to not use invalidation predication algorithm, we need to iterate
			// over existing substitution records to see if any of them holds at the moment. If so, we know
			// that the rule holds
			for r := range rule.Head[i].GetEvaluationResults().Records {
				if r.ConsiderationTime <= now {
					holds = true
					break
				}
			}
		}
		if holds {
			return true
		}
	}
	return false
}

// Cleanup removes the expired records from the substitution tables of all
// formulae in the rule
func (rule *Rule) Cleanup() {
	now := clock.GetNow()
	// First, iterate over the disjointed formulae in the body of the rule
	for i, conjuncttedFormulae := range (*rule).Body {
		// Then, interate over formulae joined together
		for _, f := range conjuncttedFormulae.Formulae {
			f.Cleanup()
		}
		// Delete expired records in the head formula
		rule.Head[i].Cleanup()
	}
	// Reset the flag that indicates whether the formula is evaluated at the current time point or not
	rule.EvaluatedAtCurrentTimePoint = false
	// Clear up the invalidity interval of the rule (if expired)
	if rule.InvalidUntilTimePoint != types.NoTimePoint && rule.InvalidUntilTimePoint < now {
		rule.InvalidUntilTimePoint = types.NoTimePoint
	}
}

// CountRecentConcludedSubstitutionRecords returns the number of substitution records
// that are concluded at the present time point
func (rule *Rule) CountRecentConcludedSubstitutionRecords() int {
	nRecords := 0

	// We only count recent concluded substitution records (not invalidation tokens)
	for _, head := range rule.Head {
		nRecords += head.GetEvaluationResults().CountRecentSubstitutionRecords()
	}

	return nRecords
}

// CountRecentConclusions returns the total number of substitution records and invalidation tokens
// concluded at this time point
func (rule *Rule) CountRecentConclusions() int {
	n := 0

	// We only count recent concluded substitution records as well as recent invalidation tokens if the program
	// is configured to use invalidation tokens
	if config.UseInvalidationPrediction {
		for _, head := range rule.Head {
			n += head.GetEvaluationResults().CountRecentSubstitutionRecords() + head.GetEvaluationResults().CountRecentInvalidationTokens()
		}
	} else {
		// If the program is not configured to use invalidation prediction algorithm, return only the number of recent
		// substitution records
		for _, head := range rule.Head {
			n += head.GetEvaluationResults().CountRecentSubstitutionRecords()

		}
	}

	return n
}

// CollectRecentConcludedAtoms returns the recent conclusions of the stratum in form of AtomBatches
func (rule *Rule) CollectRecentConcludedAtoms() *[]*data.AtomBatch {
	atomBatches := []*data.AtomBatch{}

	// We only create and return an atom batch if there is at least one record
	// in the head's evaluation results substitution table
	for _, head := range rule.Head {
		if head.GetEvaluationResults().CountRecentSubstitutionRecords() > 0 {
			atomBatches = append(atomBatches, &data.AtomBatch{
				Predicate:       head.GetPredicate(),
				ConstantRecords: &head.GetEvaluationResults().RecentSubstitutionRecords,
			})
		} else {
			// If the rule has no recent substitution records, it does not necessarily mean that it does not hold.
			// We must first check if head of the rule has any substitution records or not.
			// If the program is configured to use invalidation tokens, return invalidation tokens
			// as conclusions of the rule
			if config.UseInvalidationPrediction {
				if head.GetEvaluationResults().CountInvalidationTokens() == 0 && head.GetEvaluationResults().CountSubstitutionRecords() == 0 {
					panic("Rule \"" + rule.StringFormat + "\" concluded neither substitution records nor invalidation tokens")
				}
				// If the rule recently concluded new invalidation tokens, return them
				if head.GetEvaluationResults().CountRecentInvalidationTokens() > 0 {
					atomBatches = append(atomBatches, &data.AtomBatch{
						Predicate:       head.GetPredicate(),
						ConstantRecords: &head.GetEvaluationResults().RecentInvalidationTokens,
					})
				}
			}
		}
	}

	return &atomBatches
}

// CollectRecentAndNonRecentConcludedAtoms collects all concluded atoms, including recent and non-recent atoms
func (rule *Rule) CollectRecentAndNonRecentConcludedAtoms() *[]*data.AtomBatch {
	atomBatches := []*data.AtomBatch{}

	// We only create and return an atom batch if there is at least one record
	// in the head's evaluation results substitution table
	for _, head := range rule.Head {
		if head.GetEvaluationResults().CountSubstitutionRecords() > 0 {
			atomBatches = append(atomBatches, &data.AtomBatch{
				Predicate:       head.GetPredicate(),
				ConstantRecords: &head.GetEvaluationResults().Records,
			})
		} else {
			if config.UseInvalidationPrediction {
				// If the rule recently concluded new invalidation tokens, return them
				if head.GetEvaluationResults().CountInvalidationTokens() > 0 {
					atomBatches = append(atomBatches, &data.AtomBatch{
						Predicate:       head.GetPredicate(),
						ConstantRecords: &head.GetEvaluationResults().InvalidationTokens,
					})
				}
			}
		}
	}

	return &atomBatches

}

// GetHeadPredicate returns the head's predicate
func (rule *Rule) GetHeadPredicate() types.Predicate {
	return rule.Head[0].GetPredicate()
}

//Equals takes another rule and compares the string format of the two rules together.
// Returns true of the two string formats match and false otherwise.
func (rule *Rule) Equals(other *Rule) bool {
	return rule.StringFormat == other.StringFormat
}

// Implements a thread-safe mechanism to update the stats about the total number of evaluated formulae
func incrementTotalNumberOfFormulaEvaluations() {
	m := &sync.Mutex{}
	m.Lock()
	stats.ExecutionProfile.TotalNumberOfFormulaEvaluations++
	m.Unlock()
}

func incrementTotalNumberOfSkippedFormulae() {
	m := &sync.Mutex{}
	m.Lock()
	stats.ExecutionProfile.TotalNumberOfSkippedFormulaExecutions++
	m.Unlock()
}

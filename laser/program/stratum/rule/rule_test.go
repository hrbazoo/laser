package rule

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/data/substitutiontable"

	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/conjunctformulae"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/atom"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula/timewindim"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the tests for the package rule
 */

func TestRuleCreation1(t *testing.T) {
	cassert := cassert.New(t)

	var head formula.Formula
	var body []conjunctformulae.ConjFormulae

	head = timewindim.Create(types.Predicate("h"), make([]types.Variable, 0), 1)
	body = []conjunctformulae.ConjFormulae{*conjunctformulae.CreateConjFormulae(&[]formula.Formula{timewindim.Create(types.Predicate("p"), make([]types.Variable, 0), 1)}, types.UnknownTimePoint, types.UnknownTupleCounter)}

	New([]formula.Formula{head}, &body, "")

	cassert.Equal(head.GetPredicate(), types.Predicate("h"))
	cassert.Equal(len(body), 1)
	cassert.Equal(len(body[0].Formulae), 1)
	cassert.Equal(body[0].Formulae[0].GetPredicate(), types.Predicate("p"))
}

func TestRuleEvaluation1(t *testing.T) {
	cassert := cassert.New(t)
	clock.Reset()

	var head formula.Formula
	var body []conjunctformulae.ConjFormulae

	head = atom.Create(types.Predicate("h"), make([]types.Variable, 0))
	body = []conjunctformulae.ConjFormulae{*conjunctformulae.CreateConjFormulae(&[]formula.Formula{timewindim.Create(types.Predicate("p"), make([]types.Variable, 0), 1)}, types.UnknownTimePoint, types.UnknownTupleCounter)}

	r := New([]formula.Formula{head}, &body, "")
	substitutions := make([]types.Substitution, 0) // There is no substitution
	record := substitutiontable.CreateSubstitutionRecord(&substitutions, types.TimePoint(types.StartTimePoint), types.TimePoint(types.StartTimePoint))
	body[0].Formulae[0].Insert(record)

	cassert.Equal(r.Evaluate(), true)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 1)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 1)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 0)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), true)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 1)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 0)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), false)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 0)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 1)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), false)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 0)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 1)
}

func TestRuleEvaluation2(t *testing.T) {
	cassert := cassert.New(t)
	clock.Reset()

	var head formula.Formula
	var body []conjunctformulae.ConjFormulae

	head = atom.Create(types.Predicate("h"), []types.Variable{"A", "B"})
	body = []conjunctformulae.ConjFormulae{
		*conjunctformulae.CreateConjFormulae(
			&[]formula.Formula{timewindim.Create(types.Predicate("p"), []types.Variable{types.Variable("A"), types.Variable("B")}, 1)},
			types.UnknownTimePoint,
			types.UnknownTupleCounter,
		),
	}

	r := New([]formula.Formula{head}, &body, "")
	substitutions := []types.Substitution{"a", "b"} // There is no substitution
	record := substitutiontable.CreateSubstitutionRecord(&substitutions, types.TimePoint(types.StartTimePoint), types.TimePoint(types.StartTimePoint))
	body[0].Formulae[0].Insert(record)

	cassert.Equal(r.Evaluate(), true)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 1)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 1)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 0)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), true)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 1)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 0)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), false)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 0)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 1)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), false)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 0)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 1)
}

func TestRuleEvaluation3(t *testing.T) {
	cassert := cassert.New(t)
	clock.Reset()

	var head formula.Formula
	var body []conjunctformulae.ConjFormulae

	head = atom.Create(types.Predicate("h"), []types.Variable{"A", "B"})
	body = []conjunctformulae.ConjFormulae{
		*conjunctformulae.CreateConjFormulae(
			&[]formula.Formula{
				timewindim.Create(types.Predicate("p"), []types.Variable{types.Variable("A"), types.Variable("B")}, 1),
				timewindim.Create(types.Predicate("q"), []types.Variable{types.Variable("B"), types.Variable("C")}, 2),
			},
			types.UnknownTimePoint,
			types.UnknownTupleCounter,
		),
	}

	r := New([]formula.Formula{head}, &body, "")

	cassert.Equal(r.GetHeadPredicate(), types.Predicate("h"))
	substitutions := []types.Substitution{"a", "b"}
	record := substitutiontable.CreateSubstitutionRecord(&substitutions, types.TimePoint(types.StartTimePoint), types.TimePoint(types.StartTimePoint))
	body[0].Formulae[0].Insert(record)

	substitutions = []types.Substitution{"b", "c"}
	record = substitutiontable.CreateSubstitutionRecord(&substitutions, types.TimePoint(types.StartTimePoint), types.TimePoint(types.StartTimePoint))
	body[0].Formulae[1].Insert(record)

	cassert.Equal(r.Evaluate(), true)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 1)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 1)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 0)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), true)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 1)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 0)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), false)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 0)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 1)

	r.Cleanup()
	clock.GoToNextTimePoint()

	cassert.Equal(r.Evaluate(), false)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentSubstitutionRecords), 0)
	cassert.Equal(len(r.Body[0].Formulae[0].GetEvaluationResults().Records), 0)
	cassert.CEqual(len(r.Body[0].Formulae[0].GetEvaluationResults().RecentInvalidationTokens), 1)
}

func TestRuleEquals1(t *testing.T) {
	cassert := cassert.New(t)

	r1 := Parse("h(X,Y) := p(X,Y),q(Y,X)")
	r2 := Parse("h(X,Y) := p(X,Y),q(Y,X)")

	cassert.Equal(r1.GetHeadPredicate(), types.Predicate("h"))
	cassert.Equal(r1.Equals(r2), true)
}

func TestRuleEquals2(t *testing.T) {
	cassert := cassert.New(t)

	r1 := Parse("h(X,Y) := p(X,Y),q(Y,X)")
	r2 := Parse("h(X,Y) := p(X,Y),r(Y,X)")

	cassert.Equal(r1.GetHeadPredicate(), types.Predicate("h"))
	cassert.Equal(r1.Equals(r2), false)
}

func TestRuleEvaluation4(t *testing.T) {
	cassert := cassert.New(t)
	clock.Reset()

	r := Parse("h(X,Y) := timewinbox(p(X,Y), 3, 1), timewindim(q(Y,X), 5, 1)")

	a1 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a", "b"}, clock.GetNow(), clock.GetNow())
	a2 := substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b", "a"}, clock.GetNow(), clock.GetNow())

	r.Body[0].Formulae[0].Insert(a1)
	r.Body[0].Formulae[1].Insert(a2)

	// now = 1
	holds := r.Evaluate()
	cassert.Equal(holds, true)

	r.Cleanup()
	clock.GoToNextTimePoint()

	// now = 2
	holds = r.Evaluate()
	cassert.Equal(holds, false)

	r.Cleanup()
	clock.GoToNextTimePoint()

	// now = 3
	a1 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a", "b"}, clock.GetNow(), clock.GetNow())
	a2 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b", "a"}, clock.GetNow(), clock.GetNow())
	r.Body[0].Formulae[0].Insert(a1)
	r.Body[0].Formulae[1].Insert(a2)
	holds = r.Evaluate()
	cassert.Equal(holds, false)

	r.Cleanup()
	clock.GoToNextTimePoint()

	// now = 4
	a1 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a", "b"}, clock.GetNow(), clock.GetNow())
	a2 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b", "a"}, clock.GetNow(), clock.GetNow())
	r.Body[0].Formulae[0].Insert(a1)
	r.Body[0].Formulae[1].Insert(a2)
	holds = r.Evaluate()
	cassert.Equal(holds, false)

	r.Cleanup()
	clock.GoToNextTimePoint()

	// now = 5
	a1 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a", "b"}, clock.GetNow(), clock.GetNow())
	a2 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b", "a"}, clock.GetNow(), clock.GetNow())
	r.Body[0].Formulae[0].Insert(a1)
	r.Body[0].Formulae[1].Insert(a2)
	holds = r.Evaluate()
	cassert.Equal(holds, false)

	r.Cleanup()
	clock.GoToNextTimePoint()

	// now = 6
	a1 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"a", "b"}, clock.GetNow(), clock.GetNow())
	a2 = substitutiontable.CreateSubstitutionRecord(&[]types.Substitution{"b", "a"}, clock.GetNow(), clock.GetNow())
	r.Body[0].Formulae[0].Insert(a1)
	r.Body[0].Formulae[1].Insert(a2)
	holds = r.Evaluate()
	cassert.Equal(holds, true)
	cassert.Equal(r.Head[0].GetEvaluationResults().CountSubstitutionRecords(), 1)

	r.Cleanup()
	clock.GoToNextTimePoint()

	// now = 7
	holds = r.Evaluate()
	cassert.Equal(holds, false)
	cassert.Equal(r.Head[0].GetEvaluationResults().CountSubstitutionRecords(), 0)
}

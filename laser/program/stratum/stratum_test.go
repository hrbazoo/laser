package stratum

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/data"
	"bitbucket.org/hrbazoo/laser/data/types"

	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit tests of stratum package
 */

// TestStratumNum1 tests whether addition of rule in the stratum works
// as expected
func TestStratumNum1(t *testing.T) {
	assert := cassert.New(t)

	r1 := rule.Parse("h1(X,Y) := p(X,Y),q(Y,X)")
	r2 := rule.Parse("h2(X,Y) := p(X,Y),r(Y,X)")

	s := New([]*rule.Rule{r1, r2})

	assert.Equal(len(s.Rules), 2)

	assert.Equal(s.Rules[0].Equals(r1), true)
	assert.Equal(s.Rules[1].Equals(r2), true)

	assert.Equal(s.Rules[0].Equals(r2), false)
	assert.Equal(s.Rules[1].Equals(r1), false)

	assert.Equal(len(s.PredicateToFormula), 3)

	pFormulae, pExists := s.PredicateToFormula["p"]
	qFormulae, qExists := s.PredicateToFormula["q"]
	rFormulae, rExists := s.PredicateToFormula["r"]

	_, hExists := s.PredicateToFormula["h"]

	assert.Equal(pExists, true)
	assert.Equal(qExists, true)
	assert.Equal(rExists, true)
	assert.Equal(hExists, false)

	assert.Equal(len(*pFormulae), 2)
	assert.Equal(len(*qFormulae), 1)
	assert.Equal(len(*rFormulae), 1)

	assert.Equal(*pFormulae, []*formula.Formula{&r2.Body[0].Formulae[0], &r1.Body[0].Formulae[0]})
	assert.Equal(*qFormulae, []*formula.Formula{&r1.Body[0].Formulae[1]})
	assert.Equal(*rFormulae, []*formula.Formula{&r2.Body[0].Formulae[1]})
}

// TestStratumNum2 tests whether addition of rule in the stratum works
// as expected
func TestStratumNum2(t *testing.T) {
	assert := cassert.New(t)

	r1 := rule.Parse("h1(X,Y) := p(X,Y),q(Y,X)")
	r2 := rule.Parse("h2(X,Y) := p(X,Y),r(Y,X)")

	s := New([]*rule.Rule{r1, r2})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	s.InsertMultipleAtomBatches(&atomBatch)

	evalResult := s.Evaluate()

	assert.Equal(evalResult, true)

	conclusions := s.CollectRecentConcludedAtoms()

	assert.Equal(len(*conclusions), 1)
	assert.Equal(len(*(*(*conclusions)[0]).ConstantRecords), 1)
	assert.Equal((*(*conclusions)[0]).Predicate, types.Predicate("h1"))
	for constants := range *(*(*conclusions)[0]).ConstantRecords {
		assert.Equal(constants.Substitutions, []types.Substitution{"a", "b"})
		assert.Equal(constants.ConsiderationTime, types.StartTimePoint)
		assert.Equal(constants.HorizonTime, types.StartTimePoint)
	}

}

// TestStratumNum3 tests whether concluded atoms are used to infer additional
// conclusions are not
func TestStratumNum3(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	r1 := rule.Parse("h1(X,Y) := p(X,Y),q(Y,X)")
	r2 := rule.Parse("h2(X,Y) := q(X,Y),h1(Y,X)")

	s := New([]*rule.Rule{r1, r2})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	s.InsertMultipleAtomBatches(&atomBatch)

	evalResult := s.Evaluate()

	assert.Equal(evalResult, true)
	assert.Equal(s.InvalidUntilTimePoint, types.NoTimePoint)

	conclusions := s.CollectRecentConcludedAtoms()

	assert.Equal(len(*conclusions), 2)
	assert.Equal(len(*(*(*conclusions)[0]).ConstantRecords), 1)
	assert.Equal((*(*conclusions)[0]).Predicate, types.Predicate("h1"))
	for constants := range *(*(*conclusions)[0]).ConstantRecords {
		assert.Equal(constants.Substitutions, []types.Substitution{"a", "b"})
		assert.Equal(constants.ConsiderationTime, types.StartTimePoint)
		assert.Equal(constants.HorizonTime, types.StartTimePoint)
	}
	assert.Equal(len(*(*(*conclusions)[1]).ConstantRecords), 1)
	assert.Equal((*(*conclusions)[1]).Predicate, types.Predicate("h2"))
	for constants := range *(*(*conclusions)[1]).ConstantRecords {
		assert.Equal(constants.Substitutions, []types.Substitution{"b", "a"})
		assert.Equal(constants.ConsiderationTime, types.StartTimePoint)
		assert.Equal(constants.HorizonTime, types.StartTimePoint)
	}

	s.Cleanup()
	clock.GoToNextTimePoint()

	evalResult = s.Evaluate()

	assert.Equal(evalResult, false)
	assert.CEqual(s.InvalidUntilTimePoint, types.TimePoint(clock.GetNow()))

	s.Cleanup()
	clock.GoToNextTimePoint()

	evalResult = s.Evaluate()

	assert.Equal(evalResult, false)
	assert.CEqual(s.InvalidUntilTimePoint, types.TimePoint(clock.GetNow()))

	s.Cleanup()
	clock.GoToNextTimePoint()

	evalResult = s.Evaluate()

	assert.Equal(evalResult, false)
	assert.CEqual(s.InvalidUntilTimePoint, types.TimePoint(clock.GetNow()))

	s.Cleanup()
	clock.GoToNextTimePoint()

	evalResult = s.Evaluate()

	assert.Equal(evalResult, false)
	assert.CEqual(s.InvalidUntilTimePoint, types.TimePoint(clock.GetNow()))

	s.Cleanup()
	clock.GoToNextTimePoint()

	evalResult = s.Evaluate()

	assert.Equal(evalResult, false)
	assert.CEqual(s.InvalidUntilTimePoint, types.TimePoint(clock.GetNow()))
}

// TestStratumNum4 tests whether the invalidity period of the stratum is calculated correctly
func TestStratumNum4(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	r1 := rule.Parse("h1(X,Y) := timewindim(p(X,Y), 20, 1),timewinbox(q(Y,X), 30, 1)")
	r2 := rule.Parse("h2(X,Y) := p(X,Y),h1(Y,X)")

	s := New([]*rule.Rule{r1, r2})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	s.InsertMultipleAtomBatches(&atomBatch)

	evalResult := s.Evaluate()

	assert.Equal(evalResult, true)
	assert.Equal(s.InvalidUntilTimePoint, types.NoTimePoint)

	conclusions := s.CollectRecentConcludedAtoms()

	assert.Equal(len(*conclusions), 1)
	assert.Equal(len(*(*(*conclusions)[0]).ConstantRecords), 1)
	assert.Equal((*(*conclusions)[0]).Predicate, types.Predicate("h1"))

	s.Cleanup()
	clock.GoToNextTimePoint()

	atomBatch = data.NewAtomBatch(&[]string{
		"p(a,b)",
	})
	s.InsertMultipleAtomBatches(&atomBatch)

	evalResult = s.Evaluate()

	assert.Equal(evalResult, false)
	assert.CEqual(s.InvalidUntilTimePoint, types.TimePoint(32))
}

func TestStratumNum5(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	r1 := rule.Parse("h1(X,Y) := timewindim(p(X,Y), 20, 1),timewinbox(q(Y,X), 30, 1)")
	r2 := rule.Parse("h2(X,Y) := q(X,Y),h1(Y,X)")

	s := New([]*rule.Rule{r1, r2})

	atomBatch := data.NewAtomBatch(&[]string{
		"p(a,b)",
		"q(b,a)",
	})

	s.InsertMultipleAtomBatches(&atomBatch)

	evalResult := s.Evaluate()

	assert.Equal(evalResult, true)
	assert.Equal(s.InvalidUntilTimePoint, types.NoTimePoint)

	conclusions := s.CollectRecentConcludedAtoms()

	assert.Equal(len(*conclusions), 2)
	assert.Equal(len(*(*(*conclusions)[0]).ConstantRecords), 1)
	assert.Equal((*(*conclusions)[0]).Predicate, types.Predicate("h1"))
	for constants := range *(*(*conclusions)[0]).ConstantRecords {
		assert.Equal(constants.Substitutions, []types.Substitution{"a", "b"})
		assert.Equal(constants.ConsiderationTime, types.StartTimePoint)
		assert.Equal(constants.HorizonTime, types.StartTimePoint)
	}
	assert.Equal(len(*(*(*conclusions)[1]).ConstantRecords), 1)
	assert.Equal((*(*conclusions)[1]).Predicate, types.Predicate("h2"))
	for constants := range *(*(*conclusions)[1]).ConstantRecords {
		assert.Equal(constants.Substitutions, []types.Substitution{"b", "a"})
		assert.Equal(constants.ConsiderationTime, types.StartTimePoint)
		assert.Equal(constants.HorizonTime, types.StartTimePoint)
	}

	s.Cleanup()
	clock.GoToNextTimePoint()

	evalResult = s.Evaluate()

	assert.Equal(evalResult, false)
	assert.CEqual(s.InvalidUntilTimePoint, types.TimePoint(2))
}

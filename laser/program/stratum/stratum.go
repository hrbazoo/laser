package stratum

import (
	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule"
	"bitbucket.org/hrbazoo/laser/program/stratum/rule/formula/formula"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of Stratum in Laser
 */

// Stratum includes information about a Laser Stratum
type Stratum struct {
	// Each stratum includes a number of rules
	Rules                 []*rule.Rule                            // Rules inside the stratum
	InvalidUntilTimePoint types.TimePoint                         // Indicates for how long all rules in the stratum are invalid
	PredicateToFormula    map[types.Predicate]*[]*formula.Formula // A map from predicate to a list of formula in stratum's rules
}

// New creates an empty Stratum
func New(rules []*rule.Rule) *Stratum {
	// Create an empty stratum
	s := &Stratum{
		Rules:                 []*rule.Rule{},
		InvalidUntilTimePoint: types.NoTimePoint,
		PredicateToFormula:    map[types.Predicate]*[]*formula.Formula{},
	}
	// One by one add rules to the stratum
	for _, r := range rules {
		s.Add(r)
	}
	// Return the pointer to the stratum
	return s
}

// Add takes a rule and adds it to the list of rules in this stratum. Furthermore, it creates the
// mapping from predicates to formulae in the rule
func (stratum *Stratum) Add(r *rule.Rule) {
	// Add the rule to the list of rules in the Stratum
	stratum.Rules = append(stratum.Rules, r)
	// Iterated over each disjointed part of the rule body
	for _, conjoinedFormula := range r.Body {
		// Iterated over each conjoined part of the rule body (DO NOT use range, because we need pointer of the formula)
		for i := 0; i < len(conjoinedFormula.Formulae); i++ {
			f := conjoinedFormula.Formulae[i]
			// Get the predicate of the formula
			p := f.GetPredicate()
			// Is there already a mapping from this predicate to the list of formulae that use it?
			formulaList, exists := stratum.PredicateToFormula[p]
			if exists { // If so? Add the formula to the list
				*formulaList = append(*formulaList, &f)
			} else { // Otherwise, create a new list
				stratum.PredicateToFormula[p] = &[]*formula.Formula{&f}
			}
		}
	}
}

// InsertMultipleAtomBatches takes a number of atom batches and feeds them to the rules inside the stratum
func (stratum *Stratum) InsertMultipleAtomBatches(atomBatches *[]*data.AtomBatch) {
	for _, atomBatch := range *atomBatches {
		// If at least one formula in the stratum has the same predicate as the atom's, insert substitutions
		// in that formula
		if formulae, exists := stratum.PredicateToFormula[atomBatch.Predicate]; exists {
			for _, f := range *formulae { // Insert the constants in each formula in the list of formulae
				for constantArray := range *atomBatch.ConstantRecords {
					(*f).Insert(constantArray)
				}
			}
		}
	}
}

// InsertSingleAtomBatch takes an atom batch and inserts atoms of the batch into the rules of the stratum
func (stratum *Stratum) InsertSingleAtomBatch(atomBatch *data.AtomBatch) {
	// If at least one formula in the stratum has the same predicate as the atom's, insert substitutions
	// in that formula
	if formulae, exists := stratum.PredicateToFormula[atomBatch.Predicate]; exists {
		for _, f := range *formulae { // Insert the constats in each formula in the list of formulae
			for constantArray := range *atomBatch.ConstantRecords {
				(*f).Insert(constantArray)
			}
		}
	}
}

// Evaluate calculates the fix-point computation of the stratum.
func (stratum *Stratum) Evaluate() bool {
	// If the stratum is known to be invalid at this time point, return false
	if config.UseInvalidationPrediction && clock.GetNow() < stratum.InvalidUntilTimePoint {
		return false
	}
	// Evaluate the stratum
	evalResult := stratum.evaluateInternal()
	// If the program is configured to use the invalidation prediction algorithm, find in what
	// interval the whole stratum is guaranteed to not hold. This time interval is the overlap
	// of the time intervals in which none of the rules holds.
	if !evalResult && config.UseInvalidationPrediction {
		invalidUntil := types.MaxTimePoint
		// Iterate over all rules to find the time interval in which all rules are invalid
		for _, r := range stratum.Rules {
			// If a rule does not hold and the program uses the invalidation prediction algorithm, the
			// invalidity period must be set properly. If a rule does not conclude new substitution records does not
			// necessarily mean that the rule is invalid. It could be that the rule does not have any new
			// substitution records, but some of the past substitution records still hold. So, in those cases
			// at least there must be some substitution records in the head atoms of the rule
			if r.InvalidUntilTimePoint == types.NoTimePoint {
				hasSubstitutionRecords := false // Are there substitution records in the head atoms of the rule?
				for _, h := range r.Head {      // Check every head in the rule
					// Does it have substitution records? If so, set the flag and break out of the loop
					if h.GetEvaluationResults().CountSubstitutionRecords() > 0 {
						hasSubstitutionRecords = true
						break
					}
				}
				// If the rule did not conclude anything new and there are not substitution records in the head atoms of the rule
				// and yet, the invalidity period of the rule is not set, panic! There is a bug in the algorithm
				if !hasSubstitutionRecords {
					panic("The rule \"" + r.StringFormat + "\" does not hold but the invalidity period is not set")
				}
			}
			// Try to see if we can reduce the size of time interval in which the stratum does not hold
			if r.InvalidUntilTimePoint < invalidUntil {
				invalidUntil = r.InvalidUntilTimePoint
			}
		}
		if invalidUntil != types.MaxTimePoint {
			stratum.InvalidUntilTimePoint = invalidUntil
		}
	}

	return evalResult
}
func (stratum *Stratum) evaluateInternal() bool {
	now := clock.GetNow()
	// Evaluate rules in the stratum
	hasNew := false // Shows whether the evaluation yielded new conclusions
	for _, r := range stratum.Rules {
		// If the rule is certainly invalid, ignore it
		if config.UseInvalidationPrediction && r.InvalidUntilTimePoint > now {
			continue
		}
		ruleEvalRecordsBefore := r.CountRecentConclusions()
		evalResult := r.Evaluate()
		ruleEvalRecordsAfter := r.CountRecentConclusions()
		ruleHasNewConclusions := evalResult && (ruleEvalRecordsAfter-ruleEvalRecordsBefore > 0)
		hasNew = (ruleHasNewConclusions || hasNew)
		// Insert recent conclusions into the stratum
		stratum.InsertMultipleAtomBatches(r.CollectRecentConcludedAtoms())
	}

	// If the evaluation yielded no new conclusions, we are done!
	if !hasNew {
		return false
	}

	// Re-evaluate the stratum
	stratum.evaluateInternal()
	return hasNew
}

// HasValidConclusions is a function that checks every rule in the stratum to see if it still
// has valid conclusions (non-expired) conclusions or not. Returns true if the stratum has valid
// conclusions and false otherwise.
func (stratum *Stratum) HasValidConclusions() bool {
	for _, r := range stratum.Rules {
		// If there is one (or more) rule(s) that has no invalidations tokens, that means that
		// the rule certainly holds. So, we can safely return true without risking any wrong results
		for _, head := range (*r).Head {
			// If head has no invalidation token, it means that the rule is valid (we only use this when the system is configured to
			// use the invalidation prediction algorithm)
			if config.UseInvalidationPrediction &&
				head.GetEvaluationResults().CountInvalidationTokens() == 0 &&
				head.GetEvaluationResults().CountSubstitutionRecords() > 0 {
				return true
			} else {
				// If the head has invalidation tokens, we can count only those whose consideration time
				// is smaller or equal to the current time point
				for r := range head.GetEvaluationResults().Records {
					// If the consideration time of any records in the result substitution table is smaller or equal to the current time point,
					// the rule holds and we can safely return true.
					if r.ConsiderationTime <= clock.GetNow() {
						return true
					}
				}
			}
		}
	}
	// All rules have invalidation tokens, i.e., all of them are invalid
	return false
}

// CollectRecentConcludedAtoms returns the recent conclusions of the stratum in form of AtomBatches
func (stratum *Stratum) CollectRecentConcludedAtoms() *[]*data.AtomBatch {
	atomBatches := []*data.AtomBatch{}

	// Iterate over rules and retrieve the conclusions of each rule
	for _, r := range stratum.Rules {
		// We only create and return an atom batch if there is at least one record
		// in the head's evaluation results substitution table
		for _, head := range (*r).Head {
			if head.GetEvaluationResults().CountRecentSubstitutionRecords() > 0 {
				atomBatches = append(atomBatches, &data.AtomBatch{
					Predicate:       head.GetPredicate(),
					ConstantRecords: &head.GetEvaluationResults().RecentSubstitutionRecords,
				})
			}
		}
	}

	return &atomBatches
}

// CollectAllConcludedAtoms returns all conclusions of the stratum in form of AtomBatches
func (stratum *Stratum) CollectAllConcludedAtoms() *[]*data.AtomBatch {
	atomBatches := []*data.AtomBatch{}

	// Iterate over rules and retrieve the conclusions of each rule
	for _, r := range stratum.Rules {
		atomBatches = append(atomBatches, (*r.CollectRecentConcludedAtoms())...)
	}

	return &atomBatches

}

// CollectRecentAndNonRecentConcludedAtoms collects all concluded atoms, including recent and non-recent atoms
func (stratum *Stratum) CollectRecentAndNonRecentConcludedAtoms() *[]*data.AtomBatch {
	atomBatches := []*data.AtomBatch{}

	// Iterate over rules and retrieve the conclusions of each rule
	for _, r := range stratum.Rules {
		atomBatches = append(atomBatches, (*r.CollectRecentAndNonRecentConcludedAtoms())...)
	}

	return &atomBatches

}

// Cleanup removes the expired substitution records and invalidation tokens in the substitution tables
// of formula in the Stratum
func (stratum *Stratum) Cleanup() {
	for _, r := range stratum.Rules {
		r.Cleanup()
	}
	// Reset the invalidity time interval if needed
	if stratum.InvalidUntilTimePoint < clock.GetNow() && stratum.InvalidUntilTimePoint != types.NoTimePoint {
		stratum.InvalidUntilTimePoint = types.NoTimePoint
	}
}

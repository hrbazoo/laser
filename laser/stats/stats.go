package stats

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the data structure to collect performance and memory
 * usage statistics about the execution of the program.
 */

// LaserStats defines the structure that stores statistics about the memory consumption
// and execution of Laser
type LaserStats struct {
	TotalNumberOfJoinOperations           uint64 // Shows the number of joins performed by the program
	TotalNumberOfSkippedFormulaExecutions uint64 // Shows the number of time conjunct-formula not executed thanks to invalidation prediction
	TotalNumberOfSubstitutionRecords      uint64 // Shows the total number of substitution records in all substitution tables of the program
	TotalNumberOfInvalidationTokens       uint64 // Shows the total number of invalidation tokens in all substitution tables of the program
	TotalNumberOfFormulaEvaluations       uint64 // Shows the total number of times that formulae of any kind were
}

// ExecutionProfile stores some statistics about the execution of the program
var ExecutionProfile = LaserStats{
	TotalNumberOfJoinOperations:           0,
	TotalNumberOfSkippedFormulaExecutions: 0,
	TotalNumberOfSubstitutionRecords:      0,
	TotalNumberOfInvalidationTokens:       0,
	TotalNumberOfFormulaEvaluations:       0,
}

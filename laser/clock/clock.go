package clock

import "bitbucket.org/hrbazoo/laser/data/types"

// Now keeps the current time point
var Now = types.StartTimePoint

// Tuples keeps the number of input tuples
var Tuples = types.StartTupleCounter

// Reset sets the Now to the first time point and returns the reset clock
func Reset() types.TimePoint {
	Now = types.StartTimePoint
	return Now
}

// GetNow return the current time point
func GetNow() types.TimePoint {
	return Now
}

// GoToNextTimePoint increments the Now by one time point and returns the new time point
func GoToNextTimePoint() types.TimePoint {
	Now++
	return Now
}

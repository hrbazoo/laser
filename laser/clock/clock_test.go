package clock

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit unit tests of the clock.go source code
 */

// TestClock tests whether the clock reports the correct Now value and whether
// it increments the clock value correctly
func TestClock(t *testing.T) {
	assert := cassert.New(t)

	assert.Equal(Now, types.StartTimePoint)
	assert.Equal(GetNow(), types.StartTimePoint)

	GoToNextTimePoint()

	assert.Equal(Now, types.TimePoint(int(types.StartTimePoint)+1))
	assert.Equal(GetNow(), types.TimePoint(int(types.StartTimePoint)+1))

	Reset()

	assert.Equal(Now, types.StartTimePoint)
	assert.Equal(GetNow(), types.StartTimePoint)

	GoToNextTimePoint()

	assert.Equal(Now, types.TimePoint(int(types.StartTimePoint)+1))
	assert.Equal(GetNow(), types.TimePoint(int(types.StartTimePoint)+1))
}

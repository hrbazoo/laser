package data

import (
	"strings"
	"unsafe"

	"bitbucket.org/hrbazoo/laser/data/substitutiontable"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of the type Atom in Laser.
 */

// Atom is the structure that represents atomic facts such as p(a,b), it is comprised
// of a predicate and a number of substitutions for variables.
type Atom struct {
	Predicate     types.Predicate
	constantsInfo *substitutiontable.SubstitutionRecord
}

// GetConstants Returns the constants of the atom
func (a *Atom) GetConstants() []types.Constant {
	return *(*[]types.Constant)(unsafe.Pointer(&a.constantsInfo.Substitutions))
}

// GetPredicate returns the atom predicate
func (a *Atom) GetPredicate() types.Predicate {
	return a.Predicate
}

// GetConsiderationTime returns the consideration time of the atom
func (a *Atom) GetConsiderationTime() types.TimePoint {
	return a.constantsInfo.ConsiderationTime
}

// GetHorizonTime returns the horizon time of the atom
func (a *Atom) GetHorizonTime() types.TimePoint {
	return a.constantsInfo.HorizonTime
}

func (a *Atom) String() string {
	atomStringFormat := string(a.Predicate)
	// If there are substitutions for variables of the atom, we need to add '(' to the string
	if len(a.constantsInfo.Substitutions) > 0 {
		atomStringFormat += "("
	}
	// Add substitutions to the string + "," after each
	for i := 0; i < len(a.constantsInfo.Substitutions)-1; i++ {
		atomStringFormat += string(a.constantsInfo.Substitutions[i]) + ","
	}
	// The last substitution is an exception. We don't add "," after that. Instead, we add a ')'
	if len(a.constantsInfo.Substitutions) > 0 {
		atomStringFormat += string(a.constantsInfo.Substitutions[len(a.constantsInfo.Substitutions)-1])
		atomStringFormat += ")"
	}
	return atomStringFormat
}

// AtomBatch is a batch of atoms that all share the same predicate
type AtomBatch struct {
	Predicate       types.Predicate
	ConstantRecords *map[*substitutiontable.SubstitutionRecord]struct{}
}

// NewAtomBatch takes a list of strings, parses them and creates a list of AtomBatches
func NewAtomBatch(a *[]string) []*AtomBatch {

	predicateToConstants := map[types.Predicate]*map[*substitutiontable.SubstitutionRecord]struct{}{}
	for i := 0; i < len(*a); i++ {
		// Parse current atom
		atm := ParseAtom((*a)[i])
		// Does the predicate exists in the atom batch already?
		constantSet, exists := predicateToConstants[atm.Predicate]
		if !exists { // It does not exists, we need to add the set of constants to the atom batch for the given predicate
			predicateToConstants[atm.Predicate] = &map[*substitutiontable.SubstitutionRecord]struct{}{atm.constantsInfo: struct{}{}}
		} else { // Exist
			// Add records of constants to the set
			(*constantSet)[atm.constantsInfo] = struct{}{}
		}
	}

	// Create the array of atomBatches that we are going to return
	atomBatches := []*AtomBatch{}

	for predicate, constantSet := range predicateToConstants {
		atomBatches = append(atomBatches, &AtomBatch{
			Predicate:       predicate,
			ConstantRecords: constantSet,
		})
	}

	return atomBatches
}

// GetAtomStringFormats converts all atoms in the batch to string and returns an
// array of string
func (a *AtomBatch) GetAtomStringFormats() []string {
	atoms := []string{}
	// Get the predicate and add it to the string format of current
	pred := a.Predicate
	for substitutionRecord := range *a.ConstantRecords {
		// If the record is an invalidation token, ignore it
		if substitutionRecord.ConsiderationTime < 0 {
			continue
		}
		var atom strings.Builder
		// Add the predicate to the atom
		atom.WriteString(string(pred))
		// If there are substitutions for variables of the atom, we need to add '(' to the string
		if len(substitutionRecord.Substitutions) > 0 {
			atom.WriteString("(")
		}
		// Add substitutions to the string + "," after each
		for i := 0; i < len(substitutionRecord.Substitutions)-1; i++ {
			atom.WriteString(string(substitutionRecord.Substitutions[i]))
			atom.WriteString(",")
		}
		// The last substitution is an exception. We don't add "," after that. Instead, we add a ')'
		if len(substitutionRecord.Substitutions) > 0 {
			atom.WriteString(string(substitutionRecord.Substitutions[len(substitutionRecord.Substitutions)-1]))
			atom.WriteString(")")
		}
		// Add the string format to the list of string formats
		atoms = append(atoms, atom.String())
	}
	return atoms
}

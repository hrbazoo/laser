package types

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the definition of Constant type, which is the
 * type of constants in input atoms
 */

// Constant defines the type of constants in input atoms
type Constant Substitution

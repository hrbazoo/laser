package types

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains TimePoint type which defines descrete points in time
 */

// TimePoint defines descrete points in time
type TimePoint int

// NoTimePoint is a time-point that is ignored by the evaluation engine
const NoTimePoint = TimePoint(0)

// MaxTimePoint indicates the largest time point this program can handle
const MaxTimePoint = TimePoint(int(^uint(0) >> 1))

// UnknownTimePoint is used to indicate that the time point at which an event
// may happen is unknown
const UnknownTimePoint = NoTimePoint

// StartTimePoint is the time-point in which the execution of the program begins
const StartTimePoint = TimePoint(1)

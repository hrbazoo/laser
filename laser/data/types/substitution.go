package types

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the definition of Substitution type, which is the
 * types of data times stored in a substitution table.
 */

// Substitution defines the type of variable substitutions
// that are processed and stored in the substitution tables
type Substitution string

package types

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the definition of Variable type, which is the
 * type of variables in formula
 */

// Variable defines the type of variables in formula
type Variable string

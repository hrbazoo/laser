package types

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains TupleCounter type which defines the sequence
 * number associated with each incoming tuple
 */

// TupleCounter defines descrete sequence number associated with each incoming tuple
type TupleCounter int

// NoTupleCounter is a tuple counter that is ignored by the evaluation engine
const NoTupleCounter = TupleCounter(0)

// UnknownTupleCounter is used to indicate that the tuple counter at which an event
// may happen is unknown
const UnknownTupleCounter = NoTupleCounter

// StartTupleCounter is the counter value of the first tuple that is received
const StartTupleCounter = TupleCounter(1)

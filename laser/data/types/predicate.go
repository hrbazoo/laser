package types

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the definition of type Predicate in Laser. An incoming
 * data item (atom) usually consists of a predicate and zero or more constants.
 */

// Predicate defines the type of predicates in Laser
type Predicate string

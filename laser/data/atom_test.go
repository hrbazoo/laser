package data

import (
	"strings"
	"testing"
	"unsafe"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of the type Atom unit tests.
 */

// TestAtomNum1 tests whether parsing atom batches works
func TestAtomNum1(t *testing.T) {
	assert := cassert.New(t)

	atoms := NewAtomBatch(&[]string{
		"p(a,b)",
		"q(c,d)",
		"r",
		"p(c,d)",
	})

	assert.Equal(len(atoms), 3)

	predicates := map[types.Predicate]int{}

	for _, ab := range atoms {
		predicates[ab.Predicate]++
	}

	pCount, pExists := predicates[types.Predicate("p")]
	qCount, qExists := predicates[types.Predicate("q")]
	rCount, rExists := predicates[types.Predicate("r")]

	assert.Equal(pExists, true)
	assert.Equal(qExists, true)
	assert.Equal(rExists, true)

	assert.Equal(pCount, 1)
	assert.Equal(qCount, 1)
	assert.Equal(rCount, 1)

	for _, atomBatch := range atoms {
		if atomBatch.Predicate == types.Predicate("p") {
			m := map[string]int{}
			assert.Equal(len(*atomBatch.ConstantRecords), 2)
			for c := range *atomBatch.ConstantRecords {
				m[strings.Join(*(*[]string)(unsafe.Pointer(&c.Substitutions)), "")]++
				assert.Equal(c.ConsiderationTime, types.StartTimePoint)
				assert.Equal(c.HorizonTime, types.StartTimePoint)
			}
			_, abExists := m["ab"]
			_, cdExists := m["cd"]
			assert.Equal(abExists, true)
			assert.Equal(cdExists, true)
		} else if atomBatch.Predicate == types.Predicate("q") {
			m := map[string]int{}
			assert.Equal(len(*atomBatch.ConstantRecords), 1)
			for c := range *atomBatch.ConstantRecords {
				m[strings.Join(*(*[]string)(unsafe.Pointer(&c.Substitutions)), "")]++
				assert.Equal(c.ConsiderationTime, types.StartTimePoint)
				assert.Equal(c.HorizonTime, types.StartTimePoint)
			}
			_, cdExists := m["cd"]
			assert.Equal(cdExists, true)
		} else if atomBatch.Predicate == types.Predicate("r") {
			m := map[string]int{}
			assert.Equal(len(*atomBatch.ConstantRecords), 1)
			for c := range *atomBatch.ConstantRecords {
				m[strings.Join(*(*[]string)(unsafe.Pointer(&c.Substitutions)), "")]++
				assert.Equal(c.ConsiderationTime, types.StartTimePoint)
				assert.Equal(c.HorizonTime, types.StartTimePoint)
			}
			_, emptyExists := m[""] // Has no constants
			assert.Equal(emptyExists, true)
		} else { // We must never get here
			assert.Equal(true, false)
		}
	}
}

// TestAtomNum2
func TestAtomNum2(t *testing.T) {
	assert := cassert.New(t)

	atoms := NewAtomBatch(&[]string{
		"p(a,b)",
	})

	assert.Equal(len(atoms), 1)

	assert.Equal(atoms[0].GetAtomStringFormats()[0], "p(a,b)")

	atoms = NewAtomBatch(&[]string{
		"p",
	})

	assert.Equal(len(atoms), 1)

	assert.Equal(atoms[0].GetAtomStringFormats()[0], "p")

	atoms = NewAtomBatch(&[]string{
		"p(a)",
	})

	assert.Equal(len(atoms), 1)

	assert.Equal(atoms[0].GetAtomStringFormats()[0], "p(a)")
}

func TestAtomNum3(t *testing.T) {
	assert := cassert.New(t)

	a1 := ParseAtom("p(a,b)")

	assert.Equal((*a1).String(), "p(a,b)")

	a2 := ParseAtom("p(a)")

	assert.Equal((*a2).String(), "p(a)")

	a3 := ParseAtom("p")

	assert.Equal((*a3).String(), "p")
}

package substitutiontable

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the core implementation of substitution tables in
 * Laser stream reasoner.
 */

import (
	"fmt"
	"log"
	"sync"

	"bitbucket.org/hrbazoo/laser/stats"

	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data/types"
)

// SubstitutionTable contains the internal data structures of substitution tables
// that are the corner stones of the Laser stream reasoner. Substitution tables store
// the substitutions of variables in Laser rules together with additional annotations
// that indicate for how long each substitution holds or does not hold. This information
// is then used by other algorithms in Laser to accelerator the evaluations of rules
type SubstitutionTable struct {
	Records                    map[*SubstitutionRecord]struct{}                     // Substitution records
	InvalidationTokens         map[*SubstitutionRecord]struct{}                     // Invalidation tokens
	ConsiderationTimeToRecords map[types.TimePoint]map[*SubstitutionRecord]struct{} // Maps consideration time to substitution records
	HorizonTimeToRecords       map[types.TimePoint]map[*SubstitutionRecord]struct{} // Maps horizon time to substitution records
	VariableToRecordIndex      map[types.Variable][]int                             // Maps variable names (column headers) to their indexes in the record
	RecentSubstitutionRecords  map[*SubstitutionRecord]struct{}                     // Substitution records recently added to the table
	RecentInvalidationTokens   map[*SubstitutionRecord]struct{}                     // Invalidation tokens recently added to the table
	NumberOfVariables          int                                                  // Holds the number of variables (columns) in the substitution table
}

// New takes an array of variable names and creates an empty substitution table with
// as many columns (the length of records) as the number of given variables and returns
// a pointer to the newly created table.
func New(variables []types.Variable) *SubstitutionTable {
	// First, map the variable names to their indexes in the record
	variableToRecordIndex := map[types.Variable][]int{} // Maps variable names to record indexes
	// The indexes are determined by the order of variables
	for idx, varName := range variables {
		// Map each variable to its index in the array of variables
		variableToRecordIndex[varName] = append(variableToRecordIndex[varName], idx)
	}
	// Create the data structure and returns its pointer
	return &SubstitutionTable{
		Records:                    map[*SubstitutionRecord]struct{}{},                     // Empty array of records
		InvalidationTokens:         map[*SubstitutionRecord]struct{}{},                     // The invalidation tokens
		ConsiderationTimeToRecords: map[types.TimePoint]map[*SubstitutionRecord]struct{}{}, // Empty map of consideration time to substitution records
		HorizonTimeToRecords:       map[types.TimePoint]map[*SubstitutionRecord]struct{}{}, // Empty map of horizon time to substitution records
		VariableToRecordIndex:      variableToRecordIndex,                                  // header of records (variables)
		RecentSubstitutionRecords:  map[*SubstitutionRecord]struct{}{},                     // An empty set of substitution records recently added to the table
		RecentInvalidationTokens:   map[*SubstitutionRecord]struct{}{},                     // An empty set of invalidation token recently added to the table
		NumberOfVariables:          len(variables),                                         // The number of variables (columns) in the substitution table
	}
}

// Insert takes a substitution record and adds it to the substitution table.
// If the record is an invalidation token, adds it to the list of invalidation
// tokens, otherwise, while adding the record, it creates mappings from the
// records's consideration time and the horizon time to a pointer of the record
// in order to accelerate the future lookups.
func (table *SubstitutionTable) Insert(record *SubstitutionRecord) {
	// If the record is nil, return immediately
	if record == nil {
		return
	}
	// If the program is configured to not use the invalidation prediction algorithm, it must panic if an invalidation
	// token is to be stored in the substitution table.
	if !config.UseInvalidationPrediction && record.ConsiderationTime < 0 {
		panic("Program is not configured to use invalidation prediction. Yet, attempted to store invalidation token in the substitution table")
	}
	// If the consideration time (considertation counter) of the record is negative number, the record
	// is an invalidation token and must be appended to the invalidation token slice. This must be done
	// only when the program is configured to use the invalidation prediction algorithm.
	if config.UseInvalidationPrediction && record.ConsiderationTime < 0 {
		// Update the stats about the total number of invalidation tokens
		incrementNumberOfInvalidationTokens()
		// Store the token in the substitution table
		table.InvalidationTokens[record] = struct{}{}
		// If the horizon time is not already in the table, add it
		if _, ok := table.HorizonTimeToRecords[record.HorizonTime]; !ok {
			table.HorizonTimeToRecords[record.HorizonTime] = map[*SubstitutionRecord]struct{}{}
		}
		// Create the mapping
		table.HorizonTimeToRecords[record.HorizonTime][record] = struct{}{}
		// Add it to the set of recently added records
		table.RecentInvalidationTokens[record] = struct{}{}
		return // Return, we are done!
	}
	// If not an invalidation token, first check if there are exactly as many substitutions
	// as the number of variables
	if len(record.Substitutions) != table.NumberOfVariables {
		fmt.Println(record)
		fmt.Println(table.VariableToRecordIndex)
		msg := fmt.Sprintf("Too few or too many substitutions in the substitution record: Expected %d, received %d", len(record.Substitutions), table.NumberOfVariables)
		panic(msg)
	}
	// Add the record to the set of records
	table.Records[record] = struct{}{}
	// Increment the total number of substitution records in the table
	incrementNumberOfSubstitutionRecords()
	// Create a mapping from the record's consideration time to the record's pointer
	if _, ok := table.ConsiderationTimeToRecords[record.ConsiderationTime]; !ok {
		// No record with this consideration time in the table? Create the set
		table.ConsiderationTimeToRecords[record.ConsiderationTime] = map[*SubstitutionRecord]struct{}{}
	}
	// Add the new record to the mapping from consideration time to record pointer
	table.ConsiderationTimeToRecords[record.ConsiderationTime][record] = struct{}{}
	// Create a mapping from the record's horizon time to the record's pointer
	if _, ok := table.HorizonTimeToRecords[record.HorizonTime]; !ok {
		// No record with this horizon time in the table? Create the set
		table.HorizonTimeToRecords[record.HorizonTime] = map[*SubstitutionRecord]struct{}{}
	}
	// add the new record to the mapping from horizon time to record pointers
	table.HorizonTimeToRecords[record.HorizonTime][record] = struct{}{}
	// Add the record to the set of recently added records
	table.RecentSubstitutionRecords[record] = struct{}{}
}

// GetInvalidationTokens returns the set of invalidation tokens currently
// inside the substitution table
func (table *SubstitutionTable) GetInvalidationTokens() map[*SubstitutionRecord]struct{} {
	if !config.UseInvalidationPrediction {
		panic("Program is configured to not use invalidation token, yet requested to get the invalidation tokens")
	}
	return table.InvalidationTokens
}

// GetRecordsByConsiderationTime takes a time point as parameter and returns
// all records in the substitution table whose consideration time is equal to
// the given time point. Returns an empty map of SubstitutionRecord pointers
// if the requested time point does not exist in the substitution table.
func (table *SubstitutionTable) GetRecordsByConsiderationTime(t types.TimePoint) map[*SubstitutionRecord]struct{} {
	// Try to get the records in the table
	records, ok := table.ConsiderationTimeToRecords[t]
	// If there is no record for the given consideration time, return an empty map
	if !ok {
		return map[*SubstitutionRecord]struct{}{}
	}
	// If there are indeed records at the given consideration time point, return them
	return records
}

// ContainsSubstitutions takes a list of substitutions and a horizon time and a consideration time and returns true if at the given
// time points the substitution table contains the given substitutions
func (table *SubstitutionTable) ContainsSubstitutions(substitutions *[]types.Substitution, ct types.TimePoint, ht types.TimePoint) bool {
	if substitutions == nil {
		return false
	}

	records, ok := table.HorizonTimeToRecords[ht]
	if ok {
		for r := range records {
			if r.ConsiderationTime == ct && len(*substitutions) == len(r.Substitutions) {
				equal := true
				for i := 0; i < len(*substitutions); i++ {
					if (*substitutions)[i] != r.Substitutions[i] {
						equal = false
						break
					}
				}
				if equal {
					return true
				}
			}
		}
	}
	return false
}

// Contains takes a substitution record as well as the mapping from variables to positions in that record
// and determines whether the substitution table contains a substitution record equivalent to the given
// record
func (table *SubstitutionTable) Contains(record *SubstitutionRecord, varToRecordIndex *map[types.Variable][]int) bool {
	// nil records are not expected at this stage. If the record is nil,
	// log a warning message and return false
	if record == nil {
		log.Printf("WARNING: didn't expect a nil record in the Contains() function of the substitution table")
		return false
	}

	ct := record.ConsiderationTime        // Keep the consideration time of the record
	ht := record.HorizonTime              // Keep the Horizon time of the record
	cc := record.ConsiderationCounter     // Keep the consideration counter of the record
	hc := record.HorizonCounter           // Keep the horizon counter of the record
	substitutions := record.Substitutions // Substitutions of the record

	// Look in the mapping from the consideration time to records to see if there is
	// any record with the given consideration time or not.
	records, ok := table.ConsiderationTimeToRecords[ct]
	// If there is no record with the given record's substitution table, return immediately
	if !ok {
		return false
	}

	// Loop over retrieved records to see if any of them is equivalent of the given record
	for r := range records {
		// If all other record's information matches that of given record
		// return true
		if r.HorizonTime == ht && r.HorizonCounter == hc && r.ConsiderationCounter == cc {
			// If the length of slices differ, ignore the record
			if len(substitutions) != len(r.Substitutions) {
				continue
			}

			equal := true // Flag which is set if the two slices are different
			// Loop and compare each substitution
			for variable, indexes1 := range *varToRecordIndex {
				index1 := indexes1[0] // If a variable appears in more than two positions in the record, the are certainly equal at this point
				if indexes2, exists := table.VariableToRecordIndex[variable]; exists {
					index2 := indexes2[0]
					if substitutions[index1] != r.Substitutions[index2] {
						equal = false // Indicates that substitution records are not equal
						break
					}
				}
			}

			// If substitutions are not equal, go to the next record
			if !equal {
				continue
			}

			return true
		}
	}

	// Didn't find the match? Return false
	return false
}

// GetRecordsByHorizonTime takes a time point as parameter and returns
// all records in the substitution table whose horizon time is equal to
// the given time point. Returns an empty map of SubstitutionRecord pointers
// if the requested time point does not exist in the substitution table.
func (table *SubstitutionTable) GetRecordsByHorizonTime(t types.TimePoint) map[*SubstitutionRecord]struct{} {
	// Try to get the records in the table
	records, ok := table.HorizonTimeToRecords[t]
	// If there is no record for the given horizon time, return an empty map
	if !ok {
		return map[*SubstitutionRecord]struct{}{}
	}
	// If there are indeed records at the given horizon time point, return them
	return records

}

// DeleteRecentRecords removes empties the set of recent records
func (table *SubstitutionTable) DeleteRecentRecords() {
	// Only if the program is configured to use invalidation prediction algorithm, clean the
	// buffer for the recent invalidation tokens
	if config.UseInvalidationPrediction {
		table.RecentInvalidationTokens = map[*SubstitutionRecord]struct{}{}
	}
	table.RecentSubstitutionRecords = map[*SubstitutionRecord]struct{}{}
}

// DeleteRecordsByHorizonTime takes a time point as parameter and deletes all
// records whose horizon time is equal to the given time point from the table
// as well as from the mappings from the consideration time and horizon time to
// records.
func (table *SubstitutionTable) DeleteRecordsByHorizonTime(t types.TimePoint) {
	// Clean the table that ensures uniqueness from all records that have this horizon time
	// table.existingRecordsAndTokens[t] = map[string]struct{}{}
	// Try to get the records in the table. These are either normal records or
	// they are invalidation tokens
	records, ok := table.HorizonTimeToRecords[t]
	// If there is no record or invalidation token in the table for the given
	// time point, return immediately
	if !ok {
		return
	}
	// Loop over the found records and remove them one by one from the table. Further,
	// Remove the mappings from the consideration time and horizon time to the records
	for record := range records {
		// For normal records, remove not only the mapping from the horizon time to the
		// records, but also from the consideration time to them
		if record.ConsiderationTime > 0 {
			delete(table.Records, record)
			// Delete the mapping from consideration time to the record
			delete(table.ConsiderationTimeToRecords[record.ConsiderationTime], record)
			// If there is no mapping from a consideration time point to record, remove the time point from
			// the map
			if len(table.ConsiderationTimeToRecords[record.ConsiderationTime]) == 0 {
				delete(table.ConsiderationTimeToRecords, record.ConsiderationTime)
			}
		} else { // Negative consideration time indicates invalidation tokens
			// Delete the invalidation tokens
			delete(table.InvalidationTokens, record)
		}
		// Delete the mapping from the horizon time to the record
		delete(table.HorizonTimeToRecords[record.HorizonTime], record)
		// If there is no mapping from a horizon time point to a record, remove
		// the time point from the map
		if len(table.HorizonTimeToRecords[record.HorizonTime]) == 0 {
			delete(table.HorizonTimeToRecords, record.HorizonTime)
		}
	}
}

// Merge takes another substitution table and merges that into this substitution table
// Obviously, the underlining assumption behind this method is that both tables have the
// same variables; both in number and name
func (table *SubstitutionTable) Merge(other *SubstitutionTable) {
	for _, records := range other.ConsiderationTimeToRecords {
		for r := range records {
			table.Insert(r)
		}
	}
}

// Size returns the number of records (invalidation tokens as well as the
// substitution records) that are currently in the table
func (table *SubstitutionTable) Size() int {
	// If the program is configured to use invalidation prediction algorithm,
	// the total number of items in the substitution table equals the number
	// of substitution records plus the number of invalidation tokens in the substitution table.
	if config.UseInvalidationPrediction {
		return len(table.Records) + len(table.InvalidationTokens)
	}
	// If the invalidation prediction algorithm is not used, the size of the substitution table is
	// the total number of substitution records.
	return len(table.Records)
}

// CountSubstitutionRecords returns the number of substitution records that are currently in the table
func (table *SubstitutionTable) CountSubstitutionRecords() int {
	return len(table.Records)
}

// CountInvalidationTokens returns the number of invalidation tokens that are currently in the table
func (table *SubstitutionTable) CountInvalidationTokens() int {
	// If the invalidation prediction algorithm is not used, panic if requested to count them
	if !config.UseInvalidationPrediction {
		panic("The program is configured to not use invalidation prediction algorithm, yet, the method to count invalidation tokens is called!")
	}
	return len(table.InvalidationTokens)
}

// CountRecentSubstitutionRecords returns the number of recently added substitution records to the table
func (table *SubstitutionTable) CountRecentSubstitutionRecords() int {
	return len(table.RecentSubstitutionRecords)
}

// CountRecentInvalidationTokens returns the number of recently added invalidation tokens to the table
func (table *SubstitutionTable) CountRecentInvalidationTokens() int {
	// If the invalidation prediction algorithm is not used, panic if requested to count them
	if !config.UseInvalidationPrediction {
		panic("The program is configured to not use invalidation prediction algorithm, yet, the method to count recent invalidation tokens is called!")
	}
	return len(table.RecentInvalidationTokens)
}

// Updates the total number of substitution records stored in the substitution tables.
// To possible race conditions (if ever the program is going to be parallelized), we
// use mutexes to sync the execution
func incrementNumberOfSubstitutionRecords() {
	mutex := &sync.Mutex{}                                    // Create the mutex
	mutex.Lock()                                              // Only one thread is allowed to execute the following line at a time
	stats.ExecutionProfile.TotalNumberOfSubstitutionRecords++ // Increment the counter
	mutex.Unlock()                                            // Unlock the mutex! Allow another thread to increment the counter
}

// Updates the total number of invalidation tokens stored in the substitution tables.
// To possible race conditions (if ever the program is going to be parallelized), we
// use mutexes to sync the execution
func incrementNumberOfInvalidationTokens() {
	// If the program is not configured to use invalidation prediction algorithm, panic!
	if !config.UseInvalidationPrediction {
		panic("Trying to increment the total number of invalidation tokens while the program is not configured to use invalidation tokens")
	}
	mutex := &sync.Mutex{}                                   // Create the mutex
	mutex.Lock()                                             // Only one thread is allowed to execute the following line at a time
	stats.ExecutionProfile.TotalNumberOfInvalidationTokens++ // Increment the counter
	mutex.Unlock()                                           // Unlock the mutex! Allow another thread to increment the counter
}

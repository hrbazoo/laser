package substitutiontable

/*
 * Copyright (C) 2018, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the core implementation of substitution record in
 * Laser stream reasoner.
 */

import (
	"bitbucket.org/hrbazoo/laser/data/types"
)

// SubstitutionRecord stores the substitutions of variables in the Laser stream reasoner rules.
// Each substitution record is annotated with some additional information that indicates
// when and for how long the substitutions hold
type SubstitutionRecord struct {
	Substitutions        []types.Substitution // Substitutions
	ConsiderationTime    types.TimePoint      // The time point in which substitutions begin to hold
	ConsiderationCounter types.TupleCounter   // The tuple counter after which the substitutions begin to hold
	HorizonTime          types.TimePoint      // The time point in which the substitutions cease to hold
	HorizonCounter       types.TupleCounter   // The tuple counter after which the substitutions cease to hold
}

// Creates an empty substitution record. sets the consideration time,
// horizon time, consideration counter, and horizon counter of the
// new record to zero. Returns a pointer to the newly created record
func createEmptySubstitutionRecord() *SubstitutionRecord {
	return &SubstitutionRecord{
		Substitutions:        []types.Substitution{}, // Empty substitutions
		ConsiderationTime:    types.NoTimePoint,      // Consideration time
		ConsiderationCounter: types.NoTupleCounter,   // Consideration counter
		HorizonTime:          types.NoTimePoint,      // Horizon time
		HorizonCounter:       types.NoTupleCounter,   // Horizon counter
	}
}

// CreateInvalidationToken takes two numbers for the consideration time and the horizon time and
// creates an invalidation token accordingly. Returns a pointer to the newly created record
func CreateInvalidationToken(ct types.TimePoint, ht types.TimePoint) *SubstitutionRecord {
	return &SubstitutionRecord{
		Substitutions:        []types.Substitution{}, // Empty substitutions
		ConsiderationTime:    -ct,                    // Consideration time
		ConsiderationCounter: types.NoTupleCounter,   // Consideration counter
		HorizonTime:          ht,                     // Horizon time
		HorizonCounter:       types.NoTupleCounter,   // Horizon counter
	}
}

// CreateSubstitutionRecord takes two numbers for the consideration time and the horizon time and
// creates an substitution record accordingly. Returns a pointer to the newly created record
func CreateSubstitutionRecord(substitutions *[]types.Substitution, ct types.TimePoint, ht types.TimePoint) *SubstitutionRecord {
	return &SubstitutionRecord{
		Substitutions:        *substitutions,       // Variable substitutions
		ConsiderationTime:    ct,                   // Consideration time
		ConsiderationCounter: types.NoTupleCounter, // Consideration counter
		HorizonTime:          ht,                   // Horizon time
		HorizonCounter:       types.NoTupleCounter, // Horizon counter
	}
}

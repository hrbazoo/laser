package substitutiontable

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2018-2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the unit unit tests of substitution tables in
 * Laser stream reasoner.
 */

// TestSubstitutionTableNum1 tests whether the substitution table correctly
// maps the variable names to indexes
func TestSubstitutionTableNum1(t *testing.T) {
	assert := cassert.New(t)

	// The list of variables
	variables := [...]types.Variable{"A", "B"}

	// Create the empty substitution table
	substitutionTable := New(variables[:])

	// Check if the substitutiontable's variable-to-index map has correct
	// length
	assert.Equal(len(substitutionTable.VariableToRecordIndex), len(variables))

	// Check if each variable is mapped to the right index value
	assert.Equal(substitutionTable.VariableToRecordIndex["A"], []int{0})
	assert.Equal(substitutionTable.VariableToRecordIndex["B"], []int{1})

}

// TestSubstitutionTableInsertNum1 tests whether inserting new record in the
// substitution table works as expected or not
func TestSubstitutionTableInsertNum1(t *testing.T) {
	assert := cassert.New(t)

	// The list of variables
	variables := [...]types.Variable{"A", "B"}

	// Create the empty substitution table
	substitutionTable := New(variables[:])

	// The consideration time of the first record we store in the substitution table
	considerationTime := types.StartTimePoint

	// Create substitution table records
	record1 := CreateSubstitutionRecord(&[]types.Substitution{"a1", "b1"}, considerationTime, considerationTime+1)

	assert.Equal(record1.Substitutions, []types.Substitution{"a1", "b1"})
	assert.Equal(record1.ConsiderationTime, considerationTime)
	assert.Equal(record1.HorizonTime, considerationTime+1)
	assert.Equal(record1.HorizonCounter, types.NoTupleCounter)
	assert.Equal(record1.ConsiderationCounter, types.NoTupleCounter)

	record2 := CreateSubstitutionRecord(&[]types.Substitution{"a2", "b2"}, considerationTime+1, considerationTime+2)

	// Check if the substitutiontable's variable-to-index map has correct
	// length
	assert.Equal(len(substitutionTable.VariableToRecordIndex), len(variables))

	// Check if each variable is mapped to the right index value
	assert.Equal(substitutionTable.VariableToRecordIndex["A"], []int{0})
	assert.Equal(substitutionTable.VariableToRecordIndex["B"], []int{1})

	// Insert both records in the substitution table
	substitutionTable.Insert(record1)
	substitutionTable.Insert(record2)

	// Test if the size of table is now equal to two
	assert.Equal(substitutionTable.Size(), 2)
	assert.Equal(substitutionTable.CountSubstitutionRecords(), 2)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 2)
	if config.UseInvalidationPrediction {
		assert.Equal(substitutionTable.CountRecentInvalidationTokens(), 0)
	}

	// Test if the inserted record is in the table
	records := substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(1))
	assert.Equal(len(records), 1)
	assert.Equal(records[record1], struct{}{})

	// Delete all records with horizon time 2
	substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(2))
	substitutionTable.DeleteRecentRecords()

	// Test if the size of table is now equal to one after the deletion
	assert.Equal(substitutionTable.Size(), 1)
	assert.Equal(substitutionTable.CountSubstitutionRecords(), 1)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.Equal(substitutionTable.CountRecentInvalidationTokens(), 0)
	}

	// Test if the second record is still in the table
	records = substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(2))
	assert.Equal(len(records), 1)
	assert.Equal(records[record2], struct{}{})

	// Delete all records with horizon time 3
	substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(3))

	// Test if the size of table is now equal to zero
	assert.Equal(substitutionTable.Size(), 0)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	if config.UseInvalidationPrediction {
		assert.Equal(substitutionTable.CountRecentInvalidationTokens(), 0)
	}
	// Test if record retrival behavior is right when the table is empty
	records = substitutionTable.GetRecordsByConsiderationTime(types.TimePoint(2))
	assert.Equal(len(records), 0)

	// Mapping from consideration time and horizon time to records must be empty now
	assert.Equal(len(substitutionTable.ConsiderationTimeToRecords), 0)
	assert.Equal(len(substitutionTable.HorizonTimeToRecords), 0)
}

// TestSubstitutionTableInvalidationTokens tests whether inserting new
// invalidation tokens and getting it back works
func TestSubstitutionTableInvalidationTokens(t *testing.T) {
	// If the program is configured to not use invalidation prediction algorithm, skip this test
	if !config.UseInvalidationPrediction {
		t.Skip()
	}
	assert := cassert.New(t)

	// The list of variables
	variables := [...]types.Variable{"A", "B"}

	// Create the empty substitution table
	substitutionTable := New(variables[:])

	// Create invalidation tokens. A substitution record is
	// considered invalidation token when the consideration
	// time or counter is negative value
	record1 := SubstitutionRecord{
		[]types.Substitution{},
		types.TimePoint(-1),
		types.TupleCounter(types.NoTupleCounter),
		types.TimePoint(-2),
		types.TupleCounter(types.NoTupleCounter),
	}
	record2 := SubstitutionRecord{
		[]types.Substitution{},
		types.TimePoint(-2),
		types.TupleCounter(types.NoTupleCounter),
		types.TimePoint(-3),
		types.TupleCounter(types.NoTupleCounter),
	}

	// Check if the substitutiontable's variable-to-index map has correct length
	assert.Equal(len(substitutionTable.VariableToRecordIndex), len(variables))

	// Check if each variable is mapped to the right index value
	assert.Equal(substitutionTable.VariableToRecordIndex["A"], []int{0})
	assert.Equal(substitutionTable.VariableToRecordIndex["B"], []int{1})

	// Insert both records in the substitution table
	substitutionTable.Insert(&record1)
	substitutionTable.Insert(&record2)

	// Test if the size of table is now equal to two
	assert.Equal(substitutionTable.Size(), 2)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	assert.Equal(substitutionTable.CountRecentInvalidationTokens(), 2)

	// Test if the inserted record is in the table
	assert.Equal(substitutionTable.InvalidationTokens, map[*SubstitutionRecord]struct{}{
		&record1: struct{}{},
		&record2: struct{}{},
	})

	// Delete all records with horizon time 2
	substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(-2))

	assert.Equal(substitutionTable.Size(), 1)
	assert.Equal(substitutionTable.CountInvalidationTokens(), 1)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	assert.Equal(substitutionTable.CountRecentInvalidationTokens(), 2)

	substitutionTable.DeleteRecentRecords()

	// Test if the size of table is now equal to one after the deletion
	assert.Equal(substitutionTable.Size(), 1)
	assert.Equal(substitutionTable.CountInvalidationTokens(), 1)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	assert.Equal(substitutionTable.CountRecentInvalidationTokens(), 0)

	// Test if the second record is still in the table
	assert.Equal(substitutionTable.InvalidationTokens, map[*SubstitutionRecord]struct{}{
		&record2: struct{}{},
	})

	// Delete all records with horizon time 3
	substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(-3))

	// Test if the size of table is now equal to zero
	assert.Equal(substitutionTable.Size(), 0)
	assert.Equal(substitutionTable.CountInvalidationTokens(), 0)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	assert.Equal(substitutionTable.CountRecentSubstitutionRecords(), 0)
	assert.Equal(substitutionTable.CountRecentInvalidationTokens(), 0)
	// Test if record retrival behavior is right when the table is empty
	// Test if the second record is still in the table
	assert.Equal(substitutionTable.InvalidationTokens, map[*SubstitutionRecord]struct{}{})

	// Mapping from consideration time and horizon time to records must be empty now
	assert.Equal(len(substitutionTable.ConsiderationTimeToRecords), 0)
	assert.Equal(len(substitutionTable.HorizonTimeToRecords), 0)
}

// TestSubstitutionTableContains tests whether Contains() routine
// works as expected or not
func TestSubstitutionTableContains(t *testing.T) {
	assert := cassert.New(t)

	// The list of variables
	variables := [...]types.Variable{"A", "B"}

	// Create the empty substitution table
	substitutionTable := New(variables[:])

	// Create the records to insert into the substitution table
	record1 := SubstitutionRecord{
		[]types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		types.TimePoint(types.StartTimePoint),
		types.TupleCounter(types.NoTupleCounter),
		types.TimePoint(types.StartTimePoint + 1),
		types.TupleCounter(types.NoTupleCounter),
	}
	record2 := SubstitutionRecord{
		[]types.Substitution{types.Substitution("a2"), types.Substitution("b2")},
		types.TimePoint(types.StartTimePoint + 1),
		types.TupleCounter(types.NoTupleCounter),
		types.TimePoint(types.StartTimePoint + 2),
		types.TupleCounter(types.NoTupleCounter),
	}

	// Insert both records in the substitution table
	substitutionTable.Insert(&record1)
	substitutionTable.Insert(&record2)

	// Test if the size of table is now equal to two
	assert.Equal(substitutionTable.Size(), 2)

	// Test if the inserted record is in the table
	assert.Equal(substitutionTable.Records, map[*SubstitutionRecord]struct{}{
		&record1: struct{}{},
		&record2: struct{}{},
	})

	// Records exist in the substitution table, so Contains() routine must return true
	assert.Equal(substitutionTable.Contains(&record1, &substitutionTable.VariableToRecordIndex), true)
	assert.Equal(substitutionTable.Contains(&record2, &substitutionTable.VariableToRecordIndex), true)

	// Delete all records with horizon time 2
	substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(types.StartTimePoint + 1))

	// Test if the size of table is now equal to one after the deletion
	assert.Equal(substitutionTable.Size(), 1)

	// The substitution table must not contain first record anymore
	assert.Equal(substitutionTable.Contains(&record1, &substitutionTable.VariableToRecordIndex), false)

	// Test if the second record is still in the table
	assert.Equal(substitutionTable.Records, map[*SubstitutionRecord]struct{}{
		&record2: struct{}{},
	})
	// The substitution table must still contain the second record
	assert.Equal(substitutionTable.Contains(&record2, &substitutionTable.VariableToRecordIndex), true)

	// Delete also the second row
	substitutionTable.DeleteRecordsByHorizonTime(types.TimePoint(types.StartTimePoint + 2))

	// Test if the size of table is now equal to zero
	assert.Equal(substitutionTable.Size(), 0)

	// Test if record retrival behavior is right when the table is empty
	// Test if the second record is still in the table
	assert.Equal(substitutionTable.Records, map[*SubstitutionRecord]struct{}{})

	// The substitution table must not contain either of the records
	assert.Equal(substitutionTable.Contains(&record1, &substitutionTable.VariableToRecordIndex), false)
	assert.Equal(substitutionTable.Contains(&record2, &substitutionTable.VariableToRecordIndex), false)

	// Mapping from consideration time and horizon time to records must be empty now
	assert.Equal(len(substitutionTable.ConsiderationTimeToRecords), 0)
	assert.Equal(len(substitutionTable.HorizonTimeToRecords), 0)
}

// TestSubstitutionTableMerge tests whether Merge() routine
// works as expected or not
func TestSubstitutionTableMerge(t *testing.T) {
	assert := cassert.New(t)

	// The list of variables
	variables := [...]types.Variable{"A", "B"}

	// Create the empty substitution table
	table1 := New(variables[:])
	table2 := New(variables[:])

	// Create the records to insert into the substitution table
	record1 := SubstitutionRecord{
		[]types.Substitution{types.Substitution("a1"), types.Substitution("b1")},
		types.TimePoint(types.StartTimePoint),
		types.TupleCounter(types.NoTupleCounter),
		types.TimePoint(types.StartTimePoint + 1),
		types.TupleCounter(types.NoTupleCounter),
	}
	record2 := SubstitutionRecord{
		[]types.Substitution{types.Substitution("a2"), types.Substitution("b2")},
		types.TimePoint(types.StartTimePoint + 1),
		types.TupleCounter(types.NoTupleCounter),
		types.TimePoint(types.StartTimePoint + 2),
		types.TupleCounter(types.NoTupleCounter),
	}

	// Insert the first record in the first substitution table
	table1.Insert(&record1)

	// Insert the second record in the second substitution table
	table2.Insert(&record2)

	// Both tables must contain one record now
	assert.Equal(table1.Size(), 1)
	assert.Equal(table2.Size(), 1)

	// Records exist in the substitution tables, so Contains() routine must return true
	assert.Equal(table1.Contains(&record1, &table1.VariableToRecordIndex), true)
	assert.Equal(table2.Contains(&record2, &table2.VariableToRecordIndex), true)

	// Merge the second table into the first table
	table1.Merge(table2)

	// The first table must contain two records now
	assert.Equal(table1.Size(), 2)
	// The second table must still contain a single record
	assert.Equal(table2.Size(), 1)

	// Records exist in the substitution tables, so Contains() routine must return true
	assert.Equal(table1.Contains(&record1, &table1.VariableToRecordIndex), true)
	assert.Equal(table1.Contains(&record2, &table2.VariableToRecordIndex), true)

	// The second record must exist in the second table
	assert.Equal(table2.Contains(&record2, &table2.VariableToRecordIndex), true)
}

package data

import (
	"fmt"
	"strings"
	"unsafe"

	"bitbucket.org/hrbazoo/laser/clock"

	"bitbucket.org/hrbazoo/laser/data/substitutiontable"

	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the implementation of a parser for atoms in Laser 2.0
 */

// Atomsin Laser 2.0 follow the following structure
// <predicate> [(<constant1> [, <constant2>...])]

// These are the reserved delims
var delims = map[rune]struct{}{
	',': struct{}{},
	'(': struct{}{},
	')': struct{}{},
}

// Prints an error message and aborts the program
func syntaxError(msg string) {
	panic(msg)
}

// Takes a list of tokens and an index value. Increments the index value and throws a syntax error
// if the index value becomes larger than the length of token list.
func goToNextToken(tokens *[]string, idx *int) {
	(*idx)++
	if len(*tokens) < *idx {
		syntaxError(fmt.Sprintf("Not enough tokens to parse: \"%s\"", strings.Join(*tokens, "")))
	}
}

// Takes a string value, a list of tokens and the index of a token in the list and
// throws a system error if the string value does not match with the token pointed at by the index.
func expect(token1 string, token2 string, msg string) {
	if token1 != token2 {
		syntaxError(msg)
	}
}

// Takes a string value, a list of tokens and the index of a token in the list and
// throws a system error if the string value matches with the token pointed at by the index.
func unexpect(token1 string, token2 string, msg string) {
	if token1 == token2 {
		syntaxError(msg)
	}
}

// Takes a string to tokenize and returns a list of tokens extracted from that string.
func tokenizer(s string) []string {
	tokens := []string{}
	buf := ""
	for _, r := range s {
		// Ignore white space characters
		if r == ' ' || r == '\t' || r == '\n' {
			continue
		}

		if _, exists := delims[r]; exists {
			// If there is no character in the buffer, ignore it
			if len(buf) > 0 {
				tokens = append(tokens, buf)
			}
			tokens = append(tokens, string(r))
			buf = ""
			continue
		}
		// append the character to the string
		buf += string(r)
	}

	// If the buffer is not empty, add it as the last token to the list
	if len(buf) > 0 {
		tokens = append(tokens, buf)
	}

	return tokens
}

// Takes the tokens of an atom and parses the atom to return the predicate and the list of constants
func parse(tokens []string) (types.Predicate, []types.Constant) {
	if len(tokens) == 0 {
		syntaxError("An empty string cannot be an atom")
	}

	unexpect(",", tokens[0], fmt.Sprintf("\",\" cannot be a predicate in atom: \"%s\"\n", strings.Join(tokens, "")))
	unexpect("(", tokens[0], fmt.Sprintf("\"(\" cannot be a predicate in atom: \"%s\"\n", strings.Join(tokens, "")))
	unexpect(")", tokens[0], fmt.Sprintf("\")\" cannot be a predicate in atom: \"%s\"\n", strings.Join(tokens, "")))

	predicate := types.Predicate(tokens[0])

	if len(tokens) == 1 {
		return predicate, make([]types.Constant, 0)
	}

	expect("(", tokens[1], fmt.Sprintf("Expected \"(\" after predicate in atom: \"%s\"\n", strings.Join(tokens, "")))

	constants := []types.Constant{}
	lastIndex := len(tokens) // The default last index in the list of tokens
	for i := 2; i < len(tokens); i++ {
		token := tokens[i]

		// This should indicate the end of parsing
		if token == ")" {
			// If there is an open parenthesis but no variable, throw an error
			if len(constants) == 0 {
				syntaxError(fmt.Sprintf("Expected constants in atom \"%s\" but found nothing\n", strings.Join(tokens, "")))
			}
			// Update the last index
			lastIndex = i + 1
			// We are done with parsing. Return the results
			break
		}

		// Anything that is not a comma or close parenthesis must be a substitution at this point
		if token != "," {
			// Add it to the list of constants if it is a lower-case string. Otherwise, throw an error
			// message and abort the process
			if token == strings.ToLower(token) {
				constants = append(constants, types.Constant(token))
			} else { // Throw an error message and abort
				msg := fmt.Sprintf("Variable \"%s\" not allowed in atom \"%s\"\n", token, strings.Join(tokens, ""))
				panic(msg)
			}
		}
	}

	if len(constants) > 0 {
		expect(")", tokens[lastIndex-1], fmt.Sprintf("Expected \")\" at the end of the list constants in atom formula in: \"%s\"\n", strings.Join(tokens, "")))
		unexpect(",", tokens[lastIndex-2], fmt.Sprintf("Unexpected \",\" before \")\" at the end of the list of substitutions in atom formula in: \"%s\"\n", strings.Join(tokens, "")))
	}

	return predicate, constants

}

// ParseAtom takes a string and parses it to return an Atom
func ParseAtom(s string) *Atom {
	// Tokenize the string
	tokens := tokenizer(s)

	// Parse the string
	predicate, constants := parse(tokens)

	now := clock.GetNow() // By default, atoms are only valid at the current time point
	// Create and return the atom
	return &Atom{
		Predicate:     predicate,
		constantsInfo: substitutiontable.CreateSubstitutionRecord((*[]types.Substitution)(unsafe.Pointer(&constants)), now, now),
	}
}

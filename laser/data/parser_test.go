package data

import (
	"testing"

	"bitbucket.org/hrbazoo/laser/cassert"
	"bitbucket.org/hrbazoo/laser/clock"

	"bitbucket.org/hrbazoo/laser/data/types"
)

/*
 * Copyright (C) 2019, Hamid R. Bazoobandi (hamid@dipperx.com)
 *
 * This file contains the test units of atom parser
 */
func TestAtomParser1(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	atom := ParseAtom("p(a,b)")

	assert.Equal(atom.Predicate, types.Predicate("p"))
	assert.Equal(len(atom.GetConstants()), 2)
	assert.Equal(atom.GetConstants()[0], types.Constant("a"))
	assert.Equal(atom.GetConstants()[1], types.Constant("b"))
	assert.Equal(atom.constantsInfo.ConsiderationTime, clock.GetNow())
	assert.Equal(atom.constantsInfo.HorizonTime, clock.GetNow())
}

func TestAtomParser2(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	atom := ParseAtom("p(a)")

	assert.Equal(atom.Predicate, types.Predicate("p"))
	assert.Equal(len(atom.GetConstants()), 1)
	assert.Equal(atom.GetConstants()[0], types.Constant("a"))
	assert.Equal(atom.constantsInfo.ConsiderationTime, clock.GetNow())
	assert.Equal(atom.constantsInfo.HorizonTime, clock.GetNow())
}

func TestAtomParser3(t *testing.T) {
	assert := cassert.New(t)
	clock.Reset()

	atom := ParseAtom("p")

	assert.Equal(atom.Predicate, types.Predicate("p"))
	assert.Equal(len(atom.GetConstants()), 0)
	assert.Equal(atom.constantsInfo.ConsiderationTime, clock.GetNow())
	assert.Equal(atom.constantsInfo.HorizonTime, clock.GetNow())
}

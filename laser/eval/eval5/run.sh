#!/bin/bash

############################################
# Copyright (C) 2019, Hamid R. Bazoobandi (h.bazoubandi@vu.nl)
# This file contains the script that runs some evaluations and
# draws the charts to report the results.
############################################
execTimeBarplotYAxisLabel="Evaluation Time (us)"
nFormulaEvaluationsBarplotYAxisLabel="Average Number of Formula Evaluations"
############################################
execTimeCSV=execTime.csv
nFormulaEvaluationsCSV=nFormulaEvaluations.csv
RplottingScript=./chart.R
############################################
execTimeBarplot="execTimeBarplot.pdf"
nFormulaEvaluationsBarplot="numFormulaEvaluations.pdf"
############################################
iterations=10
timePoints=300
###########################################
# IMPORTANT: if you change the following variables,
# remember to update the corresponding R script that
# draws the figures
############################################
nAtoms=(100 500 1000)
winSizes=( 0 1 2 )
useIp=('', '-no-invalidation-prediction')
############################################
evalOut=/tmp/evalOut
execTime=/tmp/execTime
conclusions=/tmp/conclusions
nJoins=/tmp/nJoins
nEvaluatedFormulae=/tmp/nEvaluatedFormulae
nRecords=/tmp/nRecords
nInvalidationTokens=/tmp/nInvalidationRecords
############################################
execDataCSV="nAtoms,winSize,IP,executionTime" # This is the header of the CSV file
formulaEvaluationDataCSV="nAtoms,winSize,IP,nFormulaEvaluations" # This is the header of the CSV file
############################################
for ip in ${useIp[@]}; do
    for atoms in ${nAtoms[@]}; do
        maxRand=`expr $atoms / 7`
        for win in ${winSizes[@]}; do
            ############################################
            :>$evalOut
            :>$execTime
            :>$evalOut
            :>$conclusions
            :>$nJoins
            :>$nEvaluatedFormulae
            :>$nRecords
            :>$nInvalidationTokens
            ############################################
            printf '################## win=%d atoms=%d #################\n' $win $atoms

            for i in `seq $iterations`; do
                printf "go run main.go -ltp=$timePoints -apt=$atoms -max-rand=$maxRand -win-size=$win $ip ...\t%dth evaluation\r" $i
                go run main.go -ltp=$timePoints -apt=$nAtoms -max-rand=$maxRand -win-size=$win $ip > $evalOut
                cat $evalOut | grep 'Avg execution time' | awk -F '=' '{print $2}' >> $execTime
                cat $evalOut | grep 'Conclusions' | awk -F '=' '{print $2}' >> $conclusions
                cat $evalOut | grep 'Join' | awk -F '=' '{print $2}' >> $nJoins
                cat $evalOut | grep 'Evaluated formulae' | awk -F '=' '{print $2}' >> $nEvaluatedFormulae
                cat $evalOut | grep 'Records' | awk -F '=' '{print $2}' >> $nRecords
                cat $evalOut | grep 'InvalidationTokens' | awk -F '=' '{print $2}' >> $nInvalidationTokens
            done
            ############################################
            echo ""
            avgExecTime=$(cat $execTime | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if(n > 0) {printf("%f", sum/n)}}')
            avgConclusions=$(cat $conclusions | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            avgJoins=$(cat $nJoins | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            avgFormulaeEvaluations=$(cat $nEvaluatedFormulae | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            avgRecords=$(cat $nRecords | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            avgInvalidationTokens=$(cat $nInvalidationTokens | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            ############################################
            Printf "Average execution time of program at each time point = %f ns\n" $avgExecTime
            Printf "Average number of conclusions of program at each time point = %f\n" $avgConclusions
            Printf "Average number of join operations of program at each time point = %f\n" $avgJoins
            Printf "Average number of times formulae were evaluated = %f\n" $avgFormulaeEvaluations
            Printf "Average number of substitution records = %f\n" $avgRecords
            Printf "Average number of invalidation tokens = %f\n" $avgInvalidationTokens
            ############################################
            # Add the statistics to the CSV data
            if [[ $ip = '-no-invalidation-prediction' ]]; then
                execDataCSV="$execDataCSV\natoms$atoms,w$win,NOIP,$avgExecTime"
                formulaEvaluationDataCSV="$formulaEvaluationDataCSV\natoms$atoms,w$win,NOIP,$avgFormulaeEvaluations"
            else
                execDataCSV="$execDataCSV\natoms$atoms,w$win,IP,$avgExecTime"
                formulaEvaluationDataCSV="$formulaEvaluationDataCSV\natoms$atoms,w$win,IP,$avgFormulaeEvaluations"
            fi
            echo $dataCSV
        done
    done
done

# Store the results in a csv file
printf $execDataCSV > $execTimeCSV
printf $formulaEvaluationDataCSV > $nFormulaEvaluationsCSV
# Run the R sceipt to draw the chart
Rscript --vanilla $RplottingScript $execTimeCSV "$execTimeBarplotYAxisLabel" "et"
mv Rplot.pdf $execTimeBarplot

Rscript --vanilla $RplottingScript $nFormulaEvaluationsCSV "$nFormulaEvaluationsBarplotYAxisLabel" "nf"
mv Rplot.pdf $nFormulaEvaluationsBarplot
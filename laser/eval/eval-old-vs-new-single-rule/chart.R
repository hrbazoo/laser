library(ggplot2)
library(pracma)
library(plotly)

args = commandArgs(trailingOnly=TRUE)

if (length(args)<3) {
  stop("Two argument must be supplied (input file).\n", call.=FALSE)
}

inputFile=args[1]
yAxisLabel=args[2]

options("scipen" = 100, "digits" = 1)

data = read.csv(inputFile, header = TRUE)
factor(data$winSize)

data$winSize
data$nAtoms
"==============================================="
args[3]
"==============================================="

# Stop R from changing the order of these items
data$winSize <- factor(data$winSize, levels = c("w10", "w50", "w100"))
data$nAtoms <- factor(data$nAtoms, levels = c("atoms100", "atoms500", "atoms1000"), labels = c("100 Atoms", "500 Atoms", "1000 Atoms"))

# Dodged barplot
if (strcmp(args[3], "et")) {
    qbarplot_yr_1 <- ggplot(data, aes(winSize, executionTime / 1000000)) + facet_grid(. ~ nAtoms) +
      geom_bar(aes(fill = IP), position = "dodge", stat="identity") +
      xlab("Window Length") + ylab(yAxisLabel) +
      scale_fill_discrete(name = '', labels=c("Laser1", "Laser2")) +
      theme(axis.ticks.x = element_blank()) +
      theme(axis.text.x = element_text(size = 12, face="bold")) +
      theme(axis.text.y = element_text(size = 12, face="bold")) +
      theme(axis.title.x = element_text(size = 13, face="bold")) +
      theme(axis.title.y = element_text(size = 13, face="bold")) +
      theme(legend.text = element_text(size = 11, face="bold")) +
      theme(panel.background = element_rect(colour='dark grey')) +
      theme(strip.text.x = element_text(size = 18, face="bold"),
            strip.background = element_rect(colour="dark grey"))
    
    #  Draw the chart
    # qbarplot_yr_1
} else {
    qbarplot_yr_1 <- ggplot(data, aes(winSize, nFormulaEvaluations)) + facet_grid(. ~ nAtoms) +
      geom_bar(aes(fill = IP), position = "dodge", stat="identity") +
      xlab("Window Length") + ylab(yAxisLabel) +
      scale_fill_discrete(name = '', labels=c("Old Laser", "New Laser")) +
      theme(axis.ticks.x = element_blank()) +
      theme(axis.text.x = element_text(size = 12, face="bold")) +
      theme(axis.text.y = element_text(size = 12, face="bold")) +
      theme(axis.title.x = element_text(size = 13, face="bold")) +
      theme(axis.title.y = element_text(size = 13, face="bold")) +
      theme(legend.text = element_text(size = 11, face="bold")) +
      theme(panel.background = element_rect(colour='dark grey')) +
      theme(strip.text.x = element_text(size = 18, face="bold"),
            strip.background = element_rect(colour="dark grey"))
    
    #  Draw the chart
    # qbarplot_yr_1
}

ggsave("Rplot.pdf", height=5)
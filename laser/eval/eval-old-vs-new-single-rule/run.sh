#!/bin/bash

############################################
# Copyright (C) 2019, Hamid R. Bazoobandi (h.bazoubandi@vu.nl)
# This file contains the script that runs some evaluations and
# draws the charts to report the results.
############################################
execTimeBarplotYAxisLabel="Evaluation Time (nano seconds)"
nFormulaEvaluationsBarplotYAxisLabel="Average number of Formula Evaluations"
############################################
execTimeCSV=execTime.csv
nFormulaEvaluationsCSV=nFormulaEvaluations.csv
RplottingScript=./chart.R
############################################
execTimeBarplot="execTimeBarplot.pdf"
nFormulaEvaluationsBarplot="numFormulaEvaluations.pdf"
############################################
iterations=2
timePoints=500
###########################################
# IMPORTANT: if you change the following variables,
# remember to update the corresponding R script that
# draws the figures
############################################
nAtoms=(100 500 1000)
winSizes=( 10 50 100 )
useIp=('', '-no-invalidation-prediction')
############################################
evalOut=/tmp/evalOut
execTime=/tmp/execTime
conclusions=/tmp/conclusions
nJoins=/tmp/nJoins
nEvaluatedFormulae=/tmp/nEvaluatedFormulae
nRecords=/tmp/nRecords
nInvalidationTokens=/tmp/nInvalidationRecords
oldLaserEvalPath="$HOME/projects/old-laser/Laser"
pypyPath=$oldLaserEvalPath/../pypy/bin/pypy
############################################
execDataCSV="nAtoms,winSize,IP,executionTime" # This is the header of the CSV file
formulaEvaluationDataCSV="nAtoms,winSize,IP,nFormulaEvaluations" # This is the header of the CSV file
############################################
for experiment in "oldLaser"; do
    for atoms in ${nAtoms[@]}; do
        for win in ${winSizes[@]}; do
            ############################################
            :>$evalOut
            :>$execTime
            :>$evalOut
            :>$conclusions
            :>$nJoins
            :>$nEvaluatedFormulae
            :>$nRecords
            :>$nInvalidationTokens
            ############################################
            printf '################## win=%d atoms=%d #################\n' $win $atoms
            for i in `seq $iterations`; do
                if [[ $experiment == "oldLaser" ]]; then
                    printf "$oldLaserEvalPath/eval.py evalSingleJoin2 $win $atoms $timePoints ...\t%dth evaluation\r" $i
                    $pypyPath $oldLaserEvalPath/eval.py evalSingleJoin2 $win $atoms $timePoints > $evalOut
                else
                    printf "go run main.go -ltp=$timePoints -apt=$atoms -win-size=$win $ip ...\t%dth evaluation\r" $i
                    go run main.go -ltp=$timePoints -apt=$atoms -win-size=$win "-no-invalidation-prediction" > $evalOut
                fi
                cat $evalOut | grep 'Avg execution time' | awk -F '=' '{print $2}' >> $execTime
                ############################################
            done
            echo ""
            echo $execTime
            ############################################
            cat $evalOut | grep 'Conclusions' | awk -F '=' '{print $2}' >> $conclusions
            ############################################
            avgExecTime=$(cat $execTime | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if(n > 0) {printf("%f", sum/n)}}')
            avgConclusions=$(cat $conclusions | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            ############################################
            Printf "Average execution time of program at each time point = %f ns\n" $avgExecTime
            Printf "Average number of conclusions of program at each time point = %f\n" $avgConclusions
            ############################################
            # Add the statistics to the CSV data
            if [[ $experiment == "oldLaser" ]]; then
                execDataCSV="$execDataCSV\natoms$atoms,w$win,OldLaser,$avgExecTime"
            else
                execDataCSV="$execDataCSV\natoms$atoms,w$win,NewLaser,$avgExecTime"
            fi
            echo $dataCSV
        done
    done
done

# # Store the results in a csv file
printf $execDataCSV > $execTimeCSV
printf $formulaEvaluationDataCSV > $nFormulaEvaluationsCSV
# # Run the R sceipt to draw the chart
Rscript --vanilla $RplottingScript $execTimeCSV "$execTimeBarplotYAxisLabel" "et"
mv Rplots.pdf $execTimeBarplot
library(ggplot2)
library(pracma)

args = commandArgs(trailingOnly=TRUE)

if (length(args)<3) {
  stop("Two argument must be supplied (input file).\n", call.=FALSE)
}

inputFile=args[1]
yAxisLabel=args[2]

options("scipen" = 100, "digits" = 1)

data = read.csv(inputFile, header = TRUE)
factor(data$winSize)

data$winSize
data$nAtoms
"==============================================="
args[3]
"==============================================="

# Stop R from changing the order of these items
data$winSize <- factor(data$winSize, levels = c("w10", "w50", "w100"))
data$nAtoms <- factor(data$nAtoms, levels = c("rules10", "rules20"), labels = c("10 Rules", "20 Rules"))

# Dodged barplot
if (strcmp(args[3], "et")) {
    qbarplot_yr_1 <- ggplot(data, aes(winSize, executionTime)) + facet_grid(. ~ nAtoms) +
      geom_bar(aes(fill = IP), position = "dodge", stat="identity") +
      xlab("Window Length") + ylab(yAxisLabel) +
      scale_fill_discrete(name = '', labels=c("Without IP", "With IP")) +
      theme(axis.ticks.x = element_blank()) +
      theme(axis.text.x = element_text(size = 12, face="bold")) +
      theme(axis.text.y = element_text(size = 12, face="bold")) +
      theme(axis.title.x = element_text(size = 15, face="bold")) +
      theme(axis.title.y = element_text(size = 15, face="bold")) +
      theme(legend.text = element_text(size = 15, face="bold")) +
      theme(panel.background = element_rect(colour='dark grey')) +
      theme(strip.text.x = element_text(size = 20, face="bold"),
            strip.background = element_rect(colour="dark grey"))
    
    #  Draw the chart
    qbarplot_yr_1
} else {
    qbarplot_yr_1 <- ggplot(data, aes(winSize, nFormulaEvaluations)) + facet_grid(. ~ nAtoms) +
      geom_bar(aes(fill = IP), position = "dodge", stat="identity") +
      xlab("Window Length") + ylab(yAxisLabel) +
      scale_fill_discrete(name = '', labels=c("Without IP", "With IP")) +
      theme(axis.ticks.x = element_blank()) +
      theme(axis.text.x = element_text(size = 12, face="bold")) +
      theme(axis.text.y = element_text(size = 12, face="bold")) +
      theme(axis.title.x = element_text(size = 15, face="bold")) +
      theme(axis.title.y = element_text(size = 15, face="bold")) +
      theme(legend.text = element_text(size = 15, face="bold")) +
      theme(panel.background = element_rect(colour='dark grey')) +
      theme(strip.text.x = element_text(size = 20, face="bold"),
            strip.background = element_rect(colour="dark grey"))
    
    #  Draw the chart
    qbarplot_yr_1
}

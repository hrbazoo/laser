package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"time"

	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data"
	"bitbucket.org/hrbazoo/laser/stats"

	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/program"
)

const dataPath = "./data/"
const programPath = "./program/"

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readLines(file *os.File) *[]string {
	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return &lines
}

func readRules(filename string) []string {
	file, err := os.Open(fmt.Sprintf("%s%s", programPath, filename))
	check(err)
	defer file.Close()
	return *readLines(file)
}

func readAtoms(timePoint int) []*data.AtomBatch {
	file, err := os.Open(fmt.Sprintf("%s%d.atoms", dataPath, timePoint))
	check(err)
	defer file.Close()
	return data.NewAtomBatch(readLines(file))
}

func main() {
	lastTimePointPrt := flag.Int("ltp", 10, "Number of time points the program executes")
	programFileNamePtr := flag.String("program", "program.txt", "File where program rules are stored!")
	noInvalidationPredictionPrt := flag.Bool("no-invalidation-prediction", false, "Shows whether the program must use the invalidation prediction algorithm or not.")

	flag.Parse()

	// If explicitly requested, disable the invalidation prediction algorithm
	if *noInvalidationPredictionPrt {
		config.UseInvalidationPrediction = false
	}

	fmt.Printf("===========================================\n")
	fmt.Printf("Laser evaluation with a multiple rules\n")
	fmt.Printf("Use invalidation prediction algorithm: %t\n", config.UseInvalidationPrediction)
	fmt.Printf("Total number of time points = %d\n", *lastTimePointPrt)
	fmt.Printf("===========================================\n")

	// Reset the clock. Start from the first time point
	clock.Reset()

	rules := readRules(*programFileNamePtr)

	p := program.New(rules)

	evaluationTime := time.Duration(0)
	nResults := 0
	for t := 1; t <= *lastTimePointPrt; t++ {
		atomBatches := readAtoms(t)

		p.InsertMultipleAtomBatches(&atomBatches)

		before := time.Now()
		p.Evaluate()
		evaluationTime += time.Now().Sub(before)

		results := p.CollectConcludedAtoms()

		for _, ab := range *results {
			nResults += len(ab.GetAtomStringFormats())
		}

		p.Cleanup()
		clock.GoToNextTimePoint()
	}

	fmt.Printf("\n======================================\n")
	fmt.Printf("Avg execution time = %f\n", float64(evaluationTime)/float64(*lastTimePointPrt))
	fmt.Printf("Conclusions = %d\n", nResults)
	fmt.Printf("#Join = %d\n", stats.ExecutionProfile.TotalNumberOfJoinOperations)
	fmt.Printf("#Evaluated formulae = %d\n", stats.ExecutionProfile.TotalNumberOfFormulaEvaluations)
	if config.UseInvalidationPrediction {
		fmt.Printf("#Skipped = %d\n", stats.ExecutionProfile.TotalNumberOfSkippedFormulaExecutions)
		fmt.Printf("#InvalidationTokens = %d\n", stats.ExecutionProfile.TotalNumberOfInvalidationTokens)
	}
	fmt.Printf("#Records = %d\n", stats.ExecutionProfile.TotalNumberOfSubstitutionRecords)
}

package main

import (
	"flag"
	"fmt"
	"strconv"
	"time"

	"bitbucket.org/hrbazoo/laser/config"
	"bitbucket.org/hrbazoo/laser/data/types"
	"bitbucket.org/hrbazoo/laser/stats"

	"bitbucket.org/hrbazoo/laser/clock"
	"bitbucket.org/hrbazoo/laser/data"
	"bitbucket.org/hrbazoo/laser/program"
)

func generateData(nAtoms int, rng int, predicates []types.Predicate) []*data.AtomBatch {
	atoms := []string{}
	for i, p := range predicates {
		for j := 0; i < nAtoms; i++ {
			// c1 := rand.Intn(rng)
			// c2 := rand.Intn(rng)
			c1 := i
			c2 := j
			atoms = append(atoms, string(p)+"("+strconv.Itoa(c1)+","+strconv.Itoa(c2)+")")
		}
	}

	return data.NewAtomBatch(&atoms)
}

func main() {
	lastTimePointPrt := flag.Int("ltp", 10, "Number of time points the program executes")
	nAtomsPerTimePointPtr := flag.Int("apt", 80, "How many atoms for each predicate must be generated per each time point")
	nMaxRandPrt := flag.Int("max-rand", 20, "The maximum value for the random numbers generated as constants")
	noInvalidationPredictionPrt := flag.Bool("no-invalidation-prediction", false, "Shows whether the program must use the invalidation prediction algorithm or not.")
	winSizePtr := flag.Int("win-size", 1, "Shows the window size.")

	flag.Parse()

	// If explicitly requested, disable the invalidation prediction algorithm
	if *noInvalidationPredictionPrt {
		config.UseInvalidationPrediction = false
	}

	fmt.Printf("===========================================\n")
	fmt.Printf("Laser evaluation with a single rule\n")
	fmt.Printf("Use invalidation prediction algorithm: %t\n", config.UseInvalidationPrediction)
	fmt.Printf("Total number of time points = %d\n", *lastTimePointPrt)
	fmt.Printf("Maximum value for random numbers generated as constants = %d\n", *nMaxRandPrt)
	fmt.Printf("Number of gemernated atoms for each predicate per time point = %d\n", *nAtomsPerTimePointPtr)
	fmt.Printf("Window size = %d\n", *winSizePtr)
	fmt.Printf("FLAG ARGS = %s\n", flag.Args())
	fmt.Printf("===========================================\n")

	// Reset the clock. Start from the first time point
	clock.Reset()

	rules := []string{
		fmt.Sprintf("h(X,Y) := timewindim(p(X,Y), %d, 1), timewindim(q(Y,X), %d, 1)", *winSizePtr, *winSizePtr),
	}

	p := program.New(rules)

	evaluationTime := time.Duration(0)
	nResults := 0
	for t := 1; t <= *lastTimePointPrt; t++ {
		// To evaluate the overhead of the invalidation prediction, we do not send any
		// atom q(X,Y) to the program
		var predicates []types.Predicate
		// if t > 1 && t%*winSizePtr != 0 {
		// predicates = []types.Predicate{types.Predicate("p"), types.Predicate("q")}
		// } else {
		predicates = []types.Predicate{types.Predicate("p")}
		// }
		atomBatches := generateData(*nAtomsPerTimePointPtr, *nMaxRandPrt, predicates)
		p.InsertMultipleAtomBatches(&atomBatches)

		before := time.Now()
		p.Evaluate()
		after := time.Now()
		evaluationTime += after.Sub(before)

		results := p.CollectConcludedAtoms()

		for _, ab := range *results {
			nResults += len(ab.GetAtomStringFormats())
		}

		p.Cleanup()
		clock.GoToNextTimePoint()
	}

	fmt.Printf("\n======================================\n")
	fmt.Printf("Avg execution time = %f\n", float64(evaluationTime)/float64(*lastTimePointPrt))
	fmt.Printf("Conclusions = %d\n", nResults)
	fmt.Printf("#Join = %d\n", stats.ExecutionProfile.TotalNumberOfJoinOperations)
	fmt.Printf("#Evaluated formulae = %d\n", stats.ExecutionProfile.TotalNumberOfFormulaEvaluations)
	if config.UseInvalidationPrediction {
		fmt.Printf("#Skipped = %d\n", stats.ExecutionProfile.TotalNumberOfSkippedFormulaExecutions)
		fmt.Printf("#InvalidationTokens = %d\n", stats.ExecutionProfile.TotalNumberOfInvalidationTokens)
	}
	fmt.Printf("#Records = %d\n", stats.ExecutionProfile.TotalNumberOfSubstitutionRecords)
}

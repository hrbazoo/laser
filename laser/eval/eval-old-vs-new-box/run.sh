#!/bin/bash

############################################
# Copyright (C) 2019, Hamid R. Bazoobandi (h.bazoubandi@vu.nl)
# This file contains the script that runs some evaluations and
# draws the charts to report the results.
############################################
execTimeBarplotYAxisLabel="Evaluation Time (nano seconds)"
nFormulaEvaluationsBarplotYAxisLabel="Average number of Formula Evaluations"
############################################
execTimeCSV=execTime.csv
nFormulaEvaluationsCSV=nFormulaEvaluations.csv
RplottingScript=./chart.R
############################################
execTimeBarplot="execTimeBarplot.pdf"
nFormulaEvaluationsBarplot="numFormulaEvaluations.pdf"
############################################
iterations=5
timePoints=120
###########################################
# IMPORTANT: if you change the following variables,
# remember to update the corresponding R script that
# draws the figures
############################################
nAtoms=(100 500 1000)
winSizes=( 10 50 100 )
useIp=('', '-no-invalidation-prediction')
#maxRand=100 #$(expr $nAtoms / 7)
############################################
evalOut=/tmp/evalOut
execTime=/tmp/execTime
conclusions=/tmp/conclusions
nJoins=/tmp/nJoins
nEvaluatedFormulae=/tmp/nEvaluatedFormulae
nRecords=/tmp/nRecords
nInvalidationTokens=/tmp/nInvalidationRecords
oldLaserEvalPath="$HOME/projects/old-laser/Laser"
pypyPath=$oldLaserEvalPath/../pypy/bin/pypy
############################################
execDataCSV="nAtoms,winSize,IP,executionTime" # This is the header of the CSV file
formulaEvaluationDataCSV="nAtoms,winSize,IP,nFormulaEvaluations" # This is the header of the CSV file
############################################
for experiment in "oldLaser" "newLaser"; do
    for atoms in ${nAtoms[@]}; do
        for win in ${winSizes[@]}; do
            ############################################
            :>$evalOut
            :>$execTime
            :>$evalOut
            :>$conclusions
            :>$nJoins
            :>$nEvaluatedFormulae
            :>$nRecords
            :>$nInvalidationTokens
            ############################################
            printf '################## win=%d atoms=%d #################\n' $win $atoms
            for i in `seq $iterations`; do
                if [[ $experiment == "oldLaser" ]]; then
                    printf "$oldLaserEvalPath/eval.py evalBox2 $win $atoms $timePoints ...\t%dth evaluation\r" $i
                    $pypyPath $oldLaserEvalPath/eval.py evalBox2 $win $atoms $timePoints > $evalOut
                else
                    printf "go run main.go -ltp=$timePoints -apt=$atoms -win-size=$win $ip ...\t%dth evaluation\r" $i
                    go run main.go -ltp=$timePoints -apt=$atoms -win-size=$win "-no-invalidation-prediction" > $evalOut
                fi
                cat $evalOut | grep 'Avg execution time' | awk -F '=' '{print $2}' >> $execTime
                ############################################
            done
            echo ""
            echo $execTime
            ############################################
            #         printf "go run main.go -ltp=$timePoints -apt=$atoms -max-rand=$maxRand -win-size=$win $ip ...\t%dth evaluation\r" $i
            #go run main.go -ltp=$timePoints -apt=$nAtoms -max-rand=$maxRand -win-size=$win $ip > $evalOut
            cat $evalOut | grep 'Conclusions' | awk -F '=' '{print $2}' >> $conclusions
            #         cat $evalOut | grep 'Join' | awk -F '=' '{print $2}' >> $nJoins
            #         cat $evalOut | grep 'Evaluated formulae' | awk -F '=' '{print $2}' >> $nEvaluatedFormulae
            #         cat $evalOut | grep 'Records' | awk -F '=' '{print $2}' >> $nRecords
            #         cat $evalOut | grep 'InvalidationTokens' | awk -F '=' '{print $2}' >> $nInvalidationTokens
            #     done
            #     ############################################
            #     echo ""
            avgExecTime=$(cat $execTime | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if(n > 0) {printf("%f", sum/n)}}')
            avgConclusions=$(cat $conclusions | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            #     avgJoins=$(cat $nJoins | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            #     avgFormulaeEvaluations=$(cat $nEvaluatedFormulae | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            #     avgRecords=$(cat $nRecords | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            #     avgInvalidationTokens=$(cat $nInvalidationTokens | awk 'BEGIN{sum=0;n=0}{sum += $1;n++;}END{if (n > 0) {printf("%f", sum/n)}}')
            #     ############################################
            Printf "Average execution time of program at each time point = %f ns\n" $avgExecTime
            Printf "Average number of conclusions of program at each time point = %f\n" $avgConclusions
            #     Printf "Average number of join operations of program at each time point = %f\n" $avgJoins
            #     Printf "Average number of times formulae were evaluated = %f\n" $avgFormulaeEvaluations
            #     Printf "Average number of substitution records = %f\n" $avgRecords
            #     Printf "Average number of invalidation tokens = %f\n" $avgInvalidationTokens
            #     ############################################
            # Add the statistics to the CSV data
            if [[ $experiment == "oldLaser" ]]; then
                execDataCSV="$execDataCSV\natoms$atoms,w$win,OldLaser,$avgExecTime"
                # formulaEvaluationDataCSV="$formulaEvaluationDataCSV\natoms$atoms,w$win,NOIP,$avgFormulaeEvaluations"
            else
                execDataCSV="$execDataCSV\natoms$atoms,w$win,NewLaser,$avgExecTime"
                # formulaEvaluationDataCSV="$formulaEvaluationDataCSV\natoms$atoms,w$win,IP,$avgFormulaeEvaluations"
            fi
            echo $dataCSV
        done
    done
done

# # Store the results in a csv file
printf $execDataCSV > $execTimeCSV
printf $formulaEvaluationDataCSV > $nFormulaEvaluationsCSV
# # Run the R sceipt to draw the chart
Rscript --vanilla $RplottingScript $execTimeCSV "$execTimeBarplotYAxisLabel" "et"
mv Rplots.pdf $execTimeBarplot

# Rscript --vanilla $RplottingScript $nFormulaEvaluationsCSV "$nFormulaEvaluationsBarplotYAxisLabel" "nf"
# mv Rplots.pdf $nFormulaEvaluationsBarplot
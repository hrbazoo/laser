package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	"bitbucket.org/hrbazoo/laser/data/types"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func generateData(nAtoms int /*rng int, */, nPredicates int) *[]string {
	rand.Seed(int64(time.Now().Nanosecond()))

	pPred := types.Predicate("p")
	atoms := []string{}

	for i := 0; i < nPredicates; i++ {
		for j := 0; j < nAtoms; j++ {
			// c1 := rand.Intn(j)
			// c2 := rand.Intn(j)
			c1 := j
			c2 := j
			newAtom := fmt.Sprintf("%s%d(%d,%d)", pPred, i, c1, c2)
			atoms = append(atoms, newAtom)
		}
	}

	return &atoms
}

func main() {
	lastTimePointPrt := flag.Int("ltp", 10, "Number of time points the program executes")
	nAtomsPerTimePointPtr := flag.Int("apt", 80, "How many atoms for each predicate must be generated per each time point")
	winSizePtr := flag.Int("winsize", 2, "The window length")
	nPredicatesPtr := flag.Int("npred", 2, "The total number of predicates")
	outputPathPtr := flag.String("outpath", "./", "Where generated data files are stored")

	flag.Parse()

	fmt.Printf("===========================================\n")
	fmt.Printf("Data generator started...\n")
	fmt.Printf("Total number of time points = %d\n", *lastTimePointPrt)
	fmt.Printf("Window size = %d\n", *winSizePtr)
	// fmt.Printf("Maximum range for the number generator = %d\n", *nMaxRangePrt)
	fmt.Printf("Number of gemernated atoms for each predicate per time point = %d\n", *nAtomsPerTimePointPtr)
	fmt.Printf("Output path = %s\n", *outputPathPtr)
	fmt.Printf("Number of predicates = %d\n", *nPredicatesPtr)
	fmt.Printf("===========================================\n")

	for t := 1; t <= *lastTimePointPrt; t++ {
		atoms := &[]string{}

		if t > 1 && t%*winSizePtr != 0 {
			atoms = generateData(*nAtomsPerTimePointPtr /**nMaxRangePrt, */, *nPredicatesPtr)
		} else {
			// fmt.Println("Skipped time point", t)
		}

		f, err := os.Create(fmt.Sprintf("%s/%d.atoms", *outputPathPtr, t))
		check(err)
		defer f.Close()

		for _, a := range *atoms {
			_, err := f.WriteString(fmt.Sprintf("%s\n", a))
			check(err)
		}
		f.Sync()
	}

}

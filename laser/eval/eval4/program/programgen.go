package main

import (
	"flag"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func generateProgram(winSize int, nRules int) []string {
	firstRule := fmt.Sprintf("h1(X,Y) := timewindim(p0(X,Y), %d, 1), timewinbox(p1(X,Y), %d, 1)", winSize, winSize)

	rules := []string{firstRule}
	for i := 2; i <= nRules; i++ {
		rule := fmt.Sprintf("h%d(X,Y) := h%d(X,Y)", i, i-1)
		rules = append(rules, rule)
	}

	for _, r := range rules {
		fmt.Println(r)
	}

	return rules
}

func main() {
	winSizePtr := flag.Int("win-size", 1, "Shows the window size.")
	nRulesPtr := flag.Int("rules", 2, "Shows the number of rules.")
	outputFilenamePtr := flag.String("output", "program.txt", "The name of the output file.")

	flag.Parse()

	fmt.Printf("===========================================\n")
	fmt.Printf("Program generator started...\n")
	fmt.Printf("Number of gemernated rules =%d\n", *nRulesPtr)
	fmt.Printf("Window size = %d\n", *winSizePtr)
	fmt.Printf("===========================================\n")

	rules := generateProgram(*winSizePtr, *nRulesPtr)

	f, err := os.Create(*outputFilenamePtr)
	check(err)
	defer f.Close()

	for _, r := range rules {
		_, err := f.WriteString(fmt.Sprintf("%s\n", r))
		check(err)
		f.Sync()
	}
}
